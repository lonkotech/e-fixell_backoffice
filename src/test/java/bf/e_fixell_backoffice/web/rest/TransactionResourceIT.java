package bf.e_fixell_backoffice.web.rest;

import bf.e_fixell_backoffice.EFixellBackofficeApp;
import bf.e_fixell_backoffice.domain.Transaction;
import bf.e_fixell_backoffice.domain.FicheTechnique;
import bf.e_fixell_backoffice.domain.Produit;
import bf.e_fixell_backoffice.domain.Commande;
import bf.e_fixell_backoffice.domain.Approvisionnement;
import bf.e_fixell_backoffice.domain.Livraison;
import bf.e_fixell_backoffice.domain.Vente;
import bf.e_fixell_backoffice.domain.Devise;
import bf.e_fixell_backoffice.repository.TransactionRepository;
import bf.e_fixell_backoffice.service.TransactionService;
import bf.e_fixell_backoffice.service.dto.TransactionDTO;
import bf.e_fixell_backoffice.service.mapper.TransactionMapper;
import bf.e_fixell_backoffice.service.dto.TransactionCriteria;
import bf.e_fixell_backoffice.service.TransactionQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import bf.e_fixell_backoffice.domain.enumeration.TypeTransaction;
import bf.e_fixell_backoffice.domain.enumeration.Etat;
/**
 * Integration tests for the {@link TransactionResource} REST controller.
 */
@SpringBootTest(classes = EFixellBackofficeApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class TransactionResourceIT {

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final Instant DEFAULT_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_QUANTITE = 1;
    private static final Integer UPDATED_QUANTITE = 2;
    private static final Integer SMALLER_QUANTITE = 1 - 1;

    private static final BigDecimal DEFAULT_PRIX_UNITAIRE = new BigDecimal(1);
    private static final BigDecimal UPDATED_PRIX_UNITAIRE = new BigDecimal(2);
    private static final BigDecimal SMALLER_PRIX_UNITAIRE = new BigDecimal(1 - 1);

    private static final TypeTransaction DEFAULT_TYPE_TRANSACTION = TypeTransaction.COMMANDE;
    private static final TypeTransaction UPDATED_TYPE_TRANSACTION = TypeTransaction.APPROVISIONNEMENT;

    private static final Etat DEFAULT_ETAT = Etat.ANNULER;
    private static final Etat UPDATED_ETAT = Etat.PROVISOIRE;

    private static final String DEFAULT_MOTIF = "AAAAAAAAAA";
    private static final String UPDATED_MOTIF = "BBBBBBBBBB";

    private static final Double DEFAULT_VALEUR_DEVISE = 1D;
    private static final Double UPDATED_VALEUR_DEVISE = 2D;
    private static final Double SMALLER_VALEUR_DEVISE = 1D - 1D;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private TransactionMapper transactionMapper;

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private TransactionQueryService transactionQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restTransactionMockMvc;

    private Transaction transaction;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Transaction createEntity(EntityManager em) {
        Transaction transaction = new Transaction()
            .code(DEFAULT_CODE)
            .date(DEFAULT_DATE)
            .quantite(DEFAULT_QUANTITE)
            .prixUnitaire(DEFAULT_PRIX_UNITAIRE)
            .typeTransaction(DEFAULT_TYPE_TRANSACTION)
            .etat(DEFAULT_ETAT)
            .motif(DEFAULT_MOTIF)
            .valeurDevise(DEFAULT_VALEUR_DEVISE);
        return transaction;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Transaction createUpdatedEntity(EntityManager em) {
        Transaction transaction = new Transaction()
            .code(UPDATED_CODE)
            .date(UPDATED_DATE)
            .quantite(UPDATED_QUANTITE)
            .prixUnitaire(UPDATED_PRIX_UNITAIRE)
            .typeTransaction(UPDATED_TYPE_TRANSACTION)
            .etat(UPDATED_ETAT)
            .motif(UPDATED_MOTIF)
            .valeurDevise(UPDATED_VALEUR_DEVISE);
        return transaction;
    }

    @BeforeEach
    public void initTest() {
        transaction = createEntity(em);
    }

    @Test
    @Transactional
    public void createTransaction() throws Exception {
        int databaseSizeBeforeCreate = transactionRepository.findAll().size();
        // Create the Transaction
        TransactionDTO transactionDTO = transactionMapper.toDto(transaction);
        restTransactionMockMvc.perform(post("/api/transactions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(transactionDTO)))
            .andExpect(status().isCreated());

        // Validate the Transaction in the database
        List<Transaction> transactionList = transactionRepository.findAll();
        assertThat(transactionList).hasSize(databaseSizeBeforeCreate + 1);
        Transaction testTransaction = transactionList.get(transactionList.size() - 1);
        assertThat(testTransaction.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testTransaction.getDate()).isEqualTo(DEFAULT_DATE);
        assertThat(testTransaction.getQuantite()).isEqualTo(DEFAULT_QUANTITE);
        assertThat(testTransaction.getPrixUnitaire()).isEqualTo(DEFAULT_PRIX_UNITAIRE);
        assertThat(testTransaction.getTypeTransaction()).isEqualTo(DEFAULT_TYPE_TRANSACTION);
        assertThat(testTransaction.getEtat()).isEqualTo(DEFAULT_ETAT);
        assertThat(testTransaction.getMotif()).isEqualTo(DEFAULT_MOTIF);
        assertThat(testTransaction.getValeurDevise()).isEqualTo(DEFAULT_VALEUR_DEVISE);
    }

    @Test
    @Transactional
    public void createTransactionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = transactionRepository.findAll().size();

        // Create the Transaction with an existing ID
        transaction.setId(1L);
        TransactionDTO transactionDTO = transactionMapper.toDto(transaction);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTransactionMockMvc.perform(post("/api/transactions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(transactionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Transaction in the database
        List<Transaction> transactionList = transactionRepository.findAll();
        assertThat(transactionList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllTransactions() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList
        restTransactionMockMvc.perform(get("/api/transactions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(transaction.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())))
            .andExpect(jsonPath("$.[*].quantite").value(hasItem(DEFAULT_QUANTITE)))
            .andExpect(jsonPath("$.[*].prixUnitaire").value(hasItem(DEFAULT_PRIX_UNITAIRE.intValue())))
            .andExpect(jsonPath("$.[*].typeTransaction").value(hasItem(DEFAULT_TYPE_TRANSACTION.toString())))
            .andExpect(jsonPath("$.[*].etat").value(hasItem(DEFAULT_ETAT.toString())))
            .andExpect(jsonPath("$.[*].motif").value(hasItem(DEFAULT_MOTIF)))
            .andExpect(jsonPath("$.[*].valeurDevise").value(hasItem(DEFAULT_VALEUR_DEVISE.doubleValue())));
    }
    
    @Test
    @Transactional
    public void getTransaction() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get the transaction
        restTransactionMockMvc.perform(get("/api/transactions/{id}", transaction.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(transaction.getId().intValue()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE))
            .andExpect(jsonPath("$.date").value(DEFAULT_DATE.toString()))
            .andExpect(jsonPath("$.quantite").value(DEFAULT_QUANTITE))
            .andExpect(jsonPath("$.prixUnitaire").value(DEFAULT_PRIX_UNITAIRE.intValue()))
            .andExpect(jsonPath("$.typeTransaction").value(DEFAULT_TYPE_TRANSACTION.toString()))
            .andExpect(jsonPath("$.etat").value(DEFAULT_ETAT.toString()))
            .andExpect(jsonPath("$.motif").value(DEFAULT_MOTIF))
            .andExpect(jsonPath("$.valeurDevise").value(DEFAULT_VALEUR_DEVISE.doubleValue()));
    }


    @Test
    @Transactional
    public void getTransactionsByIdFiltering() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        Long id = transaction.getId();

        defaultTransactionShouldBeFound("id.equals=" + id);
        defaultTransactionShouldNotBeFound("id.notEquals=" + id);

        defaultTransactionShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultTransactionShouldNotBeFound("id.greaterThan=" + id);

        defaultTransactionShouldBeFound("id.lessThanOrEqual=" + id);
        defaultTransactionShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllTransactionsByCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where code equals to DEFAULT_CODE
        defaultTransactionShouldBeFound("code.equals=" + DEFAULT_CODE);

        // Get all the transactionList where code equals to UPDATED_CODE
        defaultTransactionShouldNotBeFound("code.equals=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    public void getAllTransactionsByCodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where code not equals to DEFAULT_CODE
        defaultTransactionShouldNotBeFound("code.notEquals=" + DEFAULT_CODE);

        // Get all the transactionList where code not equals to UPDATED_CODE
        defaultTransactionShouldBeFound("code.notEquals=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    public void getAllTransactionsByCodeIsInShouldWork() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where code in DEFAULT_CODE or UPDATED_CODE
        defaultTransactionShouldBeFound("code.in=" + DEFAULT_CODE + "," + UPDATED_CODE);

        // Get all the transactionList where code equals to UPDATED_CODE
        defaultTransactionShouldNotBeFound("code.in=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    public void getAllTransactionsByCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where code is not null
        defaultTransactionShouldBeFound("code.specified=true");

        // Get all the transactionList where code is null
        defaultTransactionShouldNotBeFound("code.specified=false");
    }
                @Test
    @Transactional
    public void getAllTransactionsByCodeContainsSomething() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where code contains DEFAULT_CODE
        defaultTransactionShouldBeFound("code.contains=" + DEFAULT_CODE);

        // Get all the transactionList where code contains UPDATED_CODE
        defaultTransactionShouldNotBeFound("code.contains=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    public void getAllTransactionsByCodeNotContainsSomething() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where code does not contain DEFAULT_CODE
        defaultTransactionShouldNotBeFound("code.doesNotContain=" + DEFAULT_CODE);

        // Get all the transactionList where code does not contain UPDATED_CODE
        defaultTransactionShouldBeFound("code.doesNotContain=" + UPDATED_CODE);
    }


    @Test
    @Transactional
    public void getAllTransactionsByDateIsEqualToSomething() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where date equals to DEFAULT_DATE
        defaultTransactionShouldBeFound("date.equals=" + DEFAULT_DATE);

        // Get all the transactionList where date equals to UPDATED_DATE
        defaultTransactionShouldNotBeFound("date.equals=" + UPDATED_DATE);
    }

    @Test
    @Transactional
    public void getAllTransactionsByDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where date not equals to DEFAULT_DATE
        defaultTransactionShouldNotBeFound("date.notEquals=" + DEFAULT_DATE);

        // Get all the transactionList where date not equals to UPDATED_DATE
        defaultTransactionShouldBeFound("date.notEquals=" + UPDATED_DATE);
    }

    @Test
    @Transactional
    public void getAllTransactionsByDateIsInShouldWork() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where date in DEFAULT_DATE or UPDATED_DATE
        defaultTransactionShouldBeFound("date.in=" + DEFAULT_DATE + "," + UPDATED_DATE);

        // Get all the transactionList where date equals to UPDATED_DATE
        defaultTransactionShouldNotBeFound("date.in=" + UPDATED_DATE);
    }

    @Test
    @Transactional
    public void getAllTransactionsByDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where date is not null
        defaultTransactionShouldBeFound("date.specified=true");

        // Get all the transactionList where date is null
        defaultTransactionShouldNotBeFound("date.specified=false");
    }

    @Test
    @Transactional
    public void getAllTransactionsByQuantiteIsEqualToSomething() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where quantite equals to DEFAULT_QUANTITE
        defaultTransactionShouldBeFound("quantite.equals=" + DEFAULT_QUANTITE);

        // Get all the transactionList where quantite equals to UPDATED_QUANTITE
        defaultTransactionShouldNotBeFound("quantite.equals=" + UPDATED_QUANTITE);
    }

    @Test
    @Transactional
    public void getAllTransactionsByQuantiteIsNotEqualToSomething() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where quantite not equals to DEFAULT_QUANTITE
        defaultTransactionShouldNotBeFound("quantite.notEquals=" + DEFAULT_QUANTITE);

        // Get all the transactionList where quantite not equals to UPDATED_QUANTITE
        defaultTransactionShouldBeFound("quantite.notEquals=" + UPDATED_QUANTITE);
    }

    @Test
    @Transactional
    public void getAllTransactionsByQuantiteIsInShouldWork() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where quantite in DEFAULT_QUANTITE or UPDATED_QUANTITE
        defaultTransactionShouldBeFound("quantite.in=" + DEFAULT_QUANTITE + "," + UPDATED_QUANTITE);

        // Get all the transactionList where quantite equals to UPDATED_QUANTITE
        defaultTransactionShouldNotBeFound("quantite.in=" + UPDATED_QUANTITE);
    }

    @Test
    @Transactional
    public void getAllTransactionsByQuantiteIsNullOrNotNull() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where quantite is not null
        defaultTransactionShouldBeFound("quantite.specified=true");

        // Get all the transactionList where quantite is null
        defaultTransactionShouldNotBeFound("quantite.specified=false");
    }

    @Test
    @Transactional
    public void getAllTransactionsByQuantiteIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where quantite is greater than or equal to DEFAULT_QUANTITE
        defaultTransactionShouldBeFound("quantite.greaterThanOrEqual=" + DEFAULT_QUANTITE);

        // Get all the transactionList where quantite is greater than or equal to UPDATED_QUANTITE
        defaultTransactionShouldNotBeFound("quantite.greaterThanOrEqual=" + UPDATED_QUANTITE);
    }

    @Test
    @Transactional
    public void getAllTransactionsByQuantiteIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where quantite is less than or equal to DEFAULT_QUANTITE
        defaultTransactionShouldBeFound("quantite.lessThanOrEqual=" + DEFAULT_QUANTITE);

        // Get all the transactionList where quantite is less than or equal to SMALLER_QUANTITE
        defaultTransactionShouldNotBeFound("quantite.lessThanOrEqual=" + SMALLER_QUANTITE);
    }

    @Test
    @Transactional
    public void getAllTransactionsByQuantiteIsLessThanSomething() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where quantite is less than DEFAULT_QUANTITE
        defaultTransactionShouldNotBeFound("quantite.lessThan=" + DEFAULT_QUANTITE);

        // Get all the transactionList where quantite is less than UPDATED_QUANTITE
        defaultTransactionShouldBeFound("quantite.lessThan=" + UPDATED_QUANTITE);
    }

    @Test
    @Transactional
    public void getAllTransactionsByQuantiteIsGreaterThanSomething() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where quantite is greater than DEFAULT_QUANTITE
        defaultTransactionShouldNotBeFound("quantite.greaterThan=" + DEFAULT_QUANTITE);

        // Get all the transactionList where quantite is greater than SMALLER_QUANTITE
        defaultTransactionShouldBeFound("quantite.greaterThan=" + SMALLER_QUANTITE);
    }


    @Test
    @Transactional
    public void getAllTransactionsByPrixUnitaireIsEqualToSomething() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where prixUnitaire equals to DEFAULT_PRIX_UNITAIRE
        defaultTransactionShouldBeFound("prixUnitaire.equals=" + DEFAULT_PRIX_UNITAIRE);

        // Get all the transactionList where prixUnitaire equals to UPDATED_PRIX_UNITAIRE
        defaultTransactionShouldNotBeFound("prixUnitaire.equals=" + UPDATED_PRIX_UNITAIRE);
    }

    @Test
    @Transactional
    public void getAllTransactionsByPrixUnitaireIsNotEqualToSomething() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where prixUnitaire not equals to DEFAULT_PRIX_UNITAIRE
        defaultTransactionShouldNotBeFound("prixUnitaire.notEquals=" + DEFAULT_PRIX_UNITAIRE);

        // Get all the transactionList where prixUnitaire not equals to UPDATED_PRIX_UNITAIRE
        defaultTransactionShouldBeFound("prixUnitaire.notEquals=" + UPDATED_PRIX_UNITAIRE);
    }

    @Test
    @Transactional
    public void getAllTransactionsByPrixUnitaireIsInShouldWork() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where prixUnitaire in DEFAULT_PRIX_UNITAIRE or UPDATED_PRIX_UNITAIRE
        defaultTransactionShouldBeFound("prixUnitaire.in=" + DEFAULT_PRIX_UNITAIRE + "," + UPDATED_PRIX_UNITAIRE);

        // Get all the transactionList where prixUnitaire equals to UPDATED_PRIX_UNITAIRE
        defaultTransactionShouldNotBeFound("prixUnitaire.in=" + UPDATED_PRIX_UNITAIRE);
    }

    @Test
    @Transactional
    public void getAllTransactionsByPrixUnitaireIsNullOrNotNull() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where prixUnitaire is not null
        defaultTransactionShouldBeFound("prixUnitaire.specified=true");

        // Get all the transactionList where prixUnitaire is null
        defaultTransactionShouldNotBeFound("prixUnitaire.specified=false");
    }

    @Test
    @Transactional
    public void getAllTransactionsByPrixUnitaireIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where prixUnitaire is greater than or equal to DEFAULT_PRIX_UNITAIRE
        defaultTransactionShouldBeFound("prixUnitaire.greaterThanOrEqual=" + DEFAULT_PRIX_UNITAIRE);

        // Get all the transactionList where prixUnitaire is greater than or equal to UPDATED_PRIX_UNITAIRE
        defaultTransactionShouldNotBeFound("prixUnitaire.greaterThanOrEqual=" + UPDATED_PRIX_UNITAIRE);
    }

    @Test
    @Transactional
    public void getAllTransactionsByPrixUnitaireIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where prixUnitaire is less than or equal to DEFAULT_PRIX_UNITAIRE
        defaultTransactionShouldBeFound("prixUnitaire.lessThanOrEqual=" + DEFAULT_PRIX_UNITAIRE);

        // Get all the transactionList where prixUnitaire is less than or equal to SMALLER_PRIX_UNITAIRE
        defaultTransactionShouldNotBeFound("prixUnitaire.lessThanOrEqual=" + SMALLER_PRIX_UNITAIRE);
    }

    @Test
    @Transactional
    public void getAllTransactionsByPrixUnitaireIsLessThanSomething() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where prixUnitaire is less than DEFAULT_PRIX_UNITAIRE
        defaultTransactionShouldNotBeFound("prixUnitaire.lessThan=" + DEFAULT_PRIX_UNITAIRE);

        // Get all the transactionList where prixUnitaire is less than UPDATED_PRIX_UNITAIRE
        defaultTransactionShouldBeFound("prixUnitaire.lessThan=" + UPDATED_PRIX_UNITAIRE);
    }

    @Test
    @Transactional
    public void getAllTransactionsByPrixUnitaireIsGreaterThanSomething() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where prixUnitaire is greater than DEFAULT_PRIX_UNITAIRE
        defaultTransactionShouldNotBeFound("prixUnitaire.greaterThan=" + DEFAULT_PRIX_UNITAIRE);

        // Get all the transactionList where prixUnitaire is greater than SMALLER_PRIX_UNITAIRE
        defaultTransactionShouldBeFound("prixUnitaire.greaterThan=" + SMALLER_PRIX_UNITAIRE);
    }


    @Test
    @Transactional
    public void getAllTransactionsByTypeTransactionIsEqualToSomething() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where typeTransaction equals to DEFAULT_TYPE_TRANSACTION
        defaultTransactionShouldBeFound("typeTransaction.equals=" + DEFAULT_TYPE_TRANSACTION);

        // Get all the transactionList where typeTransaction equals to UPDATED_TYPE_TRANSACTION
        defaultTransactionShouldNotBeFound("typeTransaction.equals=" + UPDATED_TYPE_TRANSACTION);
    }

    @Test
    @Transactional
    public void getAllTransactionsByTypeTransactionIsNotEqualToSomething() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where typeTransaction not equals to DEFAULT_TYPE_TRANSACTION
        defaultTransactionShouldNotBeFound("typeTransaction.notEquals=" + DEFAULT_TYPE_TRANSACTION);

        // Get all the transactionList where typeTransaction not equals to UPDATED_TYPE_TRANSACTION
        defaultTransactionShouldBeFound("typeTransaction.notEquals=" + UPDATED_TYPE_TRANSACTION);
    }

    @Test
    @Transactional
    public void getAllTransactionsByTypeTransactionIsInShouldWork() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where typeTransaction in DEFAULT_TYPE_TRANSACTION or UPDATED_TYPE_TRANSACTION
        defaultTransactionShouldBeFound("typeTransaction.in=" + DEFAULT_TYPE_TRANSACTION + "," + UPDATED_TYPE_TRANSACTION);

        // Get all the transactionList where typeTransaction equals to UPDATED_TYPE_TRANSACTION
        defaultTransactionShouldNotBeFound("typeTransaction.in=" + UPDATED_TYPE_TRANSACTION);
    }

    @Test
    @Transactional
    public void getAllTransactionsByTypeTransactionIsNullOrNotNull() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where typeTransaction is not null
        defaultTransactionShouldBeFound("typeTransaction.specified=true");

        // Get all the transactionList where typeTransaction is null
        defaultTransactionShouldNotBeFound("typeTransaction.specified=false");
    }

    @Test
    @Transactional
    public void getAllTransactionsByEtatIsEqualToSomething() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where etat equals to DEFAULT_ETAT
        defaultTransactionShouldBeFound("etat.equals=" + DEFAULT_ETAT);

        // Get all the transactionList where etat equals to UPDATED_ETAT
        defaultTransactionShouldNotBeFound("etat.equals=" + UPDATED_ETAT);
    }

    @Test
    @Transactional
    public void getAllTransactionsByEtatIsNotEqualToSomething() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where etat not equals to DEFAULT_ETAT
        defaultTransactionShouldNotBeFound("etat.notEquals=" + DEFAULT_ETAT);

        // Get all the transactionList where etat not equals to UPDATED_ETAT
        defaultTransactionShouldBeFound("etat.notEquals=" + UPDATED_ETAT);
    }

    @Test
    @Transactional
    public void getAllTransactionsByEtatIsInShouldWork() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where etat in DEFAULT_ETAT or UPDATED_ETAT
        defaultTransactionShouldBeFound("etat.in=" + DEFAULT_ETAT + "," + UPDATED_ETAT);

        // Get all the transactionList where etat equals to UPDATED_ETAT
        defaultTransactionShouldNotBeFound("etat.in=" + UPDATED_ETAT);
    }

    @Test
    @Transactional
    public void getAllTransactionsByEtatIsNullOrNotNull() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where etat is not null
        defaultTransactionShouldBeFound("etat.specified=true");

        // Get all the transactionList where etat is null
        defaultTransactionShouldNotBeFound("etat.specified=false");
    }

    @Test
    @Transactional
    public void getAllTransactionsByMotifIsEqualToSomething() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where motif equals to DEFAULT_MOTIF
        defaultTransactionShouldBeFound("motif.equals=" + DEFAULT_MOTIF);

        // Get all the transactionList where motif equals to UPDATED_MOTIF
        defaultTransactionShouldNotBeFound("motif.equals=" + UPDATED_MOTIF);
    }

    @Test
    @Transactional
    public void getAllTransactionsByMotifIsNotEqualToSomething() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where motif not equals to DEFAULT_MOTIF
        defaultTransactionShouldNotBeFound("motif.notEquals=" + DEFAULT_MOTIF);

        // Get all the transactionList where motif not equals to UPDATED_MOTIF
        defaultTransactionShouldBeFound("motif.notEquals=" + UPDATED_MOTIF);
    }

    @Test
    @Transactional
    public void getAllTransactionsByMotifIsInShouldWork() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where motif in DEFAULT_MOTIF or UPDATED_MOTIF
        defaultTransactionShouldBeFound("motif.in=" + DEFAULT_MOTIF + "," + UPDATED_MOTIF);

        // Get all the transactionList where motif equals to UPDATED_MOTIF
        defaultTransactionShouldNotBeFound("motif.in=" + UPDATED_MOTIF);
    }

    @Test
    @Transactional
    public void getAllTransactionsByMotifIsNullOrNotNull() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where motif is not null
        defaultTransactionShouldBeFound("motif.specified=true");

        // Get all the transactionList where motif is null
        defaultTransactionShouldNotBeFound("motif.specified=false");
    }
                @Test
    @Transactional
    public void getAllTransactionsByMotifContainsSomething() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where motif contains DEFAULT_MOTIF
        defaultTransactionShouldBeFound("motif.contains=" + DEFAULT_MOTIF);

        // Get all the transactionList where motif contains UPDATED_MOTIF
        defaultTransactionShouldNotBeFound("motif.contains=" + UPDATED_MOTIF);
    }

    @Test
    @Transactional
    public void getAllTransactionsByMotifNotContainsSomething() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where motif does not contain DEFAULT_MOTIF
        defaultTransactionShouldNotBeFound("motif.doesNotContain=" + DEFAULT_MOTIF);

        // Get all the transactionList where motif does not contain UPDATED_MOTIF
        defaultTransactionShouldBeFound("motif.doesNotContain=" + UPDATED_MOTIF);
    }


    @Test
    @Transactional
    public void getAllTransactionsByValeurDeviseIsEqualToSomething() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where valeurDevise equals to DEFAULT_VALEUR_DEVISE
        defaultTransactionShouldBeFound("valeurDevise.equals=" + DEFAULT_VALEUR_DEVISE);

        // Get all the transactionList where valeurDevise equals to UPDATED_VALEUR_DEVISE
        defaultTransactionShouldNotBeFound("valeurDevise.equals=" + UPDATED_VALEUR_DEVISE);
    }

    @Test
    @Transactional
    public void getAllTransactionsByValeurDeviseIsNotEqualToSomething() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where valeurDevise not equals to DEFAULT_VALEUR_DEVISE
        defaultTransactionShouldNotBeFound("valeurDevise.notEquals=" + DEFAULT_VALEUR_DEVISE);

        // Get all the transactionList where valeurDevise not equals to UPDATED_VALEUR_DEVISE
        defaultTransactionShouldBeFound("valeurDevise.notEquals=" + UPDATED_VALEUR_DEVISE);
    }

    @Test
    @Transactional
    public void getAllTransactionsByValeurDeviseIsInShouldWork() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where valeurDevise in DEFAULT_VALEUR_DEVISE or UPDATED_VALEUR_DEVISE
        defaultTransactionShouldBeFound("valeurDevise.in=" + DEFAULT_VALEUR_DEVISE + "," + UPDATED_VALEUR_DEVISE);

        // Get all the transactionList where valeurDevise equals to UPDATED_VALEUR_DEVISE
        defaultTransactionShouldNotBeFound("valeurDevise.in=" + UPDATED_VALEUR_DEVISE);
    }

    @Test
    @Transactional
    public void getAllTransactionsByValeurDeviseIsNullOrNotNull() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where valeurDevise is not null
        defaultTransactionShouldBeFound("valeurDevise.specified=true");

        // Get all the transactionList where valeurDevise is null
        defaultTransactionShouldNotBeFound("valeurDevise.specified=false");
    }

    @Test
    @Transactional
    public void getAllTransactionsByValeurDeviseIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where valeurDevise is greater than or equal to DEFAULT_VALEUR_DEVISE
        defaultTransactionShouldBeFound("valeurDevise.greaterThanOrEqual=" + DEFAULT_VALEUR_DEVISE);

        // Get all the transactionList where valeurDevise is greater than or equal to UPDATED_VALEUR_DEVISE
        defaultTransactionShouldNotBeFound("valeurDevise.greaterThanOrEqual=" + UPDATED_VALEUR_DEVISE);
    }

    @Test
    @Transactional
    public void getAllTransactionsByValeurDeviseIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where valeurDevise is less than or equal to DEFAULT_VALEUR_DEVISE
        defaultTransactionShouldBeFound("valeurDevise.lessThanOrEqual=" + DEFAULT_VALEUR_DEVISE);

        // Get all the transactionList where valeurDevise is less than or equal to SMALLER_VALEUR_DEVISE
        defaultTransactionShouldNotBeFound("valeurDevise.lessThanOrEqual=" + SMALLER_VALEUR_DEVISE);
    }

    @Test
    @Transactional
    public void getAllTransactionsByValeurDeviseIsLessThanSomething() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where valeurDevise is less than DEFAULT_VALEUR_DEVISE
        defaultTransactionShouldNotBeFound("valeurDevise.lessThan=" + DEFAULT_VALEUR_DEVISE);

        // Get all the transactionList where valeurDevise is less than UPDATED_VALEUR_DEVISE
        defaultTransactionShouldBeFound("valeurDevise.lessThan=" + UPDATED_VALEUR_DEVISE);
    }

    @Test
    @Transactional
    public void getAllTransactionsByValeurDeviseIsGreaterThanSomething() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList where valeurDevise is greater than DEFAULT_VALEUR_DEVISE
        defaultTransactionShouldNotBeFound("valeurDevise.greaterThan=" + DEFAULT_VALEUR_DEVISE);

        // Get all the transactionList where valeurDevise is greater than SMALLER_VALEUR_DEVISE
        defaultTransactionShouldBeFound("valeurDevise.greaterThan=" + SMALLER_VALEUR_DEVISE);
    }


    @Test
    @Transactional
    public void getAllTransactionsByFicheTechniqueIsEqualToSomething() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);
        FicheTechnique ficheTechnique = FicheTechniqueResourceIT.createEntity(em);
        em.persist(ficheTechnique);
        em.flush();
        transaction.setFicheTechnique(ficheTechnique);
        transactionRepository.saveAndFlush(transaction);
        Long ficheTechniqueId = ficheTechnique.getId();

        // Get all the transactionList where ficheTechnique equals to ficheTechniqueId
        defaultTransactionShouldBeFound("ficheTechniqueId.equals=" + ficheTechniqueId);

        // Get all the transactionList where ficheTechnique equals to ficheTechniqueId + 1
        defaultTransactionShouldNotBeFound("ficheTechniqueId.equals=" + (ficheTechniqueId + 1));
    }


    @Test
    @Transactional
    public void getAllTransactionsByProduitIsEqualToSomething() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);
        Produit produit = ProduitResourceIT.createEntity(em);
        em.persist(produit);
        em.flush();
        transaction.setProduit(produit);
        transactionRepository.saveAndFlush(transaction);
        Long produitId = produit.getId();

        // Get all the transactionList where produit equals to produitId
        defaultTransactionShouldBeFound("produitId.equals=" + produitId);

        // Get all the transactionList where produit equals to produitId + 1
        defaultTransactionShouldNotBeFound("produitId.equals=" + (produitId + 1));
    }


    @Test
    @Transactional
    public void getAllTransactionsByCommandeIsEqualToSomething() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);
        Commande commande = CommandeResourceIT.createEntity(em);
        em.persist(commande);
        em.flush();
        transaction.setCommande(commande);
        transactionRepository.saveAndFlush(transaction);
        Long commandeId = commande.getId();

        // Get all the transactionList where commande equals to commandeId
        defaultTransactionShouldBeFound("commandeId.equals=" + commandeId);

        // Get all the transactionList where commande equals to commandeId + 1
        defaultTransactionShouldNotBeFound("commandeId.equals=" + (commandeId + 1));
    }


    @Test
    @Transactional
    public void getAllTransactionsByApprovisionnementIsEqualToSomething() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);
        Approvisionnement approvisionnement = ApprovisionnementResourceIT.createEntity(em);
        em.persist(approvisionnement);
        em.flush();
        transaction.setApprovisionnement(approvisionnement);
        transactionRepository.saveAndFlush(transaction);
        Long approvisionnementId = approvisionnement.getId();

        // Get all the transactionList where approvisionnement equals to approvisionnementId
        defaultTransactionShouldBeFound("approvisionnementId.equals=" + approvisionnementId);

        // Get all the transactionList where approvisionnement equals to approvisionnementId + 1
        defaultTransactionShouldNotBeFound("approvisionnementId.equals=" + (approvisionnementId + 1));
    }


    @Test
    @Transactional
    public void getAllTransactionsByLivraisonIsEqualToSomething() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);
        Livraison livraison = LivraisonResourceIT.createEntity(em);
        em.persist(livraison);
        em.flush();
        transaction.setLivraison(livraison);
        transactionRepository.saveAndFlush(transaction);
        Long livraisonId = livraison.getId();

        // Get all the transactionList where livraison equals to livraisonId
        defaultTransactionShouldBeFound("livraisonId.equals=" + livraisonId);

        // Get all the transactionList where livraison equals to livraisonId + 1
        defaultTransactionShouldNotBeFound("livraisonId.equals=" + (livraisonId + 1));
    }


    @Test
    @Transactional
    public void getAllTransactionsByVenteIsEqualToSomething() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);
        Vente vente = VenteResourceIT.createEntity(em);
        em.persist(vente);
        em.flush();
        transaction.setVente(vente);
        transactionRepository.saveAndFlush(transaction);
        Long venteId = vente.getId();

        // Get all the transactionList where vente equals to venteId
        defaultTransactionShouldBeFound("venteId.equals=" + venteId);

        // Get all the transactionList where vente equals to venteId + 1
        defaultTransactionShouldNotBeFound("venteId.equals=" + (venteId + 1));
    }


    @Test
    @Transactional
    public void getAllTransactionsByDeviseIsEqualToSomething() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);
        Devise devise = DeviseResourceIT.createEntity(em);
        em.persist(devise);
        em.flush();
        transaction.setDevise(devise);
        transactionRepository.saveAndFlush(transaction);
        Long deviseId = devise.getId();

        // Get all the transactionList where devise equals to deviseId
        defaultTransactionShouldBeFound("deviseId.equals=" + deviseId);

        // Get all the transactionList where devise equals to deviseId + 1
        defaultTransactionShouldNotBeFound("deviseId.equals=" + (deviseId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTransactionShouldBeFound(String filter) throws Exception {
        restTransactionMockMvc.perform(get("/api/transactions?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(transaction.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())))
            .andExpect(jsonPath("$.[*].quantite").value(hasItem(DEFAULT_QUANTITE)))
            .andExpect(jsonPath("$.[*].prixUnitaire").value(hasItem(DEFAULT_PRIX_UNITAIRE.intValue())))
            .andExpect(jsonPath("$.[*].typeTransaction").value(hasItem(DEFAULT_TYPE_TRANSACTION.toString())))
            .andExpect(jsonPath("$.[*].etat").value(hasItem(DEFAULT_ETAT.toString())))
            .andExpect(jsonPath("$.[*].motif").value(hasItem(DEFAULT_MOTIF)))
            .andExpect(jsonPath("$.[*].valeurDevise").value(hasItem(DEFAULT_VALEUR_DEVISE.doubleValue())));

        // Check, that the count call also returns 1
        restTransactionMockMvc.perform(get("/api/transactions/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTransactionShouldNotBeFound(String filter) throws Exception {
        restTransactionMockMvc.perform(get("/api/transactions?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTransactionMockMvc.perform(get("/api/transactions/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingTransaction() throws Exception {
        // Get the transaction
        restTransactionMockMvc.perform(get("/api/transactions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTransaction() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        int databaseSizeBeforeUpdate = transactionRepository.findAll().size();

        // Update the transaction
        Transaction updatedTransaction = transactionRepository.findById(transaction.getId()).get();
        // Disconnect from session so that the updates on updatedTransaction are not directly saved in db
        em.detach(updatedTransaction);
        updatedTransaction
            .code(UPDATED_CODE)
            .date(UPDATED_DATE)
            .quantite(UPDATED_QUANTITE)
            .prixUnitaire(UPDATED_PRIX_UNITAIRE)
            .typeTransaction(UPDATED_TYPE_TRANSACTION)
            .etat(UPDATED_ETAT)
            .motif(UPDATED_MOTIF)
            .valeurDevise(UPDATED_VALEUR_DEVISE);
        TransactionDTO transactionDTO = transactionMapper.toDto(updatedTransaction);

        restTransactionMockMvc.perform(put("/api/transactions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(transactionDTO)))
            .andExpect(status().isOk());

        // Validate the Transaction in the database
        List<Transaction> transactionList = transactionRepository.findAll();
        assertThat(transactionList).hasSize(databaseSizeBeforeUpdate);
        Transaction testTransaction = transactionList.get(transactionList.size() - 1);
        assertThat(testTransaction.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testTransaction.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testTransaction.getQuantite()).isEqualTo(UPDATED_QUANTITE);
        assertThat(testTransaction.getPrixUnitaire()).isEqualTo(UPDATED_PRIX_UNITAIRE);
        assertThat(testTransaction.getTypeTransaction()).isEqualTo(UPDATED_TYPE_TRANSACTION);
        assertThat(testTransaction.getEtat()).isEqualTo(UPDATED_ETAT);
        assertThat(testTransaction.getMotif()).isEqualTo(UPDATED_MOTIF);
        assertThat(testTransaction.getValeurDevise()).isEqualTo(UPDATED_VALEUR_DEVISE);
    }

    @Test
    @Transactional
    public void updateNonExistingTransaction() throws Exception {
        int databaseSizeBeforeUpdate = transactionRepository.findAll().size();

        // Create the Transaction
        TransactionDTO transactionDTO = transactionMapper.toDto(transaction);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTransactionMockMvc.perform(put("/api/transactions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(transactionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Transaction in the database
        List<Transaction> transactionList = transactionRepository.findAll();
        assertThat(transactionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTransaction() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        int databaseSizeBeforeDelete = transactionRepository.findAll().size();

        // Delete the transaction
        restTransactionMockMvc.perform(delete("/api/transactions/{id}", transaction.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Transaction> transactionList = transactionRepository.findAll();
        assertThat(transactionList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
