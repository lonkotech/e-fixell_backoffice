package bf.e_fixell_backoffice.service;

import bf.e_fixell_backoffice.domain.*;
import bf.e_fixell_backoffice.domain.enumeration.Etat;
import bf.e_fixell_backoffice.domain.enumeration.Statut;
import bf.e_fixell_backoffice.domain.enumeration.TypeCaisse;
import bf.e_fixell_backoffice.repository.*;
import bf.e_fixell_backoffice.service.dto.MResponse;
import bf.e_fixell_backoffice.service.dto.PersonnelDTO;
import bf.e_fixell_backoffice.service.dto.SessionCaisseDTO;
import bf.e_fixell_backoffice.service.dto.UserDTO;
import bf.e_fixell_backoffice.service.mapper.PersonnelMapper;
import bf.e_fixell_backoffice.service.mapper.SessionCaisseMapper;
import bf.e_fixell_backoffice.web.rest.errors.CustomParameterizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Personnel}.
 */
@Service
@Transactional
public class PersonnelService {

    private final Logger log = LoggerFactory.getLogger(PersonnelService.class);

    private final PersonnelRepository personnelRepository;
    private final HistoriqueAffectationRepository historiqueAffectationRepository;
    private final FonctionRepository fonctionRepository;
    private final UserService userService;
    private final UserRepository userRepository;
    private final PersonnelMapper personnelMapper;
    private final SessionCaisseMapper sessionCaisseMapper;
    private final SessionCaisseService sessionCaisseService;
    private final SessionCaisseRepository sessionCaisseRepository;
    private final CaisseRepository caisseRepository;
    private final PaiementRepository paiementRepository;

    public PersonnelService(PersonnelRepository personnelRepository, HistoriqueAffectationRepository historiqueAffectationRepository, FonctionRepository fonctionRepository, UserService userService, UserRepository userRepository, PersonnelMapper personnelMapper, SessionCaisseMapper sessionCaisseMapper, SessionCaisseService sessionCaisseService, SessionCaisseRepository sessionCaisseRepository, CaisseRepository caisseRepository, PaiementRepository paiementRepository) {
        this.personnelRepository = personnelRepository;
        this.historiqueAffectationRepository = historiqueAffectationRepository;
        this.fonctionRepository = fonctionRepository;
        this.userService = userService;
        this.userRepository = userRepository;
        this.personnelMapper = personnelMapper;
        this.sessionCaisseMapper = sessionCaisseMapper;
        this.sessionCaisseService = sessionCaisseService;
        this.sessionCaisseRepository = sessionCaisseRepository;
        this.caisseRepository = caisseRepository;
        this.paiementRepository = paiementRepository;
    }

    /**
     * Save a personnel.
     *
     * @param personnelDTO the entity to save.
     * @return the persisted entity.
     */
    public PersonnelDTO save(PersonnelDTO personnelDTO) {
        log.debug("Request to save Personnel : {}", personnelDTO);
        Personnel personnel = personnelMapper.toEntity(personnelDTO);
        HistoriqueAffectation historiqueAffectation = new HistoriqueAffectation();
        if (personnelDTO.getFonctionId() != null) {
            Fonction fonction = fonctionRepository.getOne(personnelDTO.getFonctionId());
            if (fonction.getId() != null) {
                if (personnelDTO.getSalaire().compareTo(fonction.getSalaireMin()) >= 0 && personnelDTO.getSalaire().compareTo(fonction.getSalaireMax()) <= 0) {
                    personnel = personnelRepository.save(personnel);
                    historiqueAffectation.setFonction(fonction);
                    historiqueAffectation.setDateDebut(Instant.now());
                    historiqueAffectation.setPersonnel(personnel);
                    historiqueAffectation.setSalaire(personnelDTO.getSalaire());
                    historiqueAffectation = historiqueAffectationRepository.save(historiqueAffectation);
                    if (historiqueAffectation.getId() != null) {
                        if (personnelDTO.getLogin() != null) {
                            savePersonneToUser(personnel, personnelDTO.getLogin(), personnelDTO.getPassword(), personnelDTO.getProfilId());
                        }
                    }
                } else {
                    throw new CustomParameterizedException("Salaire en dessous de la grille salariale ");
                }
            } else {
                throw new CustomParameterizedException("Fonction innexistante");
            }
        } else {
            throw new CustomParameterizedException("Fonction innexistante");
        }
        return personnelMapper.toDto(personnel);
    }

    /**
     * Get all the personnel.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<PersonnelDTO> findAll(PersonnelDTO personnelDTO, Pageable pageable) {
        log.debug("Request to get all Personnel " + personnelDTO);
        Page<Personnel> personnels = personnelRepository.findAllWithCriteria(personnelDTO.getNom(), personnelDTO.getPrenom(), personnelDTO.getTelephone(), personnelDTO.getMatricule(), personnelDTO.getFonctionId(), pageable);
        List<PersonnelDTO> list = new ArrayList<>();
        PersonnelDTO personnelDTO1 = new PersonnelDTO();
        for (Personnel personnel : personnels.getContent()) {
            HistoriqueAffectation historiqueAffectation = historiqueAffectationRepository.findByPersonnelIdAndDateFinIsNull(personnel.getId());
            personnelDTO1 = personnelMapper.toDto(personnel);
            personnelDTO1 = personnelMapper.toDto(personnel);

            if (historiqueAffectation != null && historiqueAffectation.getId() != null) {
                if (historiqueAffectation.getFonction() != null) {
                    personnelDTO1.setFonctionId(historiqueAffectation.getFonction().getId());
                    personnelDTO1.setFonctionNom(historiqueAffectation.getFonction().getLibelle());
                    list.add(personnelDTO1);
                    personnelDTO1 = new PersonnelDTO();
                }
            }
        }
        return new PageImpl<PersonnelDTO>(list, pageable, list.size());
    }

    public List<PersonnelDTO> findDetailPersonne(Page<Personnel> liste) {
        List<PersonnelDTO> list = new ArrayList<>();
        PersonnelDTO personnelDTO1 = new PersonnelDTO();
        for (Personnel personnel : liste.getContent()) {
            HistoriqueAffectation historiqueAffectation = historiqueAffectationRepository.findByPersonnelIdAndDateFinIsNull(personnel.getId());
            personnelDTO1 = personnelMapper.toDto(personnel);
            if (historiqueAffectation != null && historiqueAffectation.getId() != null) {
                if (historiqueAffectation.getFonction() != null) {
                    personnelDTO1.setFonctionId(historiqueAffectation.getFonction().getId());
                    personnelDTO1.setFonctionNom(historiqueAffectation.getFonction().getLibelle());
                    list.add(personnelDTO1);
                    personnelDTO1 = new PersonnelDTO();
                }
            }
        }
        return list;
    }

    @Transactional
    public PersonnelDTO majPersonnel(PersonnelDTO personnelDTO) {
        log.debug("Request to update Personnel : {}", personnelDTO);
        Personnel personnel = personnelMapper.toEntity(personnelDTO);
        if (personnel.getId() != null) {
            HistoriqueAffectation olDHist = historiqueAffectationRepository.findByPersonnelIdAndDateFinIsNull(personnel.getId());
            log.debug("--------tr----" + personnelDTO.getFonctionId());
            log.debug("----hist-----" + olDHist.toString());
            if (personnelDTO.getFonctionId() != null && personnelDTO.getFonctionId().equals(olDHist.getFonction().getId())) {
                Fonction fonction = fonctionRepository.getOne(personnelDTO.getFonctionId());
                if (personnelDTO.getSalaire().compareTo(fonction.getSalaireMin()) > 0 && personnelDTO.getSalaire().compareTo(fonction.getSalaireMax()) < 0) {
                    personnel = personnelRepository.save(personnel);
                    return personnelMapper.toDto(personnel);
                } else {
                    throw new CustomParameterizedException("Salaire en dessous de la grille salariale ");
                }

            } else {
                HistoriqueAffectation oldHist = historiqueAffectationRepository.findByPersonnelIdAndDateFinIsNull(personnel.getId());
                Fonction newFonction = fonctionRepository.getOne(personnelDTO.getFonctionId());

                if (personnelDTO.getSalaire().compareTo(newFonction.getSalaireMin()) >= 0 && personnelDTO.getSalaire().compareTo(newFonction.getSalaireMax()) <= 0) {
                    if (oldHist.getId() != null) {
                        oldHist.setDateFin(Instant.now());
                        oldHist.setSalaire(personnelRepository.getOne(personnel.getId()).getSalaire());
                        historiqueAffectationRepository.save(oldHist);
                    }
                    HistoriqueAffectation newHistAffectation = new HistoriqueAffectation();
                    personnel = personnelRepository.save(personnel);
                    newHistAffectation.setFonction(newFonction);
                    newHistAffectation.setDateDebut(Instant.now());
                    newHistAffectation.setPersonnel(personnel);
                    newHistAffectation.setSalaire(personnelDTO.getSalaire());
                    historiqueAffectationRepository.save(newHistAffectation);
                    return personnelMapper.toDto(personnel);
                } else {
                    throw new CustomParameterizedException("Salaire en dessous de la grille salariale ");
                }
            }
        }
        return null;
    }


    /**
     * Get one personnel by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public PersonnelDTO findOne(Long id) {
        log.debug("Request to get Personnel : {}", id);
        Optional<Personnel> personnel = personnelRepository.findByIdAndDeletedIsFalse(id);
        if (personnel.isPresent()) {
            PersonnelDTO personnelDTO = personnelMapper.toDto(personnel.get());
            HistoriqueAffectation historiqueAffectation = historiqueAffectationRepository.findByPersonnelIdAndDateFinIsNull(personnel.get().getId());
            if (historiqueAffectation.getId() != null) {
                if (historiqueAffectation.getFonction() != null) {
                    personnelDTO.setFonctionId(historiqueAffectation.getFonction().getId());
                    personnelDTO.setFonctionNom(historiqueAffectation.getFonction().getLibelle());
                }
            }
            return personnelDTO;
        }
        return null;
    }

    /**
     * Delete the personnel by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Personnel : {}", id);
        Personnel personnel = personnelRepository.getOne(id);
        if (personnel.getId() != null) {
            personnel.setDeleted(true);
            User user = userRepository.getOne(personnel.getId());
            if (user.getId() != null) {
                user.setActivated(false);
                userRepository.save(user);
            }
            personnelRepository.save(personnel);

        }
    }

    public Optional<PersonnelDTO> enableOrDesablePersonne(PersonnelDTO personnelDTO) {
        return Optional.of(personnelRepository
            .findById(personnelDTO.getId()))
            .filter(Optional::isPresent)
            .map(Optional::get)
            .map(personnel1 -> {
                personnel1.setActivated(personnelDTO.isActivated());
                User user = userRepository.findByPersonnelId(personnel1.getId());
                if (!personnelDTO.isActivated()) {
                    if (user.getId() != null) {
                        user.setActivated(false);
                        userRepository.save(user);
                    }
                }
                log.debug("Changed Information for Personnel: {}", personnel1);
                return personnel1;
            })
            .map(PersonnelDTO::new);
    }

    private void savePersonneToUser(Personnel personnel, String login, String password, Long profilId) {
        UserDTO userDTO = new UserDTO();
        userDTO.setPersonnelId(personnel.getId());
        userDTO.setLogin(login);
        userDTO.setLastName(personnel.getNom());
        userDTO.setFirstName(personnel.getPrenom());
        userDTO.setProfilId(profilId);
        userDTO.setPassword(password);
        userDTO.setActivated(false);
        userService.createUser(userDTO);
    }

    @Transactional
    public MResponse paiementSalarial(List<Long> pIdList, String mois) {

        Optional<SessionCaisseDTO> sessionCaisseDTO;
        Optional<User> user = userService.getUserWithAuthorities();
        sessionCaisseDTO = sessionCaisseService.findByUserAndStatut(user.get(), Statut.OUVERT);
        Paiement paiement1 = new Paiement();
        if (sessionCaisseDTO != null) {
            SessionCaisseDTO s = sessionCaisseDTO.get();
            Long caisseId = sessionCaisseDTO.get().getCaisseId();
            Caisse caisse = caisseRepository.findById(caisseId).get();
            if (caisse != null && caisse.getTypeCaisse().equals(TypeCaisse.CENTRALE)) {
                if (mois == null)
                    mois = LocalDate.now().getMonth().toString();
                for (Long id : pIdList) {
                    Personnel p = personnelRepository.findById(id).get();
                    if (p != null) {
                        Paiement paiement = new Paiement();
                        paiement.setMotif("Paiement salarial du moins de " + mois + " du matricule " + p.getMatricule());
                        paiement.setMontant(p.getSalaire());
                        paiement.setNumeroCompte(p.getNumeroCompte());
                        paiement.setBanque(p.getBanque().getLibelle());
                        paiement.setPersonnel(p);
                        paiement.setMontant(p.getSalaire());
                        paiement.setDate(Instant.now());
                        paiement.setEtat(Etat.VALIDER);
                        paiement.setSessioncaisse(sessionCaisseMapper.toEntity(sessionCaisseDTO.get()));
                        paiement1 = paiementRepository.save(paiement);

                        s.setSommeFin(s.getSommeFin().subtract(paiement.getMontant()));
                    }
                    sessionCaisseRepository.save(sessionCaisseMapper.toEntity(s));
                }
                return new MResponse("0", "paiement réussi", "OK", paiement1.getId());
            } else {
                throw new  RuntimeException("paiement echoué car caisse n'est pas de type central");
            }
        } else {
            throw  new RuntimeException("Pas de session de caisse central ouvert");
        }
    }
}
