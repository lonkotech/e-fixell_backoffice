package bf.e_fixell_backoffice.service.dto;

import java.time.ZonedDateTime;
import java.util.List;

public class BrouillardCaisseDTO {
    private ZonedDateTime dateDebut;
    private ZonedDateTime dateFin;
    private Long caisseId;
    private List<OperationCaisseDTO> listeOperation;
    private List<PaiementDTO> listePaiement;

    public List<OperationCaisseDTO> getListeOperation() {
        return listeOperation;
    }

    public void setListeOperation(List<OperationCaisseDTO> listeOperation) {
        this.listeOperation = listeOperation;
    }

    public List<PaiementDTO> getListePaiement() {
        return listePaiement;
    }

    public void setListePaiement(List<PaiementDTO> listePaiement) {
        this.listePaiement = listePaiement;
    }

    public ZonedDateTime getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(ZonedDateTime dateDebut) {
        this.dateDebut = dateDebut;
    }

    public ZonedDateTime getDateFin() {
        return dateFin;
    }

    public void setDateFin(ZonedDateTime dateFin) {
        this.dateFin = dateFin;
    }

    public Long getCaisseId() {
        return caisseId;
    }

    public void setCaisseId(Long caisseId) {
        this.caisseId = caisseId;
    }
}
