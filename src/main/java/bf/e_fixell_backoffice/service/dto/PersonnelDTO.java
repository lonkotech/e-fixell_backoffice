package bf.e_fixell_backoffice.service.dto;

import bf.e_fixell_backoffice.domain.Personnel;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * A DTO for the {@link Personnel} entity.
 */
public class PersonnelDTO implements Serializable {

    private Long id;

    private boolean deleted;

    private String nom;

    private String prenom;

    private String telephone;

    private String matricule;

    private BigDecimal salaire;

    private Long fonctionId;

    private String fonctionNom;

    private boolean activated;

    private boolean isUser;

    private String bic;

    private String iban;

    private Long banqueId;

    private String banqueLibelle;

    private String numeroCompte;

    private String login;

    private String password;

    private Long profilId;

    public PersonnelDTO(Personnel personnel) {
        this.id = personnel.getId();
        this.deleted = personnel.getDeleted();
        this.nom = personnel.getNom();
        this.prenom = personnel.getPrenom();
        this.telephone = personnel.getTelephone();
        this.matricule = personnel.getMatricule();
        this.salaire = personnel.getSalaire();
        this.activated = personnel.isActivated();
    }

    public PersonnelDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getMatricule() {
        return matricule;
    }

    public void setMatricule(String matricule) {
        this.matricule = matricule;
    }

    public BigDecimal getSalaire() {
        return salaire;
    }

    public void setSalaire(BigDecimal salaire) {
        this.salaire = salaire;
    }

    public boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Long getFonctionId() {
        return fonctionId;
    }

    public void setFonctionId(Long fonctionId) {
        this.fonctionId = fonctionId;
    }

    public String getFonctionNom() {
        return fonctionNom;
    }

    public void setFonctionNom(String fonctionNom) {
        this.fonctionNom = fonctionNom;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public String getBanqueLibelle() {
        return banqueLibelle;
    }

    public void setBanqueLibelle(String banqueLibelle) {
        this.banqueLibelle = banqueLibelle;
    }

    public Long getBanqueId() {
        return banqueId;
    }

    public void setBanqueId(Long banqueId) {
        this.banqueId = banqueId;
    }

    public String getNumeroCompte() {
        return numeroCompte;
    }

    public void setNumeroCompte(String numeroCompte) {
        this.numeroCompte = numeroCompte;
    }

    public String getBic() {
        return bic;
    }

    public void setBic(String bic) {
        this.bic = bic;
    }

    public boolean isUser() {
        return isUser;
    }

    public void setUser(boolean user) {
        isUser = user;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getProfilId() {
        return profilId;
    }

    public void setProfilId(Long profilId) {
        this.profilId = profilId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PersonnelDTO)) {
            return false;
        }

        return id != null && id.equals(((PersonnelDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore

    @Override
    public String toString() {
        return "PersonnelDTO{" +
            "id=" + id +
            ", deleted=" + deleted +
            ", nom='" + nom + '\'' +
            ", prenom='" + prenom + '\'' +
            ", telephone='" + telephone + '\'' +
            ", matricule='" + matricule + '\'' +
            ", salaire=" + salaire +
            ", fonctionId=" + fonctionId +
            ", fonctionNom='" + fonctionNom + '\'' +
            ", activated=" + activated +
            ", bic='" + bic + '\'' +
            ", banqueLibelle=" + banqueLibelle +
            ", banqueId=" + banqueId +
            ", numeroCompte='" + numeroCompte + '\'' +
            '}';
    }
}
