package bf.e_fixell_backoffice.service.dto;

public class AjusterStockOperationDTO {

    private Long id;

    private Integer stockTheorique;

    private Integer stockPhysique;

    private ProduitDTO produitDTO;

    private Long produitId;

    private String produitLibelle;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getStockTheorique() {
        return stockTheorique;
    }

    public void setStockTheorique(Integer stockTheorique) {
        this.stockTheorique = stockTheorique;
    }

    public Integer getStockPhysique() {
        return stockPhysique;
    }

    public void setStockPhysique(Integer stockPhysique) {
        this.stockPhysique = stockPhysique;
    }

    public ProduitDTO getProduitDTO() {
        return produitDTO;
    }

    public void setProduitDTO(ProduitDTO produitDTO) {
        this.produitDTO = produitDTO;
    }

    public Long getProduitId() {
        return produitId;
    }

    public void setProduitId(Long produitId) {
        this.produitId = produitId;
    }

    public String getProduitLibelle() {
        return produitLibelle;
    }

    public void setProduitLibelle(String produitLibelle) {
        this.produitLibelle = produitLibelle;
    }

    @Override
    public String toString() {
        return "AjusterStockOperationDTO{" +
            "id=" + id +
            ", stockTheorique=" + stockTheorique +
            ", stockPhysique=" + stockPhysique +
            ", produitDTO=" + produitDTO +
            ", produitId=" + produitId +
            ", produitLibelle=" + produitId +
            '}';
    }
}
