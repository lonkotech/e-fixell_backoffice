package bf.e_fixell_backoffice.service.dto;
import java.time.Instant;


public class StatistiqueDTO {

    private Instant dateDebut;

    private Instant dateFin;

    private String libelleProduit;

    private int quantiteVente;

    private int quantiteCommandeClient;

    private int quantiteCommandeFournisseur;

    public Instant getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Instant dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Instant getDateFin() {
        return dateFin;
    }

    public void setDateFin(Instant dateFin) {
        this.dateFin = dateFin;
    }

    public String getLibelleProduit() {
        return libelleProduit;
    }

    public void setLibelleProduit(String libelleProduit) {
        this.libelleProduit = libelleProduit;
    }

    public int getQuantiteVente() {
        return quantiteVente;
    }

    public void setQuantiteVente(int quantiteVente) {
        this.quantiteVente = quantiteVente;
    }

    public int getQuantiteCommandeClient() {
        return quantiteCommandeClient;
    }

    public void setQuantiteCommandeClient(int quantiteCommandeClient) {
        this.quantiteCommandeClient = quantiteCommandeClient;
    }

    public int getQuantiteCommandeFournisseur() {
        return quantiteCommandeFournisseur;
    }

    public void setQuantiteCommandeFournisseur(int quantiteCommandeFournisseur) {
        this.quantiteCommandeFournisseur = quantiteCommandeFournisseur;
    }
}
