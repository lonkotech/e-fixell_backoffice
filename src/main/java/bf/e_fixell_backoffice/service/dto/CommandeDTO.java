package bf.e_fixell_backoffice.service.dto;

import bf.e_fixell_backoffice.domain.Commande;
import bf.e_fixell_backoffice.domain.enumeration.Etat;
import bf.e_fixell_backoffice.domain.enumeration.Statut;
import bf.e_fixell_backoffice.domain.enumeration.TypeCommande;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

/**
 * A DTO for the {@link bf.e_fixell_backoffice.domain.Commande} entity.
 */
public class CommandeDTO implements Serializable {

    private Long id;

    private boolean deleted;

    private String code;

    private String libelle;

    private String raisonSocial;

    private Instant date;

    private BigDecimal somme;

    private Instant dateLivraisonPrevu;

    private Instant dateLivraison;

    private Etat etat;

    private Statut statut = Statut.OUVERT;

    private String motif;

    private BigDecimal avance;

    private Boolean avanceEnPercent = Boolean.FALSE;

    private BigDecimal tva;

    private BigDecimal montantSolde;

    private Boolean tvaEnPercent = Boolean.TRUE;

    private Long fournisseurId;

    private Long commandeFournisseurId;

    private List<Long> listeCommandesClients;

    private String fournisseurNom;

    private String fournisseurPrenom;

    private List<TransactionDTO> transactions;

    private List<FraisDTO> frais;

    private TypeCommande typeCommande = TypeCommande.COMMANDE_CLIENT;

    private List<EtapeTransportDTO> etapeTransports;

    private ApprovisionnementDTO approvisionnement;

    private Long clientId;

    private String clientNom;

    private String clientPrenom;

    private Boolean estSolde;

    public CommandeDTO(Commande commande) {
        this.id = commande.getId();
        this.deleted = commande.getDeleted();
        this.code = commande.getCode();
        this.libelle = commande.getLibelle();
        this.date = commande.getDate();
        this.somme = commande.getSomme();
        this.dateLivraisonPrevu = commande.getDateLivraisonPrevu();
        this.dateLivraison = commande.getDateLivraison();
        this.etat = commande.getEtat();
        this.statut = commande.getStatut();
        this.motif = commande.getMotif();
        this.avance = commande.getAvance();
        this.avanceEnPercent = commande.getAvanceEnPercent();
        this.tva = commande.getTva();
        this.estSolde = commande.getEstSolde();
        this.tvaEnPercent = commande.getTvaEnPercent();
        this.montantSolde = commande.getMontantSolde();
        if (commande.getFournisseur() != null) {
            this.fournisseurId = commande.getFournisseur().getId();
            this.fournisseurNom = commande.getFournisseur().getNom();
            this.fournisseurPrenom = commande.getFournisseur().getNom();
        }
    }

    public CommandeDTO() {
    }

    public String getRaisonSocial() {
        return raisonSocial;
    }

    public void setRaisonSocial(String raisonSocial) {
        this.raisonSocial = raisonSocial;
    }

    public Boolean getEstSolde() {
        return estSolde;
    }

    public void setEstSolde(Boolean estSolde) {
        this.estSolde = estSolde;
    }

    public BigDecimal getMontantSolde() {
        return montantSolde;
    }

    public void setMontantSolde(BigDecimal montantSolde) {
        this.montantSolde = montantSolde;
    }

    public ApprovisionnementDTO getApprovisionnement() {
        return approvisionnement;
    }

    public void setApprovisionnement(ApprovisionnementDTO approvisionnement) {
        this.approvisionnement = approvisionnement;
    }

    public TypeCommande getTypeCommande() {
        return typeCommande;
    }

    public void setTypeCommande(TypeCommande typeCommande) {
        this.typeCommande = typeCommande;
    }

    public String getFournisseurNom() {
        return fournisseurNom;
    }

    public void setFournisseurNom(String fournisseurNom) {
        this.fournisseurNom = fournisseurNom;
    }

    public String getFournisseurPrenom() {
        return fournisseurPrenom;
    }

    public void setFournisseurPrenom(String fournisseurPrenom) {
        this.fournisseurPrenom = fournisseurPrenom;
    }

    public String getClientNom() {
        return clientNom;
    }

    public void setClientNom(String clientNom) {
        this.clientNom = clientNom;
    }

    public String getClientPrenom() {
        return clientPrenom;
    }

    public void setClientPrenom(String clientPrenom) {
        this.clientPrenom = clientPrenom;
    }

    public List<EtapeTransportDTO> getEtapeTransports() {
        return etapeTransports;
    }

    public void setEtapeTransports(List<EtapeTransportDTO> etapeTransports) {
        this.etapeTransports = etapeTransports;
    }

    public List<FraisDTO> getFrais() {
        return frais;
    }

    public void setFrais(List<FraisDTO> frais) {
        this.frais = frais;
    }

    public List<TransactionDTO> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<TransactionDTO> transactions) {
        this.transactions = transactions;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public BigDecimal getSomme() {
        return somme;
    }

    public void setSomme(BigDecimal somme) {
        this.somme = somme;
    }

    public Instant getDateLivraisonPrevu() {
        return dateLivraisonPrevu;
    }

    public void setDateLivraisonPrevu(Instant dateLivraisonPrevu) {
        this.dateLivraisonPrevu = dateLivraisonPrevu;
    }

    public Etat getEtat() {
        return etat;
    }

    public void setEtat(Etat etat) {
        this.etat = etat;
    }

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    public BigDecimal getAvance() {
        return avance;
    }

    public void setAvance(BigDecimal avance) {
        this.avance = avance;
    }

    public Boolean isAvanceEnPercent() {
        return avanceEnPercent;
    }

    public void setAvanceEnPercent(Boolean avanceEnPercent) {
        this.avanceEnPercent = avanceEnPercent;
    }

    public Long getFournisseurId() {
        return fournisseurId;
    }

    public void setFournisseurId(Long fournisseurId) {
        this.fournisseurId = fournisseurId;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Instant getDateLivraison() {
        return dateLivraison;
    }

    public void setDateLivraison(Instant dateLivraison) {
        this.dateLivraison = dateLivraison;
    }

    public Statut getStatut() {
        return statut;
    }

    public void setStatut(Statut statut) {
        this.statut = statut;
    }

    public BigDecimal getTva() {
        return tva;
    }

    public void setTva(BigDecimal tva) {
        this.tva = tva;
    }

    public Boolean getTvaEnPercent() {
        return tvaEnPercent;
    }

    public void setTvaEnPercent(Boolean tvaEnPercent) {
        this.tvaEnPercent = tvaEnPercent;
    }

    public Long getCommandeFournisseurId() {
        return commandeFournisseurId;
    }

    public void setCommandeFournisseurId(Long commandeFournisseurId) {
        this.commandeFournisseurId = commandeFournisseurId;
    }

    public List<Long> getListeCommandesClients() {
        return listeCommandesClients;
    }

    public void setListeCommandesClients(List<Long> listeCommandesClients) {
        this.listeCommandesClients = listeCommandesClients;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CommandeDTO)) {
            return false;
        }

        return id != null && id.equals(((CommandeDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CommandeDTO{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", libelle='" + getLibelle() + "'" +
            ", date='" + getDate() + "'" +
            ", somme=" + getSomme() +
            ", dateLivraisonPrevu='" + getDateLivraisonPrevu() + "'" +
            ", dateLivraison='" + getDateLivraison() + "'" +
            ", etat='" + getEtat() + "'" +
            ", statut='" + getStatut() + "'" +
            ", motif='" + getMotif() + "'" +
            ", avance=" + getAvance() +
            ", tva=" + getTva() +
            ", avanceEnPercent='" + isAvanceEnPercent() + "'" +
            ", tvaEnPercent='" + getTvaEnPercent() + "'" +
            ", fournisseurId=" + getFournisseurId() +
            ", clientId=" + getClientId() +
            "}";


    }


}
