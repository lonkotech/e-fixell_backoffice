package bf.e_fixell_backoffice.service.dto;

public class MResponse {
    private String code;

    private String libelle;

    private String statut;

    private Long operationId;

    public MResponse() {
    }

    public MResponse(String code, String libelle, String statut) {
        this.code = code;
        this.libelle = libelle;
        this.statut = statut;
    }

    public MResponse(String code, String libelle, String statut, Long operationId) {
        this.code = code;
        this.libelle = libelle;
        this.statut = statut;
        this.operationId = operationId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public Long getOperationId() {
        return operationId;
    }

    public void setOperationId(Long operationId) {
        this.operationId = operationId;
    }
}
