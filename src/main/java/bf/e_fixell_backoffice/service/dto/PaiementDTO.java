package bf.e_fixell_backoffice.service.dto;

import bf.e_fixell_backoffice.domain.enumeration.Etat;

import java.time.Instant;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;

/**
 * A DTO for the {@link bf.e_fixell_backoffice.domain.Paiement} entity.
 */
public class PaiementDTO implements Serializable {

    private Long id;

    private boolean deleted;

    private String code;

    private Instant date;

    private BigDecimal montant;

    private Etat etat;

    private Long commandeId;

    private Long venteId;

    private Long sessioncaisseId;

    private Long personnelId;

    private String venteLibelle;

    private String commandeLibelle;

    private String numeroCompte;

    private String banque;

    private String caisseLibelle;

    private ZonedDateTime datePaiement;

    public String motif;

    private boolean affecteCaisse;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public Long getCommandeId() {
        return commandeId;
    }

    public void setCommandeId(Long commandeId) {
        this.commandeId = commandeId;
    }

    public Long getVenteId() {
        return venteId;
    }

    public void setVenteId(Long venteId) {
        this.venteId = venteId;
    }

    public Long getSessioncaisseId() {
        return sessioncaisseId;
    }

    public void setSessioncaisseId(Long sessionCaisseId) {
        this.sessioncaisseId = sessionCaisseId;
    }

    public boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Etat getEtat() {
        return etat;
    }

    public void setEtat(Etat etat) {
        this.etat = etat;
    }

    public String getVenteLibelle() {
        return venteLibelle;
    }

    public void setVenteLibelle(String venteLibelle) {
        this.venteLibelle = venteLibelle;
    }

    public String getCommandeLibelle() {
        return commandeLibelle;
    }

    public void setCommandeLibelle(String commandeLibelle) {
        this.commandeLibelle = commandeLibelle;
    }

    public String getCaisseLibelle() {
        return caisseLibelle;
    }

    public void setCaisseLibelle(String caisseLibelle) {
        this.caisseLibelle = caisseLibelle;
    }

    public ZonedDateTime getDatePaiement() {
        return datePaiement;
    }

    public void setDatePaiement(ZonedDateTime datePaiement) {
        this.datePaiement = datePaiement;
    }

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    public boolean isAffecteCaisse() {
        return affecteCaisse;
    }

    public void setAffecteCaisse(boolean affecteCaisse) {
        this.affecteCaisse = affecteCaisse;
    }

    public Long getPersonnelId() {
        return personnelId;
    }

    public void setPersonnelId(Long personnelId) {
        this.personnelId = personnelId;
    }

    public String getNumeroCompte() {
        return numeroCompte;
    }

    public void setNumeroCompte(String numeroCompte) {
        this.numeroCompte = numeroCompte;
    }

    public String getBanque() {
        return banque;
    }

    public void setBanque(String banque) {
        this.banque = banque;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PaiementDTO)) {
            return false;
        }

        return id != null && id.equals(((PaiementDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PaiementDTO{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", date='" + getDate() + "'" +
            ", montant=" + getMontant() +
            ", commandeId=" + getCommandeId() +
            ", venteId=" + getVenteId() +
            ", sessioncaisseId=" + getSessioncaisseId() +
            "}";
    }
}
