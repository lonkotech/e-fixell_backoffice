package bf.e_fixell_backoffice.service.dto;

import java.time.Instant;

public class EtapeTransportDTO {

    private Long id;

    private String libelle;

    private Long niveau;

    private Instant dateDepart;

    private Instant dateArrive;

    private boolean deleted;

    private CommandeDTO commandeDTO;

    private SocieteTransportDTO societeTransportDTO;

    private Long commandeId;

    private Long societeTransportId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Long getNiveau() {
        return niveau;
    }

    public void setNiveau(Long niveau) {
        this.niveau = niveau;
    }

    public Instant getDateDepart() {
        return dateDepart;
    }

    public void setDateDepart(Instant dateDepart) {
        this.dateDepart = dateDepart;
    }

    public Instant getDateArrive() {
        return dateArrive;
    }

    public void setDateArrive(Instant dateArrive) {
        this.dateArrive = dateArrive;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public CommandeDTO getCommandeDTO() {
        return commandeDTO;
    }

    public void setCommandeDTO(CommandeDTO commandeDTO) {
        this.commandeDTO = commandeDTO;
    }

    public SocieteTransportDTO getSocieteTransportDTO() {
        return societeTransportDTO;
    }

    public void setSocieteTransportDTO(SocieteTransportDTO societeTransportDTO) {
        this.societeTransportDTO = societeTransportDTO;
    }

    public Long getCommandeId() {
        return commandeId;
    }

    public void setCommandeId(Long commandeId) {
        this.commandeId = commandeId;
    }

    public Long getSocieteTransportId() {
        return societeTransportId;
    }

    public void setSocieteTransportId(Long societeTransportId) {
        this.societeTransportId = societeTransportId;
    }

    @Override
    public String toString() {
        return "EtapeTransportDTO{" +
            "id=" + id +
            ", libelle='" + libelle + '\'' +
            ", niveau=" + niveau +
            ", dateDepart=" + dateDepart +
            ", dateArrive=" + dateArrive +
            ", deleted=" + deleted +
            ", commandeDTO=" + commandeDTO +
            ", societeTransportDTO=" + societeTransportDTO +
            ", commandeId=" + commandeId +
            ", societeTransportId=" + societeTransportId +
            '}';
    }
}
