package bf.e_fixell_backoffice.service.dto;

import java.io.Serializable;
import java.util.Objects;

import bf.e_fixell_backoffice.domain.Fournisseur;
import bf.e_fixell_backoffice.domain.enumeration.TypePersonne;

/**
 * A DTO for the {@link bf.e_fixell_backoffice.domain.Fournisseur} entity.
 */
public class FournisseurDTO implements Serializable {

    private Long id;

    private boolean deleted;

    private String nom;

    private String prenom;

    private String raisonSocial;

    private String rue;

    private String telephone;

    private String fixe;

    private String codePostale;

    private String numeroReseauSocial;

    private TypePersonne typePersonne;

    private String numeroContrib;


    private Long adresseId;
    public FournisseurDTO() {
    }

    public FournisseurDTO(Fournisseur fournisseur) {
        this.id = fournisseur.getId();
        this.deleted = fournisseur.getDeleted();
        this.nom = fournisseur.getNom();
        this.prenom = fournisseur.getPrenom();
        this.raisonSocial = fournisseur.getRaisonSocial();
        this.rue = fournisseur.getRue();
        this.telephone = fournisseur.getTelephone();
        this.fixe = fournisseur.getFixe();
        this.codePostale = fournisseur.getCodePostale();
        this.numeroReseauSocial = fournisseur.getNumeroReseauSocial();
        this.typePersonne = fournisseur.getTypePersonne();
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getRaisonSocial() {
        return raisonSocial;
    }

    public void setRaisonSocial(String raisonSocial) {
        this.raisonSocial = raisonSocial;
    }

    public String getRue() {
        return rue;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getFixe() {
        return fixe;
    }

    public void setFixe(String fixe) {
        this.fixe = fixe;
    }

    public String getCodePostale() {
        return codePostale;
    }

    public void setCodePostale(String codePostale) {
        this.codePostale = codePostale;
    }

    public String getNumeroReseauSocial() {
        return numeroReseauSocial;
    }

    public void setNumeroReseauSocial(String numeroReseauSocial) {
        this.numeroReseauSocial = numeroReseauSocial;
    }

    public TypePersonne getTypePersonne() {
        return typePersonne;
    }

    public void setTypePersonne(TypePersonne typePersonne) {
        this.typePersonne = typePersonne;
    }

    public String getNumeroContrib() {
        return numeroContrib;
    }

    public void setNumeroContrib(String numeroContrib) {
        this.numeroContrib = numeroContrib;
    }

    public Long getAdresseId() {
        return adresseId;
    }

    public void setAdresseId(Long uniteOrganisationId) {
        this.adresseId = uniteOrganisationId;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FournisseurDTO fournisseurDTO = (FournisseurDTO) o;
        if (fournisseurDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), fournisseurDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "FournisseurDTO{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", prenom='" + getPrenom() + "'" +
            ", raisonSocial='" + getRaisonSocial() + "'" +
            ", rue='" + getRue() + "'" +
            ", telephone='" + getTelephone() + "'" +
            ", fixe='" + getFixe() + "'" +
            ", codePostale='" + getCodePostale() + "'" +
            ", numeroReseauSocial='" + getNumeroReseauSocial() + "'" +
            ", typePersonne='" + getTypePersonne() + "'" +
            ", numeroContrib='" + getNumeroContrib() + "'" +
            ", adresseId=" + getAdresseId() +
            "}";
    }
}
