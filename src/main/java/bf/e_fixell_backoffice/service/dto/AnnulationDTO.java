package bf.e_fixell_backoffice.service.dto;

import java.util.List;

public class AnnulationDTO {
    private Long id;

    private String motif;

    private List<Long> transactionId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    public List<Long> getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(List<Long> transactionId) {
        this.transactionId = transactionId;
    }
}
