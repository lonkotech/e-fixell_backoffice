package bf.e_fixell_backoffice.service.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;

/**
 * A DTO for the {@link bf.e_fixell_backoffice.domain.Perte} entity.
 */
public class DeviseDTO implements Serializable {

    private Long id;

    private String libelle;

    private String description;

    private String deviseBase;

    private BigDecimal valeur;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDeviseBase() {
        return deviseBase;
    }

    public void setDeviseBase(String deviseBase) {
        this.deviseBase = deviseBase;
    }

    public BigDecimal getValeur() {
        return valeur;
    }

    public void setValeur(BigDecimal valeur) {
        this.valeur = valeur;
    }
}
