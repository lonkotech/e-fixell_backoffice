package bf.e_fixell_backoffice.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.List;

/**
 * A DTO for the {@link bf.e_fixell_backoffice.domain.Inventaire} entity.
 */
public class InventaireDTO implements Serializable {

    private Long id;

    private Integer quantiteTheorique;

    private Integer quantitePhysique;

    private Integer ecart;

    private String commentaire;

    private String produitLibelle;


    private Long produitId;

    private List<TransactionDTO> transactions;

    private List<PerteDTO> pertes;

    private Instant startDate;

    private Instant endtDate;


    private String createdBy;

    private Instant createdDate;

    private String lastModifiedBy;

    private Instant lastModifiedDate;

    public Instant getStartDate() {
        return startDate;
    }

    public void setStartDate(Instant startDate) {
        this.startDate = startDate;
    }

    public Instant getEndtDate() {
        return endtDate;
    }

    public void setEndtDate(Instant endtDate) {
        this.endtDate = endtDate;
    }

    public String getProduitLibelle() {
        return produitLibelle;
    }

    public void setProduitLibelle(String produitLibelle) {
        this.produitLibelle = produitLibelle;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public List<TransactionDTO> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<TransactionDTO> transactions) {
        this.transactions = transactions;
    }

    public List<PerteDTO> getPertes() {
        return pertes;
    }

    public void setPertes(List<PerteDTO> pertes) {
        this.pertes = pertes;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getQuantiteTheorique() {
        return quantiteTheorique;
    }

    public void setQuantiteTheorique(Integer quantiteTheorique) {
        this.quantiteTheorique = quantiteTheorique;
    }

    public Integer getQuantitePhysique() {
        return quantitePhysique;
    }

    public void setQuantitePhysique(Integer quantitePhysique) {
        this.quantitePhysique = quantitePhysique;
    }

    public Integer getEcart() {
        return ecart;
    }

    public void setEcart(Integer ecart) {
        this.ecart = ecart;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public Long getProduitId() {
        return produitId;
    }

    public void setProduitId(Long produitId) {
        this.produitId = produitId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof InventaireDTO)) {
            return false;
        }

        return id != null && id.equals(((InventaireDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "InventaireDTO{" +
            "id=" + getId() +
            ", quantiteTheorique=" + getQuantiteTheorique() +
            ", quantitePhysique=" + getQuantitePhysique() +
            ", ecart=" + getEcart() +
            ", commentaire='" + getCommentaire() + "'" +
            ", produitId=" + getProduitId() +
            "}";
    }
}
