package bf.e_fixell_backoffice.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link bf.e_fixell_backoffice.domain.Caracteristique} entity.
 */
public class CaracteristiqueDTO implements Serializable {

    private Long id;

    private boolean deleted;

    private String libelle;

    private String valeur;

    private boolean isPaginated;


    private Long ficheTechniqueId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getValeur() {
        return valeur;
    }

    public void setValeur(String valeur) {
        this.valeur = valeur;
    }

    public Long getFicheTechniqueId() {
        return ficheTechniqueId;
    }

    public void setFicheTechniqueId(Long ficheTechniqueId) {
        this.ficheTechniqueId = ficheTechniqueId;
    }

    public boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isPaginated() {
        return isPaginated;
    }

    public void setPaginated(boolean paginated) {
        isPaginated = paginated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CaracteristiqueDTO)) {
            return false;
        }

        return id != null && id.equals(((CaracteristiqueDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CaracteristiqueDTO{" +
            "id=" + getId() +
            ", libelle='" + getLibelle() + "'" +
            ", valeur='" + getValeur() + "'" +
            ", ficheTechniqueId=" + getFicheTechniqueId() +
            "}";
    }
}
