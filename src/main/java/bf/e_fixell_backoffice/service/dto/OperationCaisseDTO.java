package bf.e_fixell_backoffice.service.dto;

import bf.e_fixell_backoffice.domain.enumeration.TypeOperationCaisse;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZonedDateTime;

/**
 * A DTO for the {@link bf.e_fixell_backoffice.domain.OperationCaisse} entity.
 */
public class OperationCaisseDTO implements Serializable {

    private Long id;

    private boolean deleted;

    private TypeOperationCaisse typeOperationCaisse;

    private BigDecimal montant;
    private BigDecimal soldeAvant;
    private BigDecimal soldeApres;
    private String description;
    private ZonedDateTime dateDebut;
    private ZonedDateTime dateFin;

    private Long caisseSrcId;

    private Long caisseDstId;

    private Long sessionCaisseId;

    private String caisseSrcLibelle;

    private String caisseDestLibelle;

    private Instant createdDate;

    public String getCaisseSrcLibelle() {
        return caisseSrcLibelle;
    }

    public void setCaisseSrcLibelle(String caisseSrcLibelle) {
        this.caisseSrcLibelle = caisseSrcLibelle;
    }

    public String getCaisseDestLibelle() {
        return caisseDestLibelle;
    }

    public void setCaisseDestLibelle(String caisseDestLibelle) {
        this.caisseDestLibelle = caisseDestLibelle;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TypeOperationCaisse getTypeOperationCaisse() {
        return typeOperationCaisse;
    }

    public void setTypeOperationCaisse(TypeOperationCaisse typeOperationCaisse) {
        this.typeOperationCaisse = typeOperationCaisse;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public Long getCaisseSrcId() {
        return caisseSrcId;
    }

    public void setCaisseSrcId(Long caisseId) {
        this.caisseSrcId = caisseId;
    }

    public Long getCaisseDstId() {
        return caisseDstId;
    }

    public void setCaisseDstId(Long caisseId) {
        this.caisseDstId = caisseId;
    }

    public BigDecimal getSoldeAvant() {
        return soldeAvant;
    }

    public void setSoldeAvant(BigDecimal soldeAvant) {
        this.soldeAvant = soldeAvant;
    }

    public BigDecimal getSoldeApres() {
        return soldeApres;
    }

    public void setSoldeApres(BigDecimal soldeApres) {
        this.soldeApres = soldeApres;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ZonedDateTime getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(ZonedDateTime dateDebut) {
        this.dateDebut = dateDebut;
    }

    public ZonedDateTime getDateFin() {
        return dateFin;
    }

    public void setDateFin(ZonedDateTime dateFin) {
        this.dateFin = dateFin;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Long getSessionCaisseId() {
        return sessionCaisseId;
    }

    public void setSessionCaisseId(Long sessionCaisseId) {
        this.sessionCaisseId = sessionCaisseId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OperationCaisseDTO)) {
            return false;
        }

        return id != null && id.equals(((OperationCaisseDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OperationCaisseDTO{" +
            "id=" + getId() +
            ", typeOperationCaisse='" + getTypeOperationCaisse() + "'" +
            ", montant=" + getMontant() +
            ", caisseSrcId=" + getCaisseSrcId() +
            ", caisseDstId=" + getCaisseDstId() +
            "}";
    }
}
