package bf.e_fixell_backoffice.service.dto;

public class DataParameter {

    private UserDTO userDTO;

    private String idToken;

    private Long totalVente;

    private int totalCommandeClient;

    private int totalCommandeFournisseur;

    private Long nbFournisseur;

    private Long nbClient;
    private SessionCaisseDTO sessionCaisseDTO;

    public Long getTotalVente() {
        return totalVente;
    }

    public void setTotalVente(Long totalVente) {
        this.totalVente = totalVente;
    }

    public int getTotalCommandeClient() {
        return totalCommandeClient;
    }

    public void setTotalCommandeClient(int totalCommandeClient) {
        this.totalCommandeClient = totalCommandeClient;
    }

    public int getTotalCommandeFournisseur() {
        return totalCommandeFournisseur;
    }

    public void setTotalCommandeFournisseur(int totalCommandeFournisseur) {
        this.totalCommandeFournisseur = totalCommandeFournisseur;
    }

    public Long getNbFournisseur() {
        return nbFournisseur;
    }

    public void setNbFournisseur(Long nbFournisseur) {
        this.nbFournisseur = nbFournisseur;
    }

    public Long getNbClient() {
        return nbClient;
    }

    public void setNbClient(Long nbClient) {
        this.nbClient = nbClient;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public String getIdToken() {
        return idToken;
    }

    public void setIdToken(String idToken) {
        this.idToken = idToken;
    }

    public SessionCaisseDTO getSessionCaisseDTO() {
        return sessionCaisseDTO;
    }

    public void setSessionCaisseDTO(SessionCaisseDTO sessionCaisseDTO) {
        this.sessionCaisseDTO = sessionCaisseDTO;
    }
}
