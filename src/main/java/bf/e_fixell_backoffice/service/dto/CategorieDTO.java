package bf.e_fixell_backoffice.service.dto;

import bf.e_fixell_backoffice.domain.Categorie;

import java.io.Serializable;

/**
 * A DTO for the {@link bf.e_fixell_backoffice.domain.Categorie} entity.
 */
public class CategorieDTO implements Serializable {

    private Long id;

    private boolean deleted;

    private String libelle;

    private String description;

    public CategorieDTO() {
    }

    public CategorieDTO(Categorie categorie){
        this.id=categorie.getId();
        this.libelle=categorie.getLibelle();
        this.description=categorie.getDescription();
        this.deleted=categorie.getDeleted();
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CategorieDTO)) {
            return false;
        }

        return id != null && id.equals(((CategorieDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CategorieDTO{" +
            "id=" + getId() +
            ", libelle='" + getLibelle() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
