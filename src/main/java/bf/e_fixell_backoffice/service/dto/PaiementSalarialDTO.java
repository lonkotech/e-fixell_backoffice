package bf.e_fixell_backoffice.service.dto;

import java.util.List;

public class PaiementSalarialDTO {

    private List<Long> listId;
    private String mois;

    public List<Long> getListId() {
        return listId;
    }

    public void setListId(List<Long> listId) {
        this.listId = listId;
    }

    public String getMois() {
        return mois;
    }

    public void setMois(String mois) {
        this.mois = mois;
    }
}
