package bf.e_fixell_backoffice.service.dto;

import bf.e_fixell_backoffice.domain.Transaction;
import bf.e_fixell_backoffice.domain.enumeration.Etat;
import bf.e_fixell_backoffice.domain.enumeration.TypeTransaction;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;

/**
 * A DTO for the {@link bf.e_fixell_backoffice.domain.Transaction} entity.
 */
public class TransactionDTO implements Serializable {

    private Long id;

    private boolean deleted;

    private String code;

    private Instant date;

    private Instant dateLivre;

    private Integer quantite;

    private Integer quantiteALivre;

    private Integer quantiteTotaleLivre;

    private Long transactionParentId;

    private BigDecimal prixUnitaire;

    private TypeTransaction typeTransaction;

    private Etat etat;

    private String motif;

    private Long ficheTechniqueId;

    private Integer decompte;

    private Long produitId;

    private String produitLibelle;

    private Long commandeId;

    private Long approvisionnementId;

    private Long livraisonId;

    private Long venteId;

    private Long inventaireId;

    private Double valeurDevise;

    private Long deviseId;

    private String deviseLibelle;

    private FicheTechniqueDTO ficheTechnique;

    private String caracteristiques;

    private BigDecimal prixAchatUnitaire;

    private BigDecimal prixVenteUnitaire;

    private BigDecimal prixVenteReelUnitaire;


    public TransactionDTO(Transaction transaction) {
        this.id = transaction.getId();
        this.deleted = transaction.getDeleted();
        this.code = transaction.getCode();
        this.date = transaction.getDate();
        this.quantite = transaction.getQuantite();
        this.quantiteALivre = transaction.getQuantiteALivre();
        this.prixUnitaire = transaction.getPrixUnitaire();
        this.prixVenteReelUnitaire = transaction.getPrixVenteReelUnitaire();
        this.typeTransaction = transaction.getTypeTransaction();
        this.etat = transaction.getEtat();
        this.motif = transaction.getMotif();
        this.valeurDevise = transaction.getValeurDevise();
        this.decompte = transaction.getDecompte();
        this.produitId = transaction.getProduit() != null ? transaction.getProduit().getId() : null;
        this.deviseId = transaction.getDevise() != null ? transaction.getDevise().getId() : null;
        this.deviseLibelle = transaction.getDevise() != null ? transaction.getDevise().getLibelle() : null;
        this.produitLibelle = transaction.getProduit() != null ? transaction.getProduit().getLibelle() : null;
        this.commandeId = transaction.getCommande() != null ? transaction.getCommande().getId() : null;
        this.approvisionnementId = transaction.getApprovisionnement() != null ? transaction.getApprovisionnement().getId() : null;
        this.venteId = transaction.getVente() != null ? transaction.getVente().getId() : null;
        this.caracteristiques = transaction.getCaracteristiques();
    }

    public TransactionDTO() {
    }

    public Double getValeurDevise() {
        return valeurDevise;
    }

    public void setValeurDevise(Double valeurDevise) {
        this.valeurDevise = valeurDevise;
    }

    public Long getDeviseId() {
        return deviseId;
    }

    public void setDeviseId(Long deviseId) {
        this.deviseId = deviseId;
    }

    public String getDeviseLibelle() {
        return deviseLibelle;
    }

    public void setDeviseLibelle(String deviseLibelle) {
        this.deviseLibelle = deviseLibelle;
    }

    public Long getInventaireId() {
        return inventaireId;
    }

    public void setInventaireId(Long inventaireId) {
        this.inventaireId = inventaireId;
    }

    public String getProduitLibelle() {
        return produitLibelle;
    }

    public void setProduitLibelle(String produitLibelle) {
        this.produitLibelle = produitLibelle;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public Integer getQuantite() {
        return quantite;
    }

    public void setQuantite(Integer quantite) {
        this.quantite = quantite;
    }

    public BigDecimal getPrixUnitaire() {
        return prixUnitaire;
    }

    public void setPrixUnitaire(BigDecimal prixUnitaire) {
        this.prixUnitaire = prixUnitaire;
    }

    public TypeTransaction getTypeTransaction() {
        return typeTransaction;
    }

    public void setTypeTransaction(TypeTransaction typeTransaction) {
        this.typeTransaction = typeTransaction;
    }

    public Etat getEtat() {
        return etat;
    }

    public void setEtat(Etat etat) {
        this.etat = etat;
    }

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    public Long getProduitId() {
        return produitId;
    }

    public void setProduitId(Long produitId) {
        this.produitId = produitId;
    }

    public Long getCommandeId() {
        return commandeId;
    }

    public void setCommandeId(Long commandeId) {
        this.commandeId = commandeId;
    }

    public Long getApprovisionnementId() {
        return approvisionnementId;
    }

    public void setApprovisionnementId(Long approvisionnementId) {
        this.approvisionnementId = approvisionnementId;
    }

    public Long getLivraisonId() {
        return livraisonId;
    }

    public void setLivraisonId(Long livraisonId) {
        this.livraisonId = livraisonId;
    }

    public Long getVenteId() {
        return venteId;
    }

    public void setVenteId(Long venteId) {
        this.venteId = venteId;
    }

    public FicheTechniqueDTO getFicheTechnique() {
        return ficheTechnique;
    }

    public void setFicheTechnique(FicheTechniqueDTO ficheTechnique) {
        this.ficheTechnique = ficheTechnique;
    }

    public Integer getDecompte() {
        return decompte;
    }

    public void setDecompte(Integer decompte) {
        this.decompte = decompte;
    }

    public boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Integer getQuantiteALivre() {
        return quantiteALivre;
    }

    public void setQuantiteALivre(Integer quantiteALivre) {
        this.quantiteALivre = quantiteALivre;
    }

    public String getCaracteristiques() {
        return caracteristiques;
    }

    public void setCaracteristiques(String caracteristiques) {
        this.caracteristiques = caracteristiques;
    }

    public BigDecimal getPrixAchatUnitaire() {
        return prixAchatUnitaire;
    }

    public void setPrixAchatUnitaire(BigDecimal prixAchatUnitaire) {
        this.prixAchatUnitaire = prixAchatUnitaire;
    }

    public BigDecimal getPrixVenteUnitaire() {
        return prixVenteUnitaire;
    }

    public void setPrixVenteUnitaire(BigDecimal prixVenteUnitaire) {
        this.prixVenteUnitaire = prixVenteUnitaire;
    }

    public Instant getDateLivre() {
        return dateLivre;
    }

    public void setDateLivre(Instant dateLivre) {
        this.dateLivre = dateLivre;
    }

    public BigDecimal getPrixVenteReelUnitaire() {
        return prixVenteReelUnitaire;
    }

    public void setPrixVenteReelUnitaire(BigDecimal prixVenteReelUnitaire) {
        this.prixVenteReelUnitaire = prixVenteReelUnitaire;
    }

    public Integer getQuantiteTotaleLivre() {
        return quantiteTotaleLivre;
    }

    public void setQuantiteTotaleLivre(Integer quantiteTotaleLivre) {
        this.quantiteTotaleLivre = quantiteTotaleLivre;
    }

    public Long getTransactionParentId() {
        return transactionParentId;
    }

    public void setTransactionParentId(Long transactionParentId) {
        this.transactionParentId = transactionParentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TransactionDTO)) {
            return false;
        }

        return id != null && id.equals(((TransactionDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "TransactionDTO{" +
            "id=" + id +
            ", deleted=" + deleted +
            ", code='" + code + '\'' +
            ", date=" + date +
            ", dateLivre=" + dateLivre +
            ", quantite=" + quantite +
            ", quantiteALivre=" + quantiteALivre +
            ", quantiteTotaleLivre=" + quantiteTotaleLivre +
            ", transactionParentId=" + transactionParentId +
            ", prixUnitaire=" + prixUnitaire +
            ", typeTransaction=" + typeTransaction +
            ", etat=" + etat +
            ", motif='" + motif + '\'' +
            ", ficheTechniqueId=" + ficheTechniqueId +
            ", decompte=" + decompte +
            ", produitId=" + produitId +
            ", produitLibelle='" + produitLibelle + '\'' +
            ", commandeId=" + commandeId +
            ", approvisionnementId=" + approvisionnementId +
            ", livraisonId=" + livraisonId +
            ", venteId=" + venteId +
            ", inventaireId=" + inventaireId +
            ", valeurDevise=" + valeurDevise +
            ", deviseId=" + deviseId +
            ", ficheTechnique=" + ficheTechnique +
            ", caracteristiques='" + caracteristiques + '\'' +
            ", prixAchatUnitaire=" + prixAchatUnitaire +
            ", prixVenteUnitaire=" + prixVenteUnitaire +
            ", prixVenteReelUnitaire=" + prixVenteReelUnitaire +
            '}';
    }
}
