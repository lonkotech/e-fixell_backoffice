package bf.e_fixell_backoffice.service.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

/**
 * A DTO for the {@link bf.e_fixell_backoffice.domain.Vente} entity.
 */
public class VenteDTO implements Serializable {

    private Long id;

    private boolean deleted;

    private String libelle;

    private String code;

    private Instant date;

    private BigDecimal montant;

    private BigDecimal reste;

    private BigDecimal avance;

    private BigDecimal prixReel;

    private Boolean solder;

    private Long clientId;

    private BigDecimal tva;

    private BigDecimal montantTVA;

    private Boolean tvaEnPercent;

    private BigDecimal montantSolder;

    private List<TransactionDTO> transactions;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public BigDecimal getReste() {
        return reste;
    }

    public void setReste(BigDecimal reste) {
        this.reste = reste;
    }

    public Boolean isSolder() {
        return solder;
    }

    public void setSolder(Boolean solder) {
        this.solder = solder;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public BigDecimal getPrixReel() {
        return prixReel;
    }

    public void setPrixReel(BigDecimal prixReel) {
        this.prixReel = prixReel;
    }

    public boolean getDeleted() {
        return deleted;
    }

    public BigDecimal getTva() {
        return tva;
    }

    public void setTva(BigDecimal tva) {
        this.tva = tva;
    }

    public Boolean getTvaEnPercent() {
        return tvaEnPercent;
    }

    public void setTvaEnPercent(Boolean tvaEnPercent) {
        this.tvaEnPercent = tvaEnPercent;
    }

    public BigDecimal getMontantTVA() {
        return montantTVA;
    }

    public void setMontantTVA(BigDecimal montantTVA) {
        this.montantTVA = montantTVA;
    }

    public BigDecimal getMontantSolder() {
        return montantSolder;
    }

    public void setMontantSolder(BigDecimal montantSolder) {
        this.montantSolder = montantSolder;
    }

    public BigDecimal getAvance() {
        return avance;
    }

    public void setAvance(BigDecimal avance) {
        this.avance = avance;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public List<TransactionDTO> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<TransactionDTO> transactions) {
        this.transactions = transactions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof VenteDTO)) {
            return false;
        }

        return id != null && id.equals(((VenteDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "VenteDTO{" +
            "id=" + getId() +
            ", libelle='" + getLibelle() + "'" +
            ", code='" + getCode() + "'" +
            ", date='" + getDate() + "'" +
            ", montant=" + getMontant() +
            ", tva=" + getTva() +
            ", tvaEnPercent=" + getTvaEnPercent() +
            ", reste=" + getReste() +
            ", solder='" + isSolder() + "'" +
            ", clientId=" + getClientId() +
            "}";
    }
}
