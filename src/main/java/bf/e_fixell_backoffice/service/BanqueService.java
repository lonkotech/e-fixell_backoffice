package bf.e_fixell_backoffice.service;

import bf.e_fixell_backoffice.domain.Banque;
import bf.e_fixell_backoffice.repository.BanqueRepository;
import bf.e_fixell_backoffice.service.dto.BanqueDTO;
import bf.e_fixell_backoffice.service.mapper.BanqueMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Banque}.
 */
@Service
@Transactional
public class BanqueService {

    private final Logger log = LoggerFactory.getLogger(BanqueService.class);

    private final BanqueRepository banqueRepository;

    private final BanqueMapper banqueMapper;

    public BanqueService(BanqueRepository banqueRepository, BanqueMapper banqueMapper) {
        this.banqueRepository = banqueRepository;
        this.banqueMapper = banqueMapper;
    }

    /**
     * Save a banque.
     *
     * @param banqueDTO the entity to save.
     * @return the persisted entity.
     */
    public BanqueDTO save(BanqueDTO banqueDTO) {
        log.debug("Request to save Banque : {}", banqueDTO);
        Banque banque = banqueMapper.toEntity(banqueDTO);
        banque = banqueRepository.save(banque);
        return banqueMapper.toDto(banque);
    }

    /**
     * Get all the banques.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<BanqueDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Banques");
        return banqueRepository.findByDeletedIsFalse(pageable)
            .map(banqueMapper::toDto);
    }

    /**
     * Get one banque by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public BanqueDTO findOne(Long id) {
        log.debug("Request to get Banque : {}", id);
        return banqueMapper.toDto(banqueRepository.findByIdAndDeletedIsFalse(id));
    }

    /**
     * Delete the banque by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Banque : {}", id);
        Banque banque = banqueRepository.getOne(id);
        banque.setDeleted(true);
        banqueRepository.save(banque);
    }
}
