package bf.e_fixell_backoffice.service;

import bf.e_fixell_backoffice.domain.Caisse;
import bf.e_fixell_backoffice.domain.OperationCaisse;
import bf.e_fixell_backoffice.domain.enumeration.TypeOperationCaisse;
import bf.e_fixell_backoffice.repository.CaisseRepository;
import bf.e_fixell_backoffice.repository.OperationCaisseRepository;
import bf.e_fixell_backoffice.repository.SessionCaisseRepository;
import bf.e_fixell_backoffice.service.dto.OperationCaisseDTO;
import bf.e_fixell_backoffice.service.mapper.OperationCaisseMapper;
import bf.e_fixell_backoffice.web.rest.errors.CustomParameterizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Optional;

/**
 * Service Implementation for managing {@link OperationCaisse}.
 */
@Service
@Transactional
public class OperationCaisseService {

    private final Logger log = LoggerFactory.getLogger(OperationCaisseService.class);

    private final OperationCaisseRepository operationCaisseRepository;

    private final CaisseRepository caisseRepository;

    private final OperationCaisseMapper operationCaisseMapper;

    private final SessionCaisseRepository sessionCaisseRepository;

    public OperationCaisseService(OperationCaisseRepository operationCaisseRepository, CaisseRepository caisseRepository, OperationCaisseMapper operationCaisseMapper, SessionCaisseRepository sessionCaisseRepository) {
        this.operationCaisseRepository = operationCaisseRepository;
        this.caisseRepository = caisseRepository;
        this.operationCaisseMapper = operationCaisseMapper;
        this.sessionCaisseRepository = sessionCaisseRepository;
    }

    /**
     * Save a operationCaisse.
     *
     * @param operationCaisseDTO the entity to save.
     * @return the persisted entity.
     */

    @Transactional
    public OperationCaisseDTO save(OperationCaisseDTO operationCaisseDTO) {
        log.debug("Request to save OperationCaisse : {}", operationCaisseDTO);
        OperationCaisse operationCaisse = operationCaisseMapper.toEntity(operationCaisseDTO);
        if (operationCaisseDTO.getTypeOperationCaisse().equals(TypeOperationCaisse.APPROVISIONNEMENT) && operationCaisseDTO.getCaisseSrcId() == null) {
            log.debug("-------Approvisonnement dea caisse centrale---");
            if (operationCaisseDTO.getCaisseDstId() != null && operationCaisseDTO.getMontant() != null) {
                Caisse caisseDest = caisseRepository.getOne(operationCaisseDTO.getCaisseDstId());
                BigDecimal montantOp = caisseDest.getSomme().add(operationCaisseDTO.getMontant());
                operationCaisse.setSoldeAvant(caisseDest.getSomme());
                caisseDest.setSomme(montantOp);
                operationCaisse.setCaisseDst(caisseDest);
                operationCaisse.setSoldeApres(montantOp);
                operationCaisse.setDescription(operationCaisseDTO.getDescription());
                operationCaisse = operationCaisseRepository.save(operationCaisse);
            } else {
                throw new CustomParameterizedException("Caisse ou montant introuvable ");
            }
            operationCaisse = operationCaisseRepository.save(operationCaisse);
        } else {
            if (operationCaisseDTO.getCaisseDstId() != null && operationCaisseDTO.getCaisseSrcId() != null) {
                Caisse caisseSrc = caisseRepository.getOne(operationCaisseDTO.getCaisseSrcId());
                Caisse caisseDest = caisseRepository.getOne(operationCaisseDTO.getCaisseDstId());
                if (caisseSrc.getId() != null && caisseDest.getId() != null) {
                    operationCaisse = doApprovisionnement(caisseSrc, caisseDest, operationCaisseDTO);
                } else {
                    throw new CustomParameterizedException("Caisse introuvable");
                }
            } else {
                throw new CustomParameterizedException("Caisse introuvable");
            }

        }
        return operationCaisseMapper.toDto(operationCaisse);
    }

    public OperationCaisseDTO saveRemboursement(OperationCaisseDTO operationCaisseDTO) {
        log.debug("Request to save OperationCaisse : {}", operationCaisseDTO);
        OperationCaisse operationCaisse = operationCaisseMapper.toEntity(operationCaisseDTO);
        if (operationCaisseDTO.getTypeOperationCaisse().equals(TypeOperationCaisse.MOUVEMENT)) {
            Caisse caisse = caisseRepository.getOne(operationCaisseDTO.getCaisseSrcId());
            BigDecimal montantOp = caisse.getSomme().subtract(operationCaisseDTO.getMontant());
            operationCaisse.setSoldeAvant(caisse.getSomme());
            caisse.setSomme(montantOp);
            operationCaisse.setSoldeApres(montantOp);
            operationCaisse.setDescription(operationCaisseDTO.getDescription());
            operationCaisse = operationCaisseRepository.save(operationCaisse);
        } else {
            throw new CustomParameterizedException("Caisse ou montant introuvable ");
        }
        return operationCaisseMapper.toDto(operationCaisse);
    }

    /**
     * Get all the operationCaisses.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<OperationCaisseDTO> findAll(OperationCaisseDTO operationCaisseDTO, Pageable pageable) {
        log.debug("Request to get all OperationCaisses");
        Caisse caisseSrc = new Caisse();
        Caisse caisseDest = new Caisse();
        if (operationCaisseDTO.getCaisseSrcId() != null) {
            caisseSrc = caisseRepository.getOne(operationCaisseDTO.getCaisseSrcId());
        }
        if (operationCaisseDTO.getCaisseDstId() != null) {
            caisseDest = caisseRepository.getOne(operationCaisseDTO.getCaisseDstId());
        }

        // TODO: 30/10/2021 la récupération des opérations en fonction d'un intervalle de date si une dateDebut est null on récupère à partir du début de l'année , si dateFin est null on se limite avant 24h

        if (operationCaisseDTO.getDateDebut() != null && operationCaisseDTO.getDateFin() != null) {
            return operationCaisseRepository.findByCriteria(operationCaisseDTO.getCaisseSrcId(), operationCaisseDTO.getCaisseDstId(), operationCaisseDTO.getTypeOperationCaisse(), operationCaisseDTO.getMontant(), operationCaisseDTO.getDateDebut(), operationCaisseDTO.getDateFin(), pageable)
                .map(operationCaisseMapper::toDto);
        } else if (operationCaisseDTO.getDateDebut() != null) {
            return operationCaisseRepository.findByCriteria(operationCaisseDTO.getCaisseSrcId(), operationCaisseDTO.getCaisseDstId(), operationCaisseDTO.getTypeOperationCaisse(), operationCaisseDTO.getMontant(), operationCaisseDTO.getDateDebut(), operationCaisseDTO.getDateDebut().plusHours(23), pageable)
                .map(operationCaisseMapper::toDto);
        } else if (operationCaisseDTO.getDateFin() != null) {
            return operationCaisseRepository.findByCriteria(operationCaisseDTO.getCaisseSrcId(), operationCaisseDTO.getCaisseDstId(), operationCaisseDTO.getTypeOperationCaisse(), operationCaisseDTO.getMontant(), ZonedDateTime.now().minusMonths(12), operationCaisseDTO.getDateFin().plusHours(23), pageable)
                .map(operationCaisseMapper::toDto);
        } else {
            return operationCaisseRepository.findByCriteria(operationCaisseDTO.getCaisseSrcId(), operationCaisseDTO.getCaisseDstId(), operationCaisseDTO.getTypeOperationCaisse(), operationCaisseDTO.getMontant(), operationCaisseDTO.getDateDebut(), operationCaisseDTO.getDateFin(), pageable)
                .map(operationCaisseMapper::toDto);
        }

    }


    /**
     * Get one operationCaisse by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<OperationCaisseDTO> findOne(Long id) {
        log.debug("Request to get OperationCaisse : {}", id);
        return operationCaisseRepository.findById(id)
            .map(operationCaisseMapper::toDto);
    }

    /**
     * Delete the operationCaisse by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete OperationCaisse : {}", id);
        operationCaisseRepository.deleteById(id);
    }

    private OperationCaisse doApprovisionnement(Caisse caisseSrc, Caisse caisseDest, OperationCaisseDTO operationCaisseDTO) {
        OperationCaisse operationCaisseSrc = operationCaisseMapper.toEntity(operationCaisseDTO);
        OperationCaisse operationCaisseDest = operationCaisseMapper.toEntity(operationCaisseDTO);
        if (operationCaisseDTO.getTypeOperationCaisse().equals(TypeOperationCaisse.REVERSEMENT)) {
            BigDecimal montantAdeBiter = caisseSrc.getSomme().subtract(caisseSrc.getSommeMin());
            operationCaisseSrc.setSoldeAvant(caisseSrc.getSomme());
            operationCaisseDest.setSoldeAvant(caisseDest.getSomme());
            operationCaisseSrc.setSoldeApres(caisseSrc.getSomme().subtract(montantAdeBiter));
            operationCaisseDest.setSoldeApres(caisseDest.getSomme().add(montantAdeBiter));
            caisseSrc.setSomme(caisseSrc.getSomme().subtract(montantAdeBiter));
            caisseDest.setSomme(caisseDest.getSomme().add(montantAdeBiter));
            operationCaisseSrc.setCaisseSrc(caisseSrc);
            operationCaisseSrc.setCaisseDst(null);
            operationCaisseDest.setCaisseDst(caisseDest);
            operationCaisseDest.setCaisseSrc(null);
        } else {
            if (canAppro(caisseSrc, operationCaisseDTO.getMontant())) {
                BigDecimal montantOp;
                operationCaisseSrc.setSoldeAvant(caisseSrc.getSomme());
                operationCaisseDest.setSoldeAvant(caisseDest.getSomme());
                operationCaisseSrc.setSoldeApres(caisseSrc.getSomme().subtract(operationCaisseDTO.getMontant()));
                operationCaisseDest.setSoldeApres(caisseDest.getSomme().add(operationCaisseDTO.getMontant()));
                operationCaisseSrc.setCaisseSrc(caisseSrc);
                operationCaisseSrc.setCaisseDst(null);
                operationCaisseDest.setCaisseDst(caisseDest);
                operationCaisseDest.setCaisseSrc(null);
                caisseSrc.setSomme(caisseSrc.getSomme().subtract(operationCaisseDTO.getMontant()));
                caisseDest.setSomme(caisseDest.getSomme().add(operationCaisseDTO.getMontant()));
            } else {
                throw new CustomParameterizedException("Solde de la caisse " + caisseSrc.getLibelle() + " insuffisante");
            }
        }
        caisseSrc = caisseRepository.save(caisseSrc);
        caisseDest = caisseRepository.save(caisseDest);
        operationCaisseSrc.setDescription(operationCaisseDTO.getDescription());
        operationCaisseDest.setDescription(operationCaisseDTO.getDescription());
        operationCaisseSrc = operationCaisseRepository.save(operationCaisseSrc);
        operationCaisseDest = operationCaisseRepository.save(operationCaisseDest);
        return operationCaisseSrc;

    }

    private void detailOperation(OperationCaisse operationCaisse, OperationCaisse operationCaisse1) {

    }


    public boolean canAppro(Caisse caisse, BigDecimal montant) {
        return caisse.getSomme().compareTo(montant) > 0;
    }

}
