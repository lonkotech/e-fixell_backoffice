package bf.e_fixell_backoffice.service;

import bf.e_fixell_backoffice.domain.Fonction;
import bf.e_fixell_backoffice.domain.HistoriqueAffectation;
import bf.e_fixell_backoffice.repository.FonctionRepository;
import bf.e_fixell_backoffice.repository.HistoriqueAffectationRepository;
import bf.e_fixell_backoffice.repository.PersonnelRepository;
import bf.e_fixell_backoffice.service.dto.FonctionDTO;
import bf.e_fixell_backoffice.service.dto.PersonnelDTO;
import bf.e_fixell_backoffice.service.mapper.FonctionMapper;
import bf.e_fixell_backoffice.service.mapper.PersonnelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Fonction}.
 */
@Service
@Transactional
public class FonctionService {

    private final Logger log = LoggerFactory.getLogger(FonctionService.class);

    private final FonctionRepository fonctionRepository;

    private final HistoriqueAffectationRepository historiqueAffectationRepository;

    private final PersonnelRepository personnelRepository;

    private final PersonnelMapper personnelMapper;

    private final FonctionMapper fonctionMapper;

    public FonctionService(FonctionRepository fonctionRepository, HistoriqueAffectationRepository historiqueAffectationRepository, PersonnelRepository personnelRepository, PersonnelMapper personnelMapper, FonctionMapper fonctionMapper) {
        this.fonctionRepository = fonctionRepository;
        this.historiqueAffectationRepository = historiqueAffectationRepository;
        this.personnelRepository = personnelRepository;
        this.personnelMapper = personnelMapper;
        this.fonctionMapper = fonctionMapper;
    }

    /**
     * Save a fonction.
     *
     * @param fonctionDTO the entity to save.
     * @return the persisted entity.
     */
    public FonctionDTO save(FonctionDTO fonctionDTO) {
        log.debug("Request to save Fonction : {}", fonctionDTO);
        Fonction fonction = fonctionMapper.toEntity(fonctionDTO);
        fonction = fonctionRepository.save(fonction);
        return fonctionMapper.toDto(fonction);
    }

    /**
     * Get all the fonctions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<FonctionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Fonctions");
        return fonctionRepository.findAllWithCriteria(pageable)
            .map(fonctionMapper::toDto);
    }


    /**
     * Get one fonction by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public FonctionDTO findOne(Long id) {
        log.debug("Request to get Fonction : {}", id);
        Optional<Fonction> fonction = fonctionRepository.findByIdAndDeletedIsFalse(id);
        FonctionDTO fonctionDTO = new FonctionDTO();
        List<PersonnelDTO> personnelDTOList = new ArrayList<>();
        if (fonction.isPresent()) {
            List<HistoriqueAffectation> affectationList = historiqueAffectationRepository.findByFonctionId(fonction.get().getId());

            for (HistoriqueAffectation historiqueAffectation : affectationList) {
                PersonnelDTO personnelDTO = personnelMapper.toDto(historiqueAffectation.getPersonnel());
                personnelDTOList.add(personnelDTO);
            }
            fonctionDTO = fonctionMapper.toDto(fonction.get());
            fonctionDTO.setListePersonne(personnelDTOList);
            return fonctionDTO;
        }
        return null;
    }

    /**
     * Delete the fonction by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Fonction : {}", id);
        Fonction fonction = fonctionRepository.getOne(id);
        fonction.setDeleted(true);
        fonctionRepository.save(fonction);
    }
}
