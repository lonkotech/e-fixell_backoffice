package bf.e_fixell_backoffice.service;

import bf.e_fixell_backoffice.domain.Produit;
import bf.e_fixell_backoffice.domain.enumeration.Etat;
import bf.e_fixell_backoffice.domain.enumeration.TypeTransaction;
import bf.e_fixell_backoffice.repository.ProduitRepository;
import bf.e_fixell_backoffice.service.dto.*;
import bf.e_fixell_backoffice.service.mapper.ProduitMapper;
import bf.e_fixell_backoffice.service.mapper.TransactionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Produit}.
 */
@Service
@Transactional
public class ProduitService {

    private final Logger log = LoggerFactory.getLogger(ProduitService.class);

    private final ProduitRepository produitRepository;

    private final ProduitMapper produitMapper;

    private final TransactionMapper transactionMapper;

    private final TransactionService transactionService;

    private final PerteService perteService;


    private final InventaireService inventaireService;

    public ProduitService(InventaireService inventaireService, PerteService perteService, ProduitRepository produitRepository, ProduitMapper produitMapper, TransactionMapper transactionMapper, TransactionService transactionService) {
        this.inventaireService = inventaireService;
        this.perteService = perteService;
        this.produitRepository = produitRepository;
        this.produitMapper = produitMapper;
        this.transactionMapper = transactionMapper;
        this.transactionService = transactionService;
    }

    /**
     * Save a produit.
     *
     * @param produitDTO the entity to save.
     * @return the persisted entity.
     */
    public ProduitDTO save(ProduitDTO produitDTO) {
        log.debug("Request to save Produit : {}", produitDTO);
        Produit produit = produitMapper.toEntity(produitDTO);
        produit = produitRepository.save(produit);
        return produitMapper.toDto(produit);
    }

    /**
     * Get all the produits.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<ProduitDTO> findAll() {
        log.debug("Request to get all Produits");
        return produitRepository.findAll()
            .stream().map(produitMapper::toDto).collect(Collectors.toList());
    }


    /**
     * Get one produit by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public List<TransactionDTO> findOne(Long id) {
        log.debug("Request to get Produit : {}", id);

        Produit produit = produitRepository.findById(id).get();
        if (produit != null) {
            if (!produit.getTransactions().isEmpty()) {
                return produit.getTransactions()
                    .stream()
                    .filter(t -> t.getTypeTransaction().equals(TypeTransaction.APPROVISIONNEMENT)
                        || t.getTypeTransaction().equals(TypeTransaction.LIVRAISON)
                        && t.getDecompte() >= 0)
                    .map(transactionMapper::toDto)
                    .collect(Collectors.toList());
            }
        }
        return new ArrayList<>();
    }

    /**
     * Delete the produit by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Produit : {}", id);
        produitRepository.deleteById(id);
    }

    @Transactional(readOnly = true)
    public Page<ProduitDTO> findWithCriteria(Pageable pageable, ProduitDTO produitDTO) {
        return produitRepository.findWithCriteria(pageable, produitDTO.getCode(), produitDTO.getLibelle(), produitDTO.getCategorieId(), produitDTO.getClassificationId()).map(ProduitDTO::new);
    }

    public void deleteProduitById(Long id) {
        Optional<Produit> produit = produitRepository.findById(id);
        if (produit.isPresent())
            produitRepository.deleteById(id);
    }

    public MResponse inventaireProduit(List<InventaireDTO> inventaireDTOs) {
        List<InventaireDTO> inventaireDTOList = new ArrayList<>();
        for (InventaireDTO inventaireDTO : inventaireDTOs) {
            if (inventaireDTO.getQuantiteTheorique() - inventaireDTO.getQuantitePhysique() == inventaireDTO.getEcart()) {
                InventaireDTO inventaire = new InventaireDTO();
                inventaire.setCommentaire(inventaireDTO.getCommentaire());
                // inventaire.setEcart(inventaireDTO.getQuantiteTheorique()-inventaireDTO.getQuantitePhysique());
                inventaire.setEcart(inventaireDTO.getEcart());
                inventaire.setProduitId(inventaireDTO.getProduitId());
                inventaire.setQuantitePhysique(inventaireDTO.getQuantitePhysique());
                inventaire.setQuantiteTheorique(inventaireDTO.getQuantiteTheorique());
                inventaire = inventaireService.save(inventaire);
                if (inventaireDTO.getQuantiteTheorique() > inventaireDTO.getQuantitePhysique()) {
                    PerteDTO perte = new PerteDTO();
                    Optional<Produit> produit = produitRepository.findById(inventaireDTO.getProduitId());
                    if (produit.isPresent()) {
                        perte.setProduitId(inventaireDTO.getProduitId());
                        perte.setDate(Instant.now());
                        perte.setQuantite(inventaireDTO.getEcart());
                        perte.setMontant(produit.get().getPrixAchatCourant().multiply(new BigDecimal(inventaireDTO.getEcart())));
                        perte.setLibelle(inventaireDTO.getCommentaire());
                        perte.setInventaireId(inventaire.getId());

                        produit.get().setQuantite(inventaireDTO.getQuantitePhysique());
                        perteService.save(perte);
                        produitRepository.save(produit.get());
                    }
                } else if (inventaireDTO.getQuantiteTheorique() < inventaireDTO.getQuantitePhysique()) {
                    for (TransactionDTO transactionDTO : inventaireDTO.getTransactions()) {
                        TransactionDTO transaction = new TransactionDTO();
                        transaction.setDate(Instant.now());
                        transaction.setDeleted(false);
                        transaction.setPrixAchatUnitaire(transactionDTO.getPrixAchatUnitaire());
                        transaction.setPrixVenteUnitaire(transactionDTO.getPrixVenteUnitaire());
                        transaction.setQuantite(transactionDTO.getQuantite());
                        transaction.setProduitId(inventaireDTO.getProduitId());
                        transaction.setCaracteristiques(transactionDTO.getCaracteristiques());
                        transaction.setTypeTransaction(TypeTransaction.REAJUSTEMENT);
                        transaction.setEtat(Etat.VALIDER);
                        transaction.setInventaireId(inventaire.getId());
                        transactionService.save(transaction);
                    }
                    inventaireDTOList.add(inventaire);
                }
            } else
                return new MResponse("-1", "Les données sont erronées", "NONOK");
        }
        if (inventaireDTOList.size() > 0)
            return new MResponse("0", "Operation effectue avec succes", "OK");
        else
            return new MResponse("-1", "Erreur lors de l'operation", "NONOK");
    }
}
