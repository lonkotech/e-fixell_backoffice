package bf.e_fixell_backoffice.service;

import bf.e_fixell_backoffice.domain.*;
import bf.e_fixell_backoffice.domain.enumeration.Statut;
import bf.e_fixell_backoffice.repository.*;
import bf.e_fixell_backoffice.security.SecurityUtils;
import bf.e_fixell_backoffice.service.dto.DepenseDTO;
import bf.e_fixell_backoffice.service.mapper.DepenseMapper;
import bf.e_fixell_backoffice.web.rest.errors.CustomParameterizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Depense}.
 */
@Service
@Transactional
public class DepenseService {

    private final Logger log = LoggerFactory.getLogger(DepenseService.class);

    private final DepenseRepository depenseRepository;

    private final SessionCaisseRepository sessionCaisseRepository;

    private final TypeDepenseRepository typeDepenseRepository;

    private final UserRepository userRepository;

    private final PaiementRepository paiementRepository;

    private final DepenseMapper depenseMapper;

    public DepenseService(DepenseRepository depenseRepository, SessionCaisseRepository sessionCaisseRepository, TypeDepenseRepository typeDepenseRepository, UserRepository userRepository, PaiementRepository paiementRepository, DepenseMapper depenseMapper) {
        this.depenseRepository = depenseRepository;
        this.sessionCaisseRepository = sessionCaisseRepository;
        this.typeDepenseRepository = typeDepenseRepository;
        this.userRepository = userRepository;
        this.paiementRepository = paiementRepository;
        this.depenseMapper = depenseMapper;
    }

    /**
     * Save a depense.
     *
     * @param depenseDTO the entity to save.
     * @return the persisted entity.
     */
    public DepenseDTO save(DepenseDTO depenseDTO) {
        log.debug("Request to save Depense : {}", depenseDTO);
        Depense depense = depenseMapper.toEntity(depenseDTO);
        Paiement paiement=new Paiement();
        String userLogin = SecurityUtils.getCurrentUserLogin().orElseThrow(() -> new CustomParameterizedException("Current user login not found"));
        Optional<User> user = Optional.empty();
        if (userLogin != null) {
            user = userRepository.findOneByLogin(userLogin);
            if (user.isPresent()) {

                Optional<SessionCaisse> sessionCaisse = sessionCaisseRepository.findByUserAndStatut(user.get(), Statut.OUVERT);
                if (sessionCaisse.isPresent()) {
                    if (sessionCaisse.get().getId() != null) {
                        depense.setSessionCaisse(sessionCaisse.get());
                        sessionCaisse.get().setSommeFin(sessionCaisse.get().getSommeFin().subtract(depenseDTO.getMontant()));
                        paiement.setSessioncaisse(sessionCaisse.get());
                        paiement.setMontant(depenseDTO.getMontant());
                        paiement.setMotif(depenseDTO.getDescription());
                    } else {
                        throw new CustomParameterizedException("Erreur session non trouvé");
                    }
                    if (depenseDTO.getTypeDepenseId() != null) {
                        TypeDepense typeDepense = typeDepenseRepository.getOne(depenseDTO.getTypeDepenseId());
                        if (typeDepense.getId() != null) {
                            depense.setTypeDepense(typeDepense);
                        } else {
                            throw new CustomParameterizedException("Erreur interne type de dépense introuvable");
                        }
                    } else {
                        throw new CustomParameterizedException("Erreur interne type de dépense introuvable");
                    }

                } else {
                    throw new CustomParameterizedException("Erreur session innexistante");
                }
            }
        }

        depense = depenseRepository.save(depense);
        paiement.setDate(Instant.now());
        paiementRepository.save(paiement);
        return depenseMapper.toDto(depense);
    }

    /**
     * Get all the depenses.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<DepenseDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Depenses");
        return depenseRepository.findAll(pageable)
            .map(depenseMapper::toDto);
    }


    /**
     * Get one depense by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<DepenseDTO> findOne(Long id) {
        log.debug("Request to get Depense : {}", id);
        return depenseRepository.findById(id)
            .map(depenseMapper::toDto);
    }

    /**
     * Delete the depense by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Depense : {}", id);
        depenseRepository.deleteById(id);
    }
}
