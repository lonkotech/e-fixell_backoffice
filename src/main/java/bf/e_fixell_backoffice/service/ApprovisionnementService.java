package bf.e_fixell_backoffice.service;

import bf.e_fixell_backoffice.domain.*;
import bf.e_fixell_backoffice.domain.enumeration.Etat;
import bf.e_fixell_backoffice.domain.enumeration.Statut;
import bf.e_fixell_backoffice.domain.enumeration.TypeTransaction;
import bf.e_fixell_backoffice.repository.*;
import bf.e_fixell_backoffice.service.dto.ApprovisionnementDTO;
import bf.e_fixell_backoffice.service.dto.FicheTechniqueDTO;
import bf.e_fixell_backoffice.service.dto.PrixProduitDTO;
import bf.e_fixell_backoffice.service.dto.TransactionDTO;
import bf.e_fixell_backoffice.service.mapper.ApprovisionnementMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Approvisionnement}.
 */
@Service
@Transactional
public class ApprovisionnementService {

    private final Logger log = LoggerFactory.getLogger(ApprovisionnementService.class);

    private final ApprovisionnementRepository approvisionnementRepository;

    private final ApprovisionnementMapper approvisionnementMapper;

    private final FicheTechniqueService ficheTechniqueService;

    private final CaracteristiqueService caracteristiqueService;

    private final CaracteristiqueRepository caracteristiqueRepository;

    private final PrixProduitService prixProduitService;

    private final TransactionService transactionService;

    private final TransactionRepository transactionRepository;

    private final ProduitRepository produitRepository;

    private final CommandeRepository commandeRepository;

    public ApprovisionnementService(ApprovisionnementRepository approvisionnementRepository,
                                    ApprovisionnementMapper approvisionnementMapper,
                                    FicheTechniqueService ficheTechniqueService,
                                    CaracteristiqueService caracteristiqueService,
                                    CaracteristiqueRepository caracteristiqueRepository,
                                    PrixProduitService prixProduitService,
                                    TransactionService transactionService,
                                    TransactionRepository transactionRepository,
                                    ProduitRepository produitRepository,
                                    CommandeRepository commandeRepository) {
        this.approvisionnementRepository = approvisionnementRepository;
        this.approvisionnementMapper = approvisionnementMapper;
        this.ficheTechniqueService = ficheTechniqueService;
        this.caracteristiqueService = caracteristiqueService;
        this.caracteristiqueRepository = caracteristiqueRepository;
        this.prixProduitService = prixProduitService;
        this.transactionService = transactionService;
        this.transactionRepository = transactionRepository;
        this.produitRepository = produitRepository;
        this.commandeRepository = commandeRepository;
    }

    /**
     * Save a approvisionnement.
     *
     * @param approvisionnementDTO the entity to save.
     * @return the persisted entity.
     *
     * algo
     * si approvisionnement est lie à une commande on le gère comme une livraison
     * sinon on enregistre un approvisionnement avec ses propres transactions
     */
//    public ApprovisionnementDTO save(ApprovisionnementDTO approvisionnementDTO) {
//        log.debug("Request to save Approvisionnement : {}", approvisionnementDTO);
//
//        List<Caracteristique> newListeCaracteristique = new ArrayList<>();
//        // GESTION APPR
//
//        Approvisionnement finalApprovisionnement;
//        Approvisionnement approvisionnement = approvisionnementMapper.toEntity(approvisionnementDTO);
//        finalApprovisionnement = approvisionnementRepository.save(approvisionnement);
//
//        approvisionnementDTO.getTransactions().forEach(transaction -> {
//            TransactionDTO transactionDTO = new TransactionDTO();
//            PrixProduitDTO prixProduitDTO = new PrixProduitDTO();
//            FicheTechniqueDTO ficheTechniqueDTO = new FicheTechniqueDTO();
//            Produit produit = produitRepository.getOne(transaction.getProduitId());
//            if (produit != null) {
//                if (produit.getQuantite() != null) {
//                    /*int qte = ;*/
//                    produit.setQuantite(produit.getQuantite() + transaction.getQuantite());
//                } else {
//                    produit.setQuantite(transaction.getQuantite());
//                }
//                if (produit.getPrixActuel() != null) {
//                    if (produit.getPrixActuel().compareTo(transaction.getPrixProduit().getPrix()) <= 0) {
//                        produit.setPrixActuel(transaction.getPrixProduit().getPrix());
//                    }
//                } else {
//                    produit.setPrixActuel(transaction.getPrixProduit().getPrix());
//                }
//                produitRepository.save(produit);
//            }
//
//            montantAppro = montantAppro.add(transaction.getPrixUnitaire().multiply(new BigDecimal(transaction.getQuantite())));
//            // GESTION FICHETECHNIQUE
//
//           /* ficheTechniqueDTO.setLibelle(approvisionnementDTO.getLibelle());
//            FicheTechnique newFiche = ficheTechniqueService.save(ficheTechniqueDTO);*/
//
//            // GESTION PRIX PRODUIT
//
//            prixProduitDTO.setDateDebut(Instant.now());
//            prixProduitDTO.setDateFin(Instant.now());
//            prixProduitDTO.setPrix(transaction.getPrixProduit().getPrix());
//            prixProduitDTO.setStatut(Statut.ACTIF);
//            PrixProduitDTO savePrixProduit = prixProduitService.save(prixProduitDTO);
//            // GESTION CARRACTERISTIQUE
//
//            /*transaction.getFicheTechnique().getCaracteristiques().forEach(caracteristique -> {
//                Caracteristique newCaracteristique = new Caracteristique();
//                newCaracteristique.setLibelle(caracteristique.getLibelle());
//                newCaracteristique.setValeur(caracteristique.getValeur());
//                newCaracteristique.setFicheTechnique(newFiche);
//                newListeCaracteristique.add(newCaracteristique);
//            });
//            caracteristiqueRepository.saveAll(newListeCaracteristique);*/
//
//            // GESTION TRANSACTION
//
//            transactionDTO.setApprovisionnementId(finalApprovisionnement.getId());
//            transactionDTO.setDate(Instant.now());
//           /* transactionDTO.setFicheTechniqueId(newFiche.getId());*/
//            transactionDTO.setPrixUnitaire(transaction.getPrixUnitaire());
//            transactionDTO.setQuantite(transaction.getQuantite());
//            transactionDTO.setTypeTransaction(TypeTransaction.APPROVISIONNEMENT);
//            transactionDTO.setEtat(Etat.VALIDER);
//            transactionDTO.setCaracteristiques(transaction.getCaracteristiques());
//            transactionDTO.setProduitId(transaction.getProduitId());
////            transactionDTO.setPrixProduitId(savePrixProduit.getId());
////            transactionDTO.setDecompte(transaction.getQuantite());
//            transactionDTO.setMotif(null);
//            transactionService.save(transactionDTO);
//        });
//        long compteur = 0;
//        String code = "";
//        do {
//            compteur++;
//            code = "EF-AP-" + LocalDate.now().getYear() + "-" + String.format("%03d", (approvisionnementRepository.count() + compteur));
//        } while (approvisionnementRepository.findByCode(code).get() == null);
//        finalApprovisionnement.setCode(code);
//        finalApprovisionnement.setMontant(montantAppro);
//        approvisionnementRepository.save(finalApprovisionnement);
//        return approvisionnementMapper.toDto(finalApprovisionnement);
//    }

    @Transactional
    public ApprovisionnementDTO newSave(ApprovisionnementDTO approvisionnementDTO) throws Exception {
        log.debug("Request to save Approvisionnement : {}", approvisionnementDTO);

        // GESTION APPR
        BigDecimal approT = BigDecimal.ZERO;
        Approvisionnement finalApprovisionnement;
        Approvisionnement approvisionnement = approvisionnementMapper.toEntity(approvisionnementDTO);
        finalApprovisionnement = approvisionnementRepository.save(approvisionnement);

        for(TransactionDTO transaction : approvisionnementDTO.getTransactions()) {
                if (transaction.getQuantite() > 0 && transaction.getPrixAchatUnitaire() != null
                    && transaction.getPrixVenteUnitaire() != null) {

                    Produit produit = produitRepository.getOne(transaction.getProduitId());
                    if (produit != null) {

                        if (produit.getQuantite() != null) {
                            produit.setQuantite(produit.getQuantite() + transaction.getQuantite());
                        } else {
                            produit.setQuantite(transaction.getQuantite());
                        }

                        updateProduit(produit,transaction);
                        approT = approT.add(transaction.getPrixAchatUnitaire()
                            .multiply(new BigDecimal(transaction.getQuantite())));

                        // GESTION TRANSACTION

                        transaction.setApprovisionnementId(finalApprovisionnement.getId());
                        transaction.setDate(Instant.now());
                        transaction.setDateLivre(transaction.getDate());
                        transaction.setQuantiteALivre(transaction.getQuantite());
                        transaction.setDecompte(transaction.getQuantite());
                        transaction.setTypeTransaction(TypeTransaction.APPROVISIONNEMENT);
                        transaction.setEtat(Etat.VALIDER);
                        transaction.setMotif(produit.getLibelle());
                        transactionService.save(transaction);
                    }
                    else throw new Exception("Produit est null");
                } else throw new Exception("Prix d'achat ou Prix de vente est null");
            }
//        for(TransactionDTO transaction : approvisionnementDTO.getTransactions()) {
//            if (transaction.getQuantiteLivre() > 0 && transaction.getId() != null
//                && transaction.getPrixAchatUnitaire() != null &&
//                transaction.getPrixVenteUnitaire() != null) {
//
//                Produit produit = produitRepository.getOne(transaction.getProduitId());
//                if (produit != null) {
//
//                    updateProduit(produit,transaction);
//
//                    approT = approT.add(transaction.getPrixAchatUnitaire()
//                        .multiply(new BigDecimal(transaction.getQuantiteLivre())));
//
//                    // GESTION TRANSACTION
//
//                    transaction.setApprovisionnementId(finalApprovisionnement.getId());
//                    transaction.setDateLivre(Instant.now());
//                    transaction.setTypeTransaction(TypeTransaction.APPROVISIONNEMENT);
//                    transaction.setEtat(Etat.VALIDER);
//                    transactionService.save(transaction);
//
//                } else throw new Exception("Produit est null");
//            } else throw new Exception("Prix d'achat ou Prix de vente est null");
//        }
//        Commande c = commandeRepository.findById(approvisionnementDTO.getCommandeId()).get();
//        c.setDateLivraison(Instant.now());
//        commandeRepository.save(c);
//    }
        long compteur = 0;
        String code = "";
        do {
            compteur++;
            code = "EF-AP-" + LocalDate.now().getYear()
                + "-" + String.format("%03d", (approvisionnementRepository.count() + compteur));
        } while (approvisionnementRepository.findByCode(code).isPresent());

        finalApprovisionnement.setCode(code);
        finalApprovisionnement.setMontant(approT);
        approvisionnementRepository.save(finalApprovisionnement);
        return approvisionnementMapper.toDto(finalApprovisionnement);
    }


    public void updateProduit(Produit produit,TransactionDTO transaction){
        if (produit.getPrixVenteCourant() != null) {
            if (produit.getPrixVenteCourant().compareTo(transaction.getPrixVenteUnitaire()) <= 0) {
                produit.setPrixVenteCourant(transaction.getPrixVenteUnitaire());
                produit.setPrixAchatCourant(transaction.getPrixAchatUnitaire());
            }
        } else {
            produit.setPrixVenteCourant(transaction.getPrixVenteUnitaire());
            produit.setPrixAchatCourant(transaction.getPrixAchatUnitaire());
        }
        produitRepository.save(produit);
    }

    /**
     * Get all the approvisionnements.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ApprovisionnementDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Approvisionnements");

        Page<Approvisionnement> approvisionnement = approvisionnementRepository.findAll(pageable);
        approvisionnement.getContent().forEach(approvisionnement1 -> {
            approvisionnement1.setTransactions(transactionService.findByApproId(approvisionnement1.getId()));
        });
        return approvisionnement.map(approvisionnementMapper::toDto);

        /*return approvisionnementRepository.findAll(pageable)
            .map(approvisionnementMapper::toDto);*/
    }


    /**
     * Get one approvisionnement by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ApprovisionnementDTO> findOne(Long id) {
        log.debug("Request to get Approvisionnement : {}", id);
        return approvisionnementRepository.findById(id)
            .map(approvisionnementMapper::toDto);
    }

    /**
     * Delete the approvisionnement by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Approvisionnement : {}", id);
        approvisionnementRepository.deleteById(id);
    }
}
