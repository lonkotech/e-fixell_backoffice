package bf.e_fixell_backoffice.service;

import bf.e_fixell_backoffice.domain.Caisse;
import bf.e_fixell_backoffice.domain.OperationCaisse;
import bf.e_fixell_backoffice.domain.SessionCaisse;
import bf.e_fixell_backoffice.domain.User;
import bf.e_fixell_backoffice.domain.enumeration.Statut;
import bf.e_fixell_backoffice.domain.enumeration.TypeOperationCaisse;
import bf.e_fixell_backoffice.repository.CaisseRepository;
import bf.e_fixell_backoffice.repository.OperationCaisseRepository;
import bf.e_fixell_backoffice.repository.SessionCaisseRepository;
import bf.e_fixell_backoffice.repository.UserRepository;
import bf.e_fixell_backoffice.security.SecurityUtils;
import bf.e_fixell_backoffice.service.dto.SessionCaisseDTO;
import bf.e_fixell_backoffice.service.mapper.SessionCaisseMapper;
import bf.e_fixell_backoffice.web.rest.errors.CustomParameterizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Optional;

/**
 * Service Implementation for managing {@link SessionCaisse}.
 */
@Service
@Transactional
public class SessionCaisseService {

    private final Logger log = LoggerFactory.getLogger(SessionCaisseService.class);

    private final SessionCaisseRepository sessionCaisseRepository;

    private final SessionCaisseMapper sessionCaisseMapper;

    private final CaisseRepository caisseRepository;

    private final OperationCaisseRepository operationCaisseRepository;

    private final UserRepository userRepository;

    public SessionCaisseService(SessionCaisseRepository sessionCaisseRepository, SessionCaisseMapper sessionCaisseMapper, CaisseRepository caisseRepository, OperationCaisseRepository operationCaisseRepository, UserRepository userRepository) {
        this.sessionCaisseRepository = sessionCaisseRepository;
        this.sessionCaisseMapper = sessionCaisseMapper;
        this.caisseRepository = caisseRepository;
        this.operationCaisseRepository = operationCaisseRepository;
        this.userRepository = userRepository;
    }

    /**
     * Save a sessionCaisse.
     *
     * @param sessionCaisseDTO the entity to save.
     * @return the persisted entity.
     */
    public SessionCaisseDTO save(SessionCaisseDTO sessionCaisseDTO) {
        log.debug("Request to save SessionCaisse : {}", sessionCaisseDTO);
        SessionCaisse sessionCaisse = sessionCaisseMapper.toEntity(sessionCaisseDTO);
        String userLogin = SecurityUtils.getCurrentUserLogin().orElseThrow(() -> new CustomParameterizedException("Current user login not found"));
        Optional<User> user = Optional.empty();
        Optional<SessionCaisse> existeSession = Optional.empty();
        // TODO: 31/10/2021 récupération de l'utilisateur connecter et de sa session existante
        if (userLogin != null) {
            user = userRepository.findOneByLogin(userLogin);
            existeSession = sessionCaisseRepository.findByUserAndStatut(user.get(), Statut.OUVERT);
        }
        if (sessionCaisseDTO.getCaisseId() != null) {
            Caisse caisse = caisseRepository.getOne(sessionCaisseDTO.getCaisseId());
            // TODO: 31/10/2021 vérification si la meme caisse n'est pas ouverte
            if (caisse.getId() != null) {
                SessionCaisse exitSessionSameCaisse = sessionCaisseRepository.findByCaisseAndStatut(caisse, Statut.OUVERT);
                if (exitSessionSameCaisse != null || existeSession.isPresent()) {
                    throw new CustomParameterizedException("Une session de cette caisse existe déja ou est ouverte ");
                } else {
                    sessionCaisse.setCaisse(caisse);
                    sessionCaisse.setDateDebut(Instant.now());
                    sessionCaisse.setSommeDebut(caisse.getSomme());
                    sessionCaisse.setSommeFin(caisse.getSomme());
                    sessionCaisse.setStatut(Statut.OUVERT);
                    if (user.isPresent()) {
                        sessionCaisse.setUser(user.get());
                    } else {
                        throw new CustomParameterizedException("Utilisateur non connecté");
                    }

                    sessionCaisse = sessionCaisseRepository.save(sessionCaisse);
                }
            } else {
                throw new CustomParameterizedException("Echec caisse introuvable");

            }

        } else {
            throw new CustomParameterizedException("Caisse introuvable");
        }

        return sessionCaisseMapper.toDto(sessionCaisse);
    }

    /**
     * Get all the sessionCaisses.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
//    @Transactional(readOnly = true)
//    public Page<SessionCaisseDTO> findAll(SessionCaisseDTO sessionCaisseDTO,Pageable pageable) {
//        log.debug("Request to get all SessionCaisses");
//        return sessionCaisseRepository.findAllByCriteria(sessionCaisseDTO.getCode(),pageable)
//            .map(sessionCaisseMapper::toDto);
//    }
    @Transactional(readOnly = true)
    public Page<SessionCaisseDTO> findAllW(SessionCaisseDTO sessionCaisseDTO, Pageable pageable) {
        log.debug("Request to get all SessionCaisses");
        String userLogin = SecurityUtils.getCurrentUserLogin().orElseThrow(() -> new CustomParameterizedException("Current user login not found"));
        Optional<User> user = Optional.empty();
        Optional<User> userConnecte = Optional.empty();
        Long userId = null;
        if (sessionCaisseDTO.getUserId() != null) {
            user = userRepository.findById(sessionCaisseDTO.getUserId());
            if (user.isPresent()) {
                userId = user.get().getId();
            }
        } else {
            if (userLogin != null) {
                userConnecte = userRepository.findOneByLogin(userLogin);
                if (userConnecte.isPresent()) {
                    userId = userConnecte.get().getId();
                }
            }
        }
        return sessionCaisseRepository.findAllByCriteria(sessionCaisseDTO.getCaisseId(), userId, pageable)
            .map(sessionCaisseMapper::toDto);
    }


    /**
     * Get one sessionCaisse by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<SessionCaisseDTO> findOne(Long id) {
        log.debug("Request to get SessionCaisse : {}", id);
        return sessionCaisseRepository.findById(id)
            .map(sessionCaisseMapper::toDto);
    }

    /**
     * Delete the sessionCaisse by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete SessionCaisse : {}", id);
        SessionCaisse sessionCaisse = sessionCaisseRepository.getOne(id);
        if (sessionCaisse.getId() != null) {
            if (!sessionCaisse.getStatut().equals(Statut.FERME)) {
                throw new CustomParameterizedException("Veillez fermer la session avant de le supprimer");
            } else {
                sessionCaisse.setDeleted(true);
            }

        } else {
            throw new CustomParameterizedException("Erreur session introuvable");
        }
        sessionCaisseRepository.deleteById(id);
    }

    @Transactional
    public SessionCaisseDTO fermerSaissionCaisse(SessionCaisseDTO sessionCaisseDTO) {
        log.debug("Request to save SessionCaisse called : {}", sessionCaisseDTO);
        OperationCaisse operationCaisse = new OperationCaisse();
        SessionCaisse sessionCaisse = new SessionCaisse();
        Caisse caisse = new Caisse();
        BigDecimal finSomme = new BigDecimal(0);
        String userLogin = SecurityUtils.getCurrentUserLogin().orElseThrow(() -> new CustomParameterizedException("Current user login not found"));
        Optional<User> user = Optional.empty();
        if (userLogin != null) {
            user = userRepository.findOneByLogin(userLogin);
            if (user.isPresent()) {
                sessionCaisse = sessionCaisseRepository.getOne(sessionCaisseDTO.getId());
            } else {
                throw new CustomParameterizedException("Erreur utilisateur innexistant!!");
            }
        } else {
            throw new CustomParameterizedException("Erreur utilisateur innexistant!!");
        }
        if (sessionCaisseDTO.getId() != null) {
            if (sessionCaisse.getId() != null) {
                caisse = sessionCaisse.getCaisse();
                if (sessionCaisse.getCaisse() != null) {
                    sessionCaisse.setStatut(Statut.FERME);
                    sessionCaisse.setDateFin(Instant.now());
                    finSomme = sessionCaisse.getSommeFin().subtract(sessionCaisse.getSommeDebut());
                    caisse.setSomme(caisse.getSomme().add(finSomme));
                    caisseRepository.save(caisse);

                } else {
                    throw new CustomParameterizedException("Caisse introuvable");
                }
            } else {
                throw new CustomParameterizedException("Session introuvable");
            }
            sessionCaisse = sessionCaisseRepository.save(sessionCaisse);
            operationCaisse.setTypeOperationCaisse(TypeOperationCaisse.MOUVEMENT);
            operationCaisse.setSessionCaisse(sessionCaisse);
            operationCaisse.setMontant(finSomme);
            operationCaisse.setCaisseSrc(caisse);
            operationCaisse.setCaisseDst(caisse);
            operationCaisseRepository.save(operationCaisse);
            return sessionCaisseMapper.toDto(sessionCaisse);
        } else {
            throw new CustomParameterizedException("Session innexistante");
        }


    }

    public Optional<SessionCaisseDTO> findByUserAndStatut(User user, Statut statut) {
        return sessionCaisseRepository.findByUserAndStatut(user, statut).map(sessionCaisseMapper::toDto);
    }


}
