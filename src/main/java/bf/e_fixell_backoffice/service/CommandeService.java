package bf.e_fixell_backoffice.service;

import bf.e_fixell_backoffice.domain.*;
import bf.e_fixell_backoffice.domain.enumeration.*;
import bf.e_fixell_backoffice.repository.*;
import bf.e_fixell_backoffice.service.dto.*;
import bf.e_fixell_backoffice.service.mapper.*;
import bf.e_fixell_backoffice.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Commande}.
 */
@Service
@Transactional
public class CommandeService {

    private final Logger log = LoggerFactory.getLogger(CommandeService.class);

    private final CommandeRepository commandeRepository;

    private final LivraisonRepository livraisonRepository;

    private final LivraisonMapper livraisonMapper;

    private final CommandeMapper commandeMapper;

    private final FraisMapper fraisMapper;

    private final TransactionService transactionService;

    private final TransactionRepository transactionRepository;

    private final TransactionMapper transactionMapper;

    private final EtapeTransportMapper etapeTransportMapper;

    private final FraisRepository fraisRepository;

    private final ClientService clientService;

    private final FournisseurService fournisseurService;

    private final ProduitRepository produitRepository;

    private final UserService userService;

    private final SessionCaisseService sessionCaisseService;

    private final PaiementRepository paiementRepository;

    private final PaiementMapper paiementMapper;

    private final EtapeTransportService etapeTransportService;

    private final SessionCaisseRepository sessionCaisseRepository;

    private final OperationCaisseService operationCaisseService;

    private final SessionCaisseMapper sessionCaisseMapper;

    private final PrixProduitService prixProduitService;

    private final DeviseService deviseService;

    private final ApprovisionnementService approService;

    public CommandeService(CommandeRepository commandeRepository, LivraisonRepository livraisonRepository, LivraisonMapper livraisonMapper, CommandeMapper commandeMapper, FraisMapper fraisMapper, TransactionService transactionService, TransactionRepository transactionRepository, TransactionMapper transactionMapper, EtapeTransportMapper etapeTransportMapper, FraisRepository fraisRepository, ClientService clientService, FournisseurService fournisseurService, ProduitRepository produitRepository, UserService userService, SessionCaisseService sessionCaisseService, PaiementRepository paiementRepository, PaiementMapper paiementMapper, EtapeTransportService etapeTransportService, SessionCaisseRepository sessionCaisseRepository, OperationCaisseService operationCaisseService, SessionCaisseMapper sessionCaisseMapper, PrixProduitService prixProduitService, DeviseService deviseService, ApprovisionnementService approService) {
        this.commandeRepository = commandeRepository;
        this.livraisonRepository = livraisonRepository;
        this.livraisonMapper = livraisonMapper;
        this.commandeMapper = commandeMapper;
        this.transactionMapper = transactionMapper;
        this.fraisMapper = fraisMapper;
        this.transactionService = transactionService;
        this.transactionRepository = transactionRepository;
        this.etapeTransportMapper = etapeTransportMapper;
        this.fraisRepository = fraisRepository;
        this.clientService = clientService;
        this.fournisseurService = fournisseurService;
        this.produitRepository = produitRepository;
        this.userService = userService;
        this.sessionCaisseService = sessionCaisseService;
        this.paiementRepository = paiementRepository;
        this.paiementMapper = paiementMapper;
        this.etapeTransportService = etapeTransportService;
        this.sessionCaisseRepository = sessionCaisseRepository;
        this.operationCaisseService = operationCaisseService;
        this.sessionCaisseMapper = sessionCaisseMapper;
        this.prixProduitService = prixProduitService;
        this.deviseService = deviseService;
        this.approService = approService;
    }

    /**
     * Save a commande.
     *
     * @param commandeDTO the entity to save.
     * @return the persisted entity.
     */
    public MResponse saveCommandeClient(CommandeDTO commandeDTO) {
        log.debug("Request to save Commande : {}", commandeDTO);
        Optional<User> user = userService.getUserWithAuthorities();
        Optional<SessionCaisseDTO> sessionCaisseDTO;
        BigDecimal totalTransactionCommande;
        BigDecimal tva = BigDecimal.ZERO;
        BigDecimal ftotal;
        Boolean hasTva = clientService.findOne(commandeDTO.getClientId()).get().getPayerTva();
        if (user.isPresent()) {
            sessionCaisseDTO = sessionCaisseService.findByUserAndStatut(user.get(), Statut.OUVERT);
            if (sessionCaisseDTO.isPresent()) {
                if (commandeDTO.getAvance() != null && commandeDTO.getSomme() != null && commandeDTO.getSomme().compareTo(commandeDTO.getAvance()) >= 0) {
                    totalTransactionCommande = BigDecimal.ZERO;
                    ftotal = BigDecimal.ZERO;

                    List<TransactionDTO> listT = new ArrayList<>();
                    List<FraisDTO> listF = new ArrayList<>();
                    Commande commande = new Commande();

                    commandeDTO.setDate(Instant.now());
                    commandeDTO.setEtat(Etat.VALIDER);
                    commandeDTO.setMontantSolde(commandeDTO.getAvance());
                    if (commandeDTO.getSomme().compareTo(commandeDTO.getAvance())==0)
                        commandeDTO.setEstSolde(true);
                    else
                        commandeDTO.setEstSolde(false);
                    String code = "EF-CO-" + LocalDate.now().getYear() + "-" + String.format("%03d", (commandeRepository.count() + 1));
                    commandeDTO.setCode(code);

                    for(TransactionDTO transactionDTO : commandeDTO.getTransactions()) {
                        Optional<Devise> devise = deviseService.findOne(transactionDTO.getDeviseId());
                        TransactionDTO transaction = new TransactionDTO();
                        transaction.setDate(Instant.now());
                        transaction.setDeleted(false);
                        if (devise.isPresent()){
                            if (devise.get().getDeviseBase()!=null){
                                transaction.setPrixAchatUnitaire(transactionDTO.getPrixAchatUnitaire());
                            } else {
                                transaction.setPrixAchatUnitaire(transactionDTO.getPrixAchatUnitaire().multiply(devise.get().getValeur()));
                            }
                            transaction.setValeurDevise(devise.get().getValeur().doubleValue());
                            transaction.setDeviseId(transactionDTO.getDeviseId());
                            transaction.setPrixVenteUnitaire(transactionDTO.getPrixVenteUnitaire());
                            transaction.setQuantite(transactionDTO.getQuantite());
                            transaction.setProduitId(transactionDTO.getProduitId());
                            transaction.setCaracteristiques(transactionDTO.getCaracteristiques());
                            transaction.setTypeTransaction(TypeTransaction.COMMANDE);
                            transaction.setEtat(Etat.VALIDER);
                            transaction.setQuantiteALivre(0);
                            transaction.setQuantiteTotaleLivre(0);

                            totalTransactionCommande = totalTransactionCommande.add(transactionDTO.getPrixVenteUnitaire().multiply(new BigDecimal(transactionDTO.getQuantite())));
                            listT.add(transaction);

                            Produit pP = produitRepository.findById(transaction.getProduitId()).get();
                            pP.setStockClient(pP.getStockClient()+transaction.getQuantite());
                            produitRepository.save(pP);
                        }else {
                            return new MResponse("-1","Aucune devid=se trouve",null);
                        }
                    }

                    if(hasTva){
                        if (commandeDTO.getTvaEnPercent()){
                            tva = totalTransactionCommande.multiply(commandeDTO.getTva().multiply(new BigDecimal("0.01")));
                        } else {
                            tva = commandeDTO.getTva();
                        }
                    }

                    for(FraisDTO frais : commandeDTO.getFrais()) {
                        FraisDTO fraisDTO = new FraisDTO();
                        fraisDTO.setLibelle(frais.getLibelle());
                        fraisDTO.setDeleted(false);
                        fraisDTO.setValeur(frais.getValeur());
                        fraisDTO.setTypeFraisId(frais.getTypeFraisId());
                        ftotal = ftotal.add(frais.getValeur());
                        listF.add(fraisDTO);
                    };

                    BigDecimal somme = tva.add(ftotal).add(totalTransactionCommande);

                    if (somme.compareTo(commandeDTO.getSomme()) == 0) {
                        commande = commandeRepository.save(commandeMapper.toEntity(commandeDTO));
                        Commande finalCommande = commande;

                        for (TransactionDTO t : listT) {
                            t.setCommandeId(finalCommande.getId());
                            transactionRepository.save(transactionMapper.toEntity(t));
                        };

                        for (FraisDTO f : listF) {
                            f.setCommandeId(finalCommande.getId());
                            fraisRepository.save(fraisMapper.toEntity(f));
                        };

                        payAvanceOrAmount(commandeDTO, sessionCaisseDTO.get(), commande.getId());

                    } else {
                        return new MResponse("-1", "Le montant de la commande et la somme des transactions de le commande ne sont pas egale ", null);
                    }

                    if (commande != null && commande.getId() != null) {
                        return new MResponse("0", "Operation effectuee avec succes", null,commande.getId());
                    } else {
                        return new MResponse("-1", "Erreur lors de l'operation", null);
                    }
                } else {
                    return new MResponse("-1", "Erreur sur le montant de la commande ou sur l'avance", null);
                }
            } else {
                return new MResponse("-1", "Veuillez ouvrir une session de caisse", null);
            }
        } else {
            return new MResponse("-1", "Veuillez vous connecter!", null);
        }
    }

    public MResponse saveCommandeFournisseur(CommandeDTO commandeDTO) {
        log.debug("Request to save Commande : {}", commandeDTO);
        Optional<User> user = userService.getUserWithAuthorities();
        Optional<SessionCaisseDTO> sessionCaisseDTO;
        BigDecimal totalTransactionCommande;
        BigDecimal ftotal;
        commandeDTO.setTvaEnPercent(Boolean.FALSE);
        if (user.isPresent()) {
            sessionCaisseDTO = sessionCaisseService.findByUserAndStatut(user.get(), Statut.OUVERT);
            if (sessionCaisseDTO.isPresent()) {
                if (commandeDTO.getSomme() != null && commandeDTO.getSomme().compareTo(BigDecimal.ZERO) >= 0) {
                    totalTransactionCommande = BigDecimal.ZERO;
                    ftotal = BigDecimal.ZERO;

                    List<TransactionDTO> listT = new ArrayList<>();
                    List<FraisDTO> listF = new ArrayList<>();
                    Commande commande = new Commande();

                    commandeDTO.setDate(Instant.now());
                    commandeDTO.setEtat(Etat.VALIDER);
                    long compteur = 0;
                    String code = "";
                    do {
                        compteur++;
                        code = "EF-CO-" + LocalDate.now().getYear() + "-" + String.format("%03d", (commandeRepository.count() + compteur));
                    } while (commandeRepository.findByCode(code).isPresent());
                    commandeDTO.setCode(code);
                    for(TransactionDTO transactionDTO : commandeDTO.getTransactions()) {
                        Optional<Devise> devise = deviseService.findOne(transactionDTO.getDeviseId());
                        TransactionDTO transaction = new TransactionDTO();
                        transaction.setDate(Instant.now());
                        transaction.setDeleted(false);
                        if (devise.isPresent()){
                            if (devise.get().getDeviseBase()!=null){
                                transaction.setPrixAchatUnitaire(transactionDTO.getPrixAchatUnitaire());
                            }else {
                                transaction.setPrixAchatUnitaire(transactionDTO.getPrixAchatUnitaire().multiply(devise.get().getValeur()));
                            }
                            transaction.setValeurDevise(devise.get().getValeur().doubleValue());
                            transaction.setDeviseId(transactionDTO.getDeviseId());
                            transaction.setPrixVenteUnitaire(transactionDTO.getPrixVenteUnitaire());
                            transaction.setQuantite(transactionDTO.getQuantite());
                            transaction.setProduitId(transactionDTO.getProduitId());
                            transaction.setCaracteristiques(transactionDTO.getCaracteristiques());
                            transaction.setTypeTransaction(TypeTransaction.COMMANDE);
                            transaction.setEtat(Etat.VALIDER);
                            transaction.setQuantiteTotaleLivre(0);
                            transaction.setQuantiteALivre(0);
                            totalTransactionCommande = totalTransactionCommande.add(transactionDTO.getPrixAchatUnitaire().multiply(new BigDecimal(transactionDTO.getQuantite())));
                            listT.add(transaction);
                        }else {
                            return new MResponse("-1","Aucune devid=se trouve",null);
                        }
                    };

                    for(FraisDTO frais : commandeDTO.getFrais()) {
                        FraisDTO fraisDTO = new FraisDTO();
                        fraisDTO.setLibelle(frais.getLibelle());
                        fraisDTO.setDeleted(false);
                        fraisDTO.setValeur(frais.getValeur());
                        fraisDTO.setTypeFraisId(frais.getTypeFraisId());
                        ftotal = ftotal.add(frais.getValeur());
                        listF.add(fraisDTO);
                    };

                    BigDecimal somme = ftotal.add(totalTransactionCommande);

                    System.out.println("\n\nsomme :"+somme);
                    System.out.println("\n\ncommandeDTO.getSomme() :"+commandeDTO.getSomme());
                    if (somme.compareTo(commandeDTO.getSomme()) == 0) {
                        commande = commandeRepository.save(commandeMapper.toEntity(commandeDTO));
                        for (EtapeTransportDTO etape : commandeDTO.getEtapeTransports()){
                            EtapeTransportDTO etapeTransportDTO = new EtapeTransportDTO();
                            etapeTransportDTO.setDateArrive(etape.getDateArrive());
                            etapeTransportDTO.setDateDepart(etape.getDateDepart());
                            etapeTransportDTO.setDeleted(false);
                            etapeTransportDTO.setLibelle(etape.getLibelle());
                            etapeTransportDTO.setNiveau(etape.getNiveau());
                            etapeTransportDTO.setCommandeId(commande.getId());
                            etapeTransportDTO.setSocieteTransportId(etape.getSocieteTransportId());
                            etapeTransportService.save(etapeTransportDTO);
                        };

                        for (TransactionDTO t : listT) {
                            t.setCommandeId(commande.getId());
                            transactionRepository.save(transactionMapper.toEntity(t));
                        };

                        for (FraisDTO f : listF) {
                            f.setCommandeId(commande.getId());
                            fraisRepository.save(fraisMapper.toEntity(f));
                        };

                        if (commandeDTO.getListeCommandesClients() != null){
                            Commande cd = new Commande();
                            for (Long id : commandeDTO.getListeCommandesClients()){
                                cd = commandeRepository.findById(id).get();
                                cd.setCommandeFournisseurId(commande.getId());
                                commandeRepository.save(cd);
                            }
                        }

                        System.out.println("somme :"+commandeDTO.getSomme());
                        payAvanceOrAmount(commandeDTO,sessionCaisseDTO.get(),commande.getId());

                    } else {
                        return new MResponse("-1", "Le montant de la commande et la somme des transactions de le commande ne sont pas egale ", null);
                    }

                    if (commande != null && commande.getId() != null) {
                        return new MResponse("0", "Operation effectuee avec succes", null,commande.getId());
                    } else {
                        return new MResponse("-1", "Erreur lors de l'operation", null);
                    }
                } else {
                    return new MResponse("-1", "Erreur sur le montant de la commande", null);
                }
            } else {
                return new MResponse("-1", "Veuillez ouvrir une session de caisse", null);
            }
        } else {
            return new MResponse("-1", "Veuillez vous connecter!", null);
        }
    }

    /**
     * Get all the commandes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<CommandeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Commandes");
        return commandeRepository.findAll(pageable)
            .map(commandeMapper::toDto);
    }


    /**
     * Get one commande by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<CommandeDTO> findOne(Long id) {
        log.debug("Request to get Commande : {}", id);
        List<TransactionDTO> transactionDTOList = new ArrayList<>();
        List<FraisDTO> fraisDTOList = new ArrayList<>();
        Optional<CommandeDTO> commandeDTO;
        Optional<Commande> commande = commandeRepository.findById(id);
        if (commande.isPresent()){
            log.debug("coooooooooooooooooooo {}");
            commande.get().getTransactions().forEach(transaction -> {
                TransactionDTO transactionDTO = transactionMapper.toDto(transaction);
                transactionDTOList.add(transactionDTO);
            });
            commande.get().getFrais().forEach(frais -> {
                FraisDTO fraisDTO = new FraisDTO();
                fraisDTO.setId(frais.getId());
                fraisDTO.setLibelle(frais.getLibelle());
                fraisDTO.setValeur(frais.getValeur());
                fraisDTO.setDeleted(frais.getDeleted());
                fraisDTO.setTypeFraisId(frais.getTypeFrais().getId());
                fraisDTO.setTypeFraisLibelle(frais.getTypeFrais().getLibelle());
                fraisDTOList.add(fraisDTO);
            });
        }
        commandeDTO = commande.map(commandeMapper::toDto);
        if (commande.get().getClient()!=null)
            commandeDTO.get().setRaisonSocial(commande.get().getClient().getRaisonSocial());
        else
            commandeDTO.get().setRaisonSocial(commande.get().getFournisseur().getRaisonSocial());
        commandeDTO.get().setTransactions(transactionDTOList);
        commandeDTO.get().setFrais(fraisDTOList);
       return commandeDTO;
       /* return commandeRepository.findById(id)
            .map(commandeMapper::toDto);*/
    }
    /**
     * Delete the commande by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Commande : {}", id);
        commandeRepository.deleteById(id);
    }

   /* public Page<CommandeDTO> findByCriteria(CommandeDTO commandeDTO, Pageable pageable) {
        if (commandeDTO.getTypeCommande() != null) {
            Page<CommandeDTO> page;
            if (commandeDTO.getTypeCommande().equals(TypeCommande.COMMANDE_CLIENT)) {
                System.out.println("\n\nTypeCommande.COMMANDE_CLIENT COOLLLLLL\n\n");
                List<CommandeDTO> list = commandeRepository
                    .findWithCriteria(pageable, commandeDTO.getLibelle(), commandeDTO.getCode(),TypeCommande.COMMANDE_CLIENT.name(), commandeDTO.getEtat() != null ? commandeDTO.getEtat().name() : null).getContent()
                    .stream()
                    .map(commandeMapper::toDto)
                    .collect(Collectors.toList());
                page = new PageImpl<>(list, pageable, list.size());
            } else {
                List<CommandeDTO> list = commandeRepository.findWithCriteria(pageable, commandeDTO.getLibelle(), commandeDTO.getCode(),TypeCommande.COMMANDE_FOURNISSEUR.name(), commandeDTO.getEtat() != null ? commandeDTO.getEtat().name() : null).getContent().stream().filter(c -> c.getTypeCommande() != null && c.getTypeCommande().equals(TypeCommande.COMMANDE_FOURNISSEUR)).map(commandeMapper::toDto).collect(Collectors.toList());
                page = new PageImpl<>(list, pageable, list.size());
            }
            return page;
        } else {
           return commandeRepository.findWithCriteria(pageable, commandeDTO.getLibelle(), commandeDTO.getCode(),null, commandeDTO.getEtat() != null ? commandeDTO.getEtat().name() : null).map(commandeMapper::toDto);
        }
    }*/


    public Page<CommandeDTO> findByCriteria(CommandeDTO commandeDTO, Pageable pageable) {
        if (commandeDTO.getTypeCommande() != null) {
            if (commandeDTO.getTypeCommande().equals(TypeCommande.COMMANDE_CLIENT)) {
                System.out.println("\n\nTypeCommande.COMMANDE_CLIENT COOLLLLLL\n\n");
                return commandeRepository.findWithCriteria(pageable, commandeDTO.getLibelle(), commandeDTO.getCode(),TypeCommande.COMMANDE_CLIENT.name(), commandeDTO.getEtat() != null ? commandeDTO.getEtat().name() : null).map(commandeMapper::toDto);
            } else
                return commandeRepository.findWithCriteria(pageable, commandeDTO.getLibelle(), commandeDTO.getCode(),TypeCommande.COMMANDE_FOURNISSEUR.name(), commandeDTO.getEtat() != null ? commandeDTO.getEtat().name() : null).map(commandeMapper::toDto);
        } else
            return commandeRepository.findWithCriteria(pageable, commandeDTO.getLibelle(), commandeDTO.getCode(),null, commandeDTO.getEtat() != null ? commandeDTO.getEtat().name() : null).map(commandeMapper::toDto);
    }


    public Optional<CommandeDTO> resetCommandeOrTransaction(AnnulationDTO annulationDTO) throws Exception {
        Optional<SessionCaisseDTO> sessionCaisseDTO;
        Optional<User> user = userService.getUserWithAuthorities();
        sessionCaisseDTO = sessionCaisseService.findByUserAndStatut(user.get(), Statut.OUVERT);
        if (sessionCaisseDTO.isPresent()) {
            if (annulationDTO.getId() != null && annulationDTO.getTransactionId() != null && annulationDTO.getTransactionId().size() > 0) {
                annulationDTO.getTransactionId().forEach(transId -> {
                    Optional.of(transactionRepository
                        .findById(transId))
                        .filter(Optional::isPresent)
                        .map(Optional::get)
                        .map(transaction -> {
                            transaction.setMotif(annulationDTO.getMotif());
                            transaction.setEtat(Etat.ANNULER);
                            log.debug("Changed Information for User: {}", transaction);
                            return transaction;
                        })
                        .map(TransactionDTO::new);
                });
                resetAllPaymentOfCommande(annulationDTO.getId(), sessionCaisseDTO.get());
                searchAndUpdateCommande(annulationDTO);
//                if (transactionRepository.findByCommandeIdAndEtat(annulationDTO.getId(), Etat.VALIDER).size() > 0) {
//                    // TODO: 10/10/2021 il n'y a pas de traitement lorsqu'il existe toujours des tranactions non annulées relatives à la commande
//
//                    return commandeRepository.findById(annulationDTO.getId()).map(commandeMapper::toDto);
//                } else {
//                    return searchAndUpdateCommande(annulationDTO);
//                }
            } else {
                transactionService.resetCommande(Etat.ANNULER, annulationDTO.getId(), annulationDTO.getMotif());
                return searchAndUpdateCommande(annulationDTO);
            }
        } else {
            throw new Exception("Session de caisse introuvable");
        }
        return null;
    }

    @Transactional
    public Optional<CommandeDTO> resetCommande(AnnulationDTO annulationDTO) throws Exception {
        Optional<SessionCaisseDTO> sessionCaisseDTO;
        Optional<User> user = userService.getUserWithAuthorities();
        sessionCaisseDTO = sessionCaisseService.findByUserAndStatut(user.get(), Statut.OUVERT);
        if (sessionCaisseDTO.isPresent()) {
            if (annulationDTO.getId() != null) {
                Optional<Commande> commande = commandeRepository.findById(annulationDTO.getId());
                if (commande.isPresent()){
                    for (Transaction t: commande.get().getTransactions()){
                        t.setMotif(annulationDTO.getMotif());
                        t.setEtat(Etat.ANNULER);
                        transactionRepository.save(t);
                        log.debug("Changed Information for User: {}",t);
                    }
                    resetAllPaymentOfCommande(annulationDTO.getId(), sessionCaisseDTO.get());
                }
                return searchAndUpdateCommande(annulationDTO);
            } else {
                throw new RuntimeException("commandeId est null");
            }
        } else {
            throw new RuntimeException("Session de caisse introuvable");
        }
    }

    public Optional<CommandeDTO> searchAndUpdateCommande(AnnulationDTO annulationDTO) {
        return Optional.of(commandeRepository
            .findById(annulationDTO.getId()))
            .filter(Optional::isPresent)
            .map(Optional::get)
            .map(commande -> {
                commande.setMotif(annulationDTO.getMotif());
                commande.setEtat(Etat.ANNULER);
                log.debug("Changed Information for User: {}", commande);
                return commande;
            })
            .map(CommandeDTO::new);
    }

    public PaiementDTO payAvanceOrAmount(CommandeDTO c, SessionCaisseDTO s, long idC) {

        PaiementDTO p = new PaiementDTO();
        p.setDate(Instant.now());
        p.setSessioncaisseId(s.getId());
        p.setCommandeId(idC);
        if (c.getTypeCommande().equals(TypeCommande.COMMANDE_CLIENT)){
            if (c.isAvanceEnPercent()) {
                BigDecimal m = c.getSomme().multiply(c.getAvance().multiply(new BigDecimal("0.01")));
                s.setSommeFin(s.getSommeFin().add(m));
                p.setMontant(m);
            } else {
                s.setSommeFin(s.getSommeFin().add(c.getAvance()));
                p.setMontant(c.getAvance());
            }
        } else {
            s.setSommeFin(s.getSommeFin().subtract(c.getSomme()));
            p.setMontant(c.getSomme().negate());
        }
         p.setMotif("Avance sur la commande "+c.getCode());

        sessionCaisseRepository.save(sessionCaisseMapper.toEntity(s));
        paiementRepository.save(paiementMapper.toEntity(p));

        return p;
    }

    public void resetAllPaymentOfCommande(long idC, SessionCaisseDTO s) {
        Commande c = commandeRepository.findById(idC).get();
        for (Paiement p : c.getPaiements()) {
            p.setEtat(Etat.ANNULER);
            p.setMotif("Annulation de commande");
            paiementRepository.save(p);
            s.setSommeFin(s.getSommeFin().subtract(p.getMontant()));
            sessionCaisseRepository.save(sessionCaisseMapper.toEntity(s));

//            if (!p.isAffecteCaisse()){
//                OperationCaisseDTO opC = new OperationCaisseDTO();
//                opC.setTypeOperationCaisse(TypeOperationCaisse.MOUVEMENT);
//                // TODO: 11/10/2021 revoir le message de description
//                opC.setDescription("Annulation de commande");
//                opC.setCaisseSrcId(p.getSessioncaisse().getCaisse().getId());
//                opC.setSessionCaisseId(p.getSessioncaisse().getId());
//                opC.setMontant(p.getMontant());
//
//                operationCaisseService.saveRemboursement(opC);
//            }
        }
    }

    public List<Commande> findByAllCommande() {
        return commandeRepository.findAll();
    }

    public List<Commande> getCommandeClientByCommandeFournisseurId(Long id) {
        return commandeRepository.findAllByCommandeFournisseurId(id);
    }

    public List<Commande> getCommandeClientByCommandeFournisseurIsNull() {
        return commandeRepository.findAllByFournisseurIsNullAndCommandeFournisseurIdIsNullAndDateLivraisonIsNull();
    }

    public MResponse doLivraison(CommandeDTO c) {

        c.setDateLivraison(Instant.now());
        commandeRepository.save(commandeMapper.toEntity(c));

        for (TransactionDTO t : c.getTransactions()){
            if (t.getQuantiteALivre() >= 0 ){
                t.setDateLivre(Instant.now());
                Optional<Produit> p = produitRepository.findById(t.getProduitId());
                if (p.isPresent()){

                    if (c.getTypeCommande().equals(TypeCommande.COMMANDE_FOURNISSEUR)){
                        approService.updateProduit(p.get(),t);
                        p.get().setQuantite(p.get().getQuantite()+t.getQuantiteALivre());
                    } else {
                        p.get().setQuantite(p.get().getQuantite()-t.getQuantiteALivre());
                        p.get().setStockClient(p.get().getStockClient()-t.getQuantiteALivre());
                        Optional<User> user = userService.getUserWithAuthorities();
                        Optional<SessionCaisseDTO> sessionCaisseDTO = sessionCaisseService.findByUserAndStatut(user.get(), Statut.OUVERT);
                        if (sessionCaisseDTO != null){
                            SessionCaisse s = sessionCaisseMapper.toEntity(sessionCaisseDTO.get());
                            PaiementDTO p1 = new PaiementDTO();
                            List<Paiement> pList = paiementRepository.findByCommandeId(c.getId());
                            BigDecimal sommePaye = BigDecimal.ZERO;
                            if (pList != null)
                                sommePaye = pList.stream().map(Paiement::getMontant).reduce(BigDecimal.ZERO,BigDecimal::add);
                            p1.setMontant(calculSommeCommande(c).subtract(sommePaye));
                            p1.setSessioncaisseId(sessionCaisseDTO.get().getId());
                            p1.setCommandeId(c.getId());
                            p1.setDate(Instant.now());
                            p1.setEtat(Etat.VALIDER);
                            p1.setAffecteCaisse(false);
                            p1.setMotif("Paiement complémentaire lors de la livraison");
                            paiementRepository.save(paiementMapper.toEntity(p1));
                            s.setSommeFin(s.getSommeFin().add(p1.getMontant()));
                            sessionCaisseRepository.save(s);
                        } else {
                            return  new MResponse("","Aucune session de caisse","-1",null);
                        }
                    }
                    produitRepository.save(p.get());
                }
                transactionService.save(t);
            } else {
                return new  MResponse("","QuantiteLivre est null ou transaction déjà livrée","-1",null);
            }
        }
        return new  MResponse("","livraison réussie","0",c.getId());
    }

    public BigDecimal calculSommeCommande(CommandeDTO c){
       BigDecimal totalTransactionCommande = BigDecimal.ZERO;
       BigDecimal ftotal = BigDecimal.ZERO;
       BigDecimal tv = BigDecimal.ZERO;
       Boolean  hasTva = clientService.findOne(c.getClientId()).get().getPayerTva();
                    for(TransactionDTO transactionDTO : c.getTransactions()) {

                        totalTransactionCommande = totalTransactionCommande.add(transactionDTO.getPrixVenteUnitaire()
                            .multiply(new BigDecimal(transactionDTO.getQuantite().toString())));
                    };

                    // pour les commande de type client
                    if (c.getTypeCommande().equals(TypeCommande.COMMANDE_CLIENT) && hasTva){
                        if (c.getTvaEnPercent()){
                            tv = totalTransactionCommande.multiply(c.getTva()
                                .multiply(new BigDecimal("0.01")));
                        } else {
                            tv = c.getTva();
                        }
                    }

                    for(FraisDTO frais : c.getFrais()) {
                        ftotal = ftotal.add(frais.getValeur());
                    };

                    BigDecimal somme = tv.add(ftotal).add(totalTransactionCommande);

            return somme;
    }
    public List<String>findTypeCommande(Instant dateStart,Instant dateEnd){
        return commandeRepository.findTypeCommande(dateStart,dateEnd);
    }

    public MResponse doLivraisonNewOld(CommandeDTO c) throws Exception {

        if (c.getTransactions().size() == 0)
            throw new Exception("La liste des transactions est vide");

        long compteur = 0;
        String code = "";
        do {
            compteur++;
            code = "EF-LI-" + LocalDate.now().getYear() + "-" + String.format("%03d", (livraisonRepository.count() + compteur));
        } while (livraisonRepository.findByCode(code).isPresent());

        LivraisonDTO l = new LivraisonDTO();

        l.setCode(code);
        l.setCommandeId(c.getId());
        l.setLibelle("Livraison du commande numero "+c.getCode());
        l.setEtat(Etat.VALIDER);
        l.setDate(Instant.now());

        Livraison livraison = livraisonRepository.save(livraisonMapper.toEntity(l));

        for (TransactionDTO t : c.getTransactions()){
            if (t.getQuantiteALivre() >= 0 && (t.getQuantiteALivre()+t.getQuantiteTotaleLivre()) <= t.getQuantite()){

                TransactionDTO transactionDTO = new TransactionDTO();

                transactionDTO.setDateLivre(l.getDate());
                transactionDTO.setTypeTransaction(TypeTransaction.LIVRAISON);
                transactionDTO.setLivraisonId(livraison.getId());
                transactionDTO.setDateLivre(l.getDate());
                transactionDTO.setTransactionParentId(t.getId());
                transactionDTO.setQuantiteALivre(t.getQuantiteALivre());
                transactionDTO.setPrixVenteUnitaire(t.getPrixVenteUnitaire());


                Optional<Produit> p = produitRepository.findById(t.getProduitId());
                if (p.isPresent()){

                    if (c.getTypeCommande().equals(TypeCommande.COMMANDE_FOURNISSEUR)){
                        approService.updateProduit(p.get(),t);
                        p.get().setQuantite(p.get().getQuantite()+t.getQuantiteALivre());
                    } else {
                        p.get().setQuantite(p.get().getQuantite()-t.getQuantiteALivre());
                        p.get().setStockClient(p.get().getStockClient()-t.getQuantiteALivre());
                        Optional<User> user = userService.getUserWithAuthorities();
                        Optional<SessionCaisseDTO> sessionCaisseDTO = sessionCaisseService.findByUserAndStatut(user.get(), Statut.OUVERT);
                        if (sessionCaisseDTO != null){
                            SessionCaisse s = sessionCaisseMapper.toEntity(sessionCaisseDTO.get());
                            PaiementDTO p1 = new PaiementDTO();
                            List<Paiement> pList = paiementRepository.findByCommandeId(c.getId());
                            BigDecimal sommePaye = BigDecimal.ZERO;
                            if (pList != null)
                                sommePaye = pList.stream().map(Paiement::getMontant).reduce(BigDecimal.ZERO,BigDecimal::add);
                            BigDecimal resteAPayer = calculSommeCommande(c).subtract(sommePaye);
                            BigDecimal montantTransaction = transactionDTO.getPrixVenteUnitaire().multiply(new BigDecimal(transactionDTO.getQuantiteALivre().toString()));

                            if (resteAPayer.compareTo(montantTransaction) >= 0){
                                if (resteAPayer.compareTo(montantTransaction) == 0)
                                    c.setStatut(Statut.FERME);
                                p1.setMontant(montantTransaction);
                                p1.setSessioncaisseId(sessionCaisseDTO.get().getId());
                                p1.setCommandeId(c.getId());
                                p1.setDate(l.getDate());
                                p1.setEtat(Etat.VALIDER);
                                p1.setAffecteCaisse(false);
                                p1.setMotif("Paiement complémentaire lors de la livraison");
                                paiementRepository.save(paiementMapper.toEntity(p1));
                                s.setSommeFin(s.getSommeFin().add(p1.getMontant()));
                                sessionCaisseRepository.save(s);
                                livraison.setSomme(livraison.getSomme().add(montantTransaction));
                            } else {
                                throw new Exception("Le montant de la transaction est superieur au montant restant de la commande");
                            }
                        } else {
                            throw new Exception("Aucune session de caisse");
                        }
                    }
                    produitRepository.save(p.get());
                }
                t.setQuantiteTotaleLivre(t.getQuantiteTotaleLivre()+t.getQuantiteALivre());
                transactionService.save(t);
                transactionService.save(transactionDTO);
            } else {
                throw new Exception("QuantiteALivre est null ou transaction déjà livrée");
               // return new  MResponse("","QuantiteALivre est null ou transaction déjà livrée","-1",null);
            }
        }

        livraisonRepository.save(livraison);
        c.setDateLivraison(livraison.getDate());
        commandeRepository.save(commandeMapper.toEntity(c));
        return new  MResponse("","livraison réussie","0",c.getId());
    }


    @Transactional
    public MResponse doLivraisonNew(CommandeDTO c) throws Exception {

        if (c.getTransactions().size() == 0)
            throw new Exception("La liste des transactions est vide");

        long compteur = 0;
        String code = "";
        do {
            compteur++;
            code = "EF-LI-" + LocalDate.now().getYear() + "-" + String.format("%03d", (livraisonRepository.count() + compteur));
        } while (livraisonRepository.findByCode(code).isPresent());

        LivraisonDTO l = new LivraisonDTO();

        l.setCode(code);
        l.setSomme(BigDecimal.ZERO);
        l.setCommandeId(c.getId());
        l.setLibelle("Livraison de la commande N° "+c.getCode());
        l.setEtat(Etat.VALIDER);
        l.setDate(Instant.now());

        Livraison livraison = livraisonRepository.save(livraisonMapper.toEntity(l));

        for (TransactionDTO t : c.getTransactions()){
            if (t.getQuantiteALivre() >= 0 && (t.getQuantiteALivre()+t.getQuantiteTotaleLivre()) <= t.getQuantite()){

                TransactionDTO transactionDTO = new TransactionDTO();

                transactionDTO.setDateLivre(l.getDate());
                transactionDTO.setTypeTransaction(TypeTransaction.LIVRAISON);
                transactionDTO.setLivraisonId(livraison.getId());
                transactionDTO.setDateLivre(l.getDate());
                transactionDTO.setTransactionParentId(t.getId());
                transactionDTO.setQuantiteALivre(t.getQuantiteALivre());
                transactionDTO.setPrixVenteUnitaire(t.getPrixVenteUnitaire());
                transactionDTO.setPrixAchatUnitaire(t.getPrixAchatUnitaire());
                transactionDTO.setProduitId(t.getProduitId());
                transactionDTO.setCaracteristiques(t.getCaracteristiques());

                Optional<Produit> p = produitRepository.findById(t.getProduitId());
                if (p.isPresent()){

                    if (c.getTypeCommande().equals(TypeCommande.COMMANDE_FOURNISSEUR)){
                        approService.updateProduit(p.get(),t);
                        p.get().setQuantite(p.get().getQuantite()+t.getQuantiteALivre());
                    } else {
                        if (p.get().getQuantite() > t.getQuantiteALivre()){
                            p.get().setQuantite(p.get().getQuantite()-t.getQuantiteALivre());
                            p.get().setStockClient(p.get().getStockClient()-t.getQuantiteALivre());

                            BigDecimal montantTransaction = transactionDTO.getPrixVenteUnitaire().multiply(new BigDecimal(transactionDTO.getQuantiteALivre().toString()));
                            livraison.setSomme(livraison.getSomme().add(montantTransaction));
                        } else {
                            throw new Exception("Quantité du produit insuffisant");
                        }
                    }
                    produitRepository.save(p.get());
                } else {
                    throw new Exception("Produit n'existe pas");
                }
                t.setQuantiteTotaleLivre(t.getQuantiteTotaleLivre()+t.getQuantiteALivre());
                transactionService.save(t);
                transactionService.save(transactionDTO);
            } else {
                throw new Exception("QuantiteALivre est null ou transaction déjà livrée");
            }
        }

        Livraison livraison1 = livraisonRepository.save(livraison);
        c.setDateLivraison(livraison.getDate());
        commandeRepository.save(commandeMapper.toEntity(c));
        return new  MResponse("0","livraison réussie","OK",livraison1.getId());
    }

    @Transactional
    public MResponse paiementCommande(PaiementDTO paiementDTO) {
        Optional<SessionCaisseDTO> sessionCaisseDTO;
        Optional<User> user = userService.getUserWithAuthorities();
        Paiement paiement = new Paiement();
        BigDecimal montantSoldeInit;
        BigDecimal montantSoldeFinal;
        sessionCaisseDTO = sessionCaisseService.findByUserAndStatut(user.get(), Statut.OUVERT);
        if (sessionCaisseDTO.isPresent()) {
            if (paiementDTO.getCommandeId() != null && paiementDTO.getMontant().compareTo(BigDecimal.ZERO)>0) {
                Optional<Commande> commande = commandeRepository.findById(paiementDTO.getCommandeId());
                BigDecimal paimentCommande= paiementRepository.getMontantPaimentCommande(paiementDTO.getCommandeId());
                paimentCommande = paimentCommande.add(paiementDTO.getMontant());
                if (commande.isPresent()){
                    if (commande.get().getSomme().compareTo(commande.get().getMontantSolde())>0){
                        if (commande.get().getSomme().compareTo(paimentCommande)==0)
                            commande.get().setEstSolde(true);
                        montantSoldeInit = commande.get().getMontantSolde();
                        montantSoldeFinal = montantSoldeInit.add(paiementDTO.getMontant());
                        commande.get().setMontantSolde(montantSoldeFinal);
                        commandeRepository.save(commande.get());
                        paiement.setMontant(paiementDTO.getMontant());
                        paiement.setCommande(commande.get());
                        paiement.setSessioncaisse(sessionCaisseMapper.toEntity(sessionCaisseDTO.get()));
                        paiement.setMotif(paiementDTO.getMotif());
                        paiement.setDate(paiementDTO.getDate());
                        paiement = paiementRepository.save(paiement);
                        if (paiement.getId()!=null)
                            return new MResponse("0","Operation effectuee avec succes","OK");
                        else
                            return new MResponse("-1","Erreur lors de l'operation","NONOK");
                    }else if (commande.get().getSomme().compareTo(commande.get().getMontantSolde())==0)
                        return new MResponse("0","Commande deja soldée","OK");
                    else
                        return new MResponse("-1","Le montant solde est superieur au montant de la commande","NONOK");
                }else
                    throw new RuntimeException("Commande introuvable");
            } else
                throw new RuntimeException("Commande est null");
        } else
            throw new RuntimeException("Session de caisse introuvable");
    }
}
