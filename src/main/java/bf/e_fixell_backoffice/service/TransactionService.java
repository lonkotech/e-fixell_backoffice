package bf.e_fixell_backoffice.service;

import bf.e_fixell_backoffice.domain.Commande;
import bf.e_fixell_backoffice.domain.Transaction;
import bf.e_fixell_backoffice.domain.enumeration.Etat;
import bf.e_fixell_backoffice.domain.enumeration.TypeCommande;
import bf.e_fixell_backoffice.repository.CommandeRepository;
import bf.e_fixell_backoffice.repository.ProduitRepository;
import bf.e_fixell_backoffice.repository.TransactionRepository;
import bf.e_fixell_backoffice.service.dto.StatistiqueDTO;
import bf.e_fixell_backoffice.service.dto.TransactionDTO;
import bf.e_fixell_backoffice.service.mapper.TransactionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Transaction}.
 */
@Service
@Transactional
public class TransactionService {

    private final Logger log = LoggerFactory.getLogger(TransactionService.class);

    private final TransactionRepository transactionRepository;

    private final TransactionMapper transactionMapper;

    private final CommandeRepository commandeRepository;

    private final ProduitRepository produitRepository;

    BigDecimal ttrans;
    BigDecimal tfrais;
    BigDecimal ttransIgnore;

    public TransactionService(TransactionRepository transactionRepository, TransactionMapper transactionMapper, CommandeRepository commandeRepository, ProduitRepository produitRepository) {
        this.transactionRepository = transactionRepository;
        this.transactionMapper = transactionMapper;
        this.commandeRepository = commandeRepository;
        this.produitRepository = produitRepository;
    }

    /**
     * Save a transaction.
     *
     * @param transactionDTO the entity to save.
     * @return the persisted entity.
     */
    @Transactional(readOnly = true)
    public TransactionDTO save(TransactionDTO transactionDTO) {
        log.debug("Request to save Transaction : {}", transactionDTO);
        Transaction transaction = transactionMapper.toEntity(transactionDTO);
        transaction = transactionRepository.save(transaction);
        return transactionMapper.toDto(transaction);
    }

    /**
     * Get all the transactions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<TransactionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Transactions");
        return transactionRepository.findAll(pageable)
            .map(transactionMapper::toDto);
    }


    /**
     * Get one transaction by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<TransactionDTO> findOne(Long id) {
        log.debug("Request to get Transaction : {}", id);
        return transactionRepository.findById(id)
            .map(transactionMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Set<Transaction> findByApproId(Long id) {
        log.debug("Request to get Transaction : {}", id);
        return transactionRepository.findByApprovisionnement_Id(id);
    }

    @Transactional(readOnly = true)
    public List<TransactionDTO> findByLivraisonId(Long id) {
        log.debug("Request to get Transaction : {}", id);
        return transactionRepository.findByLivraisonId(id).stream().map(transactionMapper::toDto).collect(Collectors.toList());
    }

    /**
     * Delete the transaction by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Transaction : {}", id);
        transactionRepository.deleteById(id);
    }

    public int resetCommande(Etat etat, Long id, String motif) {
        return transactionRepository.resetTransaction(etat, id, motif);
    }


    public void resetTransactionOfDifferentCommande(List<Long> listTransactionId) {

        for (Long id : listTransactionId) {
            Transaction t = transactionRepository.findById(id).get();
            t.etat(Etat.ANNULER);

            Commande c = t.getCommande();

            if (c.getTvaEnPercent()) {
                ttrans = BigDecimal.ZERO;
                tfrais = BigDecimal.ZERO;

                c.getTransactions().forEach(tr -> {
                    ttrans = ttrans.add(tr.getPrixUnitaire().multiply(new BigDecimal(tr.getQuantite())));
                });

                c.getFrais().forEach(fr -> {
                    tfrais = tfrais.add(fr.getValeur());
                });

                BigDecimal tHT = ttrans.subtract(t.getPrixUnitaire().multiply(new BigDecimal(t.getQuantite().toString())));
                BigDecimal tva = tHT.multiply(c.getTva().multiply(new BigDecimal("0.01")));
                c.setSomme(tHT.add(tva.add(tfrais)));

            } else {
                c.setSomme(c.getSomme().subtract(t.getPrixUnitaire().multiply(new BigDecimal(t.getQuantite().toString()))));
            }

            if (c.getAvanceEnPercent()) {
                c.setAvance(c.getSomme().multiply(c.getAvance().multiply(new BigDecimal("0.01"))));
            }

            transactionRepository.save(t);
            commandeRepository.save(c);
        }
    }

    public void resetTransactionOfSameCommande(List<Long> listTransactionId) {

        ttrans = BigDecimal.ZERO;
        tfrais = BigDecimal.ZERO;

        Transaction t = transactionRepository.findById(listTransactionId.get(0)).get();
        Commande c = t.getCommande();

        List<Transaction> listT = new ArrayList<>(c.getTransactions());

        listT.stream().map(trns -> {
            if (!listTransactionId.contains(trns.getId())) {
                this.ttrans = this.ttrans.add(trns.getPrixUnitaire().multiply(new BigDecimal(trns.getQuantite().toString())));
            } else {
                this.ttransIgnore = this.ttransIgnore.add(trns.getPrixUnitaire().multiply(new BigDecimal(trns.getQuantite().toString())));
                trns.setEtat(Etat.ANNULER);
                transactionRepository.save(trns);
            }
            return this;
        });

        if (c.getTvaEnPercent()) {
            BigDecimal tva = ttrans.multiply(t.getCommande().getTva().multiply(new BigDecimal("0.01")));

            t.getCommande().getFrais().forEach(fr -> {
                this.tfrais = this.tfrais.add(fr.getValeur());
            });

            c.setSomme(ttrans.add(tfrais.add(tva)));
        } else {
            c.setSomme(c.getSomme().subtract(ttransIgnore));
        }
        commandeRepository.save(c);
    }

    public List<TransactionDTO> getAllTransactionsByCommands(List<Long> listId) {

        List<Transaction> transactionList = new ArrayList<>();
        for (Long id : listId) {
            transactionList.addAll(transactionRepository.findByCommandeIdAndEtat(id, Etat.VALIDER));
        }
        return transactionList.stream().map(transactionMapper::toDto).collect(Collectors.toList());
    }

    public List<StatistiqueDTO> getStatistique(StatistiqueDTO statistiqueDTO) {
        List<Long> produitIds = produitRepository.finProduitId();
        List<StatistiqueDTO> statistiqueDTOList = new ArrayList<>();
        List<Transaction> transaction;
        List<Transaction> transactionCommandeClient;
        List<Transaction> transactionCommandeFournisseur;
        List<Transaction> transactionVente;
        if (statistiqueDTO.getDateDebut() != null && statistiqueDTO.getDateFin() != null){
            for(Long id : produitIds) {
                StatistiqueDTO statistique = new StatistiqueDTO();
                transaction = transactionRepository.findByProduitIdAndDateBetween(id,statistiqueDTO.getDateDebut(),statistiqueDTO.getDateFin());
                if (!transaction.isEmpty()){
                    transactionCommandeClient = transaction.stream().filter(tr-> tr.getCommande().getTypeCommande().equals(TypeCommande.COMMANDE_CLIENT)).collect(Collectors.toList());
                    transactionCommandeFournisseur = transaction.stream().filter(tr-> tr.getCommande().getTypeCommande().equals(TypeCommande.COMMANDE_FOURNISSEUR)).collect(Collectors.toList());
                    transactionVente = transaction.stream().filter(tr-> tr.getVente()!=null && tr.getVente().getId()!=null).collect(Collectors.toList());
                    statistique.setLibelleProduit(transaction.get(0).getProduit().getLibelle());
                    statistique.setQuantiteCommandeClient(transactionCommandeClient.size());
                    statistique.setQuantiteCommandeFournisseur(transactionCommandeFournisseur.size());
                    statistique.setQuantiteVente(transactionVente.size());
                    statistiqueDTOList.add(statistique);
                }
            }
        } else if(statistiqueDTO.getDateDebut() != null){
            for(Long id : produitIds) {
                StatistiqueDTO statistique = new StatistiqueDTO();
                transaction = transactionRepository.findByProduitIdAndDateBetween(id,statistiqueDTO.getDateDebut(),Instant.now());
                if (!transaction.isEmpty()){
                    transactionCommandeClient = transaction.stream().filter(tr-> tr.getCommande().getTypeCommande().equals(TypeCommande.COMMANDE_CLIENT)).collect(Collectors.toList());
                    transactionCommandeFournisseur = transaction.stream().filter(tr-> tr.getCommande().getTypeCommande().equals(TypeCommande.COMMANDE_FOURNISSEUR)).collect(Collectors.toList());
                    transactionVente = transaction.stream().filter(tr-> tr.getVente()!=null && tr.getVente().getId()!=null).collect(Collectors.toList());
                    statistique.setLibelleProduit(transaction.get(0).getProduit().getLibelle());
                    statistique.setQuantiteCommandeClient(transactionCommandeClient.size());
                    statistique.setQuantiteCommandeFournisseur(transactionCommandeFournisseur.size());
                    statistique.setQuantiteVente(transactionVente.size());
                    statistiqueDTOList.add(statistique);
                }
            }
        }
        else{
            for(Long id : produitIds) {
                StatistiqueDTO statistique = new StatistiqueDTO();
                transaction = transactionRepository.findByProduitIdAndDateBetween(id,Instant.now(),Instant.now());
               if (!transaction.isEmpty()){
                   transactionCommandeClient = transaction.stream().filter(tr-> tr.getCommande().getTypeCommande().equals(TypeCommande.COMMANDE_CLIENT)).collect(Collectors.toList());
                   transactionCommandeFournisseur = transaction.stream().filter(tr-> tr.getCommande().getTypeCommande().equals(TypeCommande.COMMANDE_FOURNISSEUR)).collect(Collectors.toList());
                   transactionVente = transaction.stream().filter(tr-> tr.getVente()!=null && tr.getVente().getId()!=null).collect(Collectors.toList());
                   statistique.setLibelleProduit(transaction.get(0).getProduit().getLibelle());
                   statistique.setQuantiteCommandeClient(transactionCommandeClient.size());
                   statistique.setQuantiteCommandeFournisseur(transactionCommandeFournisseur.size());
                   statistique.setQuantiteVente(transactionVente.size());
                   statistiqueDTOList.add(statistique);
               }
            }
        }
        return statistiqueDTOList;
    }

}

