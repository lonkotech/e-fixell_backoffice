package bf.e_fixell_backoffice.service;

import bf.e_fixell_backoffice.domain.Devise;
import bf.e_fixell_backoffice.repository.DeviseRepository;
import bf.e_fixell_backoffice.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Devise}.
 */
@Service
@Transactional
public class DeviseService {
    private static final String ENTITY_NAME = "devise";


    private final Logger log = LoggerFactory.getLogger(DeviseService.class);

    private final DeviseRepository deviseRepository;

    public DeviseService(DeviseRepository deviseRepository) {
        this.deviseRepository = deviseRepository;
    }

    /**
     * Save a devise.
     *
     * @param devise the entity to save.
     * @return the persisted entity.
     */
    public Devise save(Devise devise) {
        log.debug("Request to save Devise : {}", devise);
        if (devise.getDeviseBase()!=null){
            if (devise.getId()!=null){
                Optional<Devise> dv = findOne(devise.getId());
                if (dv.isPresent() && dv.get().getDeviseBase()==null){
                    throw new BadRequestAlertException("Une devise de base est deja defini", ENTITY_NAME, "devise base existe");
                }
            }else {
                Devise dv = deviseRepository.findByDeviseBase();
                if (dv!=null && dv.getId()!=null)
                    throw new BadRequestAlertException("Une devise de base est deja defini", ENTITY_NAME, "devise base existe");
            }
        }
        return deviseRepository.save(devise);
    }

    /**
     * Get all the devises.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<Devise> findAll() {
        log.debug("Request to get all Devises");
        return deviseRepository.findAll();
    }

    /**
     * Get one devise by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Devise> findOne(Long id) {
        log.debug("Request to get Devise : {}", id);
        return deviseRepository.findById(id);
    }

    /**
     * Delete the devise by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Devise : {}", id);
        deviseRepository.deleteById(id);
    }
}
