package bf.e_fixell_backoffice.service;

import bf.e_fixell_backoffice.domain.EtapeTransport;
import bf.e_fixell_backoffice.repository.EtapeTransportRepository;
import bf.e_fixell_backoffice.service.dto.EtapeTransportDTO;
import bf.e_fixell_backoffice.service.mapper.EtapeTransportMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link EtapeTransport}.
 */
@Service
@Transactional
public class EtapeTransportService {

    private final EtapeTransportMapper etapeTransportMapper;

    private final EtapeTransportRepository etapeTransportRepository;

    public EtapeTransportService(EtapeTransportMapper etapeTransportMapper, EtapeTransportRepository etapeTransportRepository) {
        this.etapeTransportMapper = etapeTransportMapper;
        this.etapeTransportRepository = etapeTransportRepository;
    }


    /**
     * Save a categorie.
     *
     * @param etapeTransportDTO the entity to save.
     * @return the persisted entity.
     */
    public EtapeTransportDTO save(EtapeTransportDTO etapeTransportDTO) {
        EtapeTransport etapeTransport = etapeTransportMapper.toEntity(etapeTransportDTO);
        etapeTransport = etapeTransportRepository.save(etapeTransport);
        return etapeTransportMapper.toDto(etapeTransport);
    }

}
