package bf.e_fixell_backoffice.service;

import bf.e_fixell_backoffice.domain.*;
import bf.e_fixell_backoffice.domain.enumeration.Etat;
import bf.e_fixell_backoffice.domain.enumeration.Statut;
import bf.e_fixell_backoffice.domain.enumeration.TypeTransaction;
import bf.e_fixell_backoffice.repository.*;
import bf.e_fixell_backoffice.security.SecurityUtils;
import bf.e_fixell_backoffice.service.dto.AnnulationDTO;
import bf.e_fixell_backoffice.service.dto.SessionCaisseDTO;
import bf.e_fixell_backoffice.service.dto.TransactionDTO;
import bf.e_fixell_backoffice.service.dto.VenteDTO;
import bf.e_fixell_backoffice.service.mapper.SessionCaisseMapper;
import bf.e_fixell_backoffice.service.mapper.TransactionMapper;
import bf.e_fixell_backoffice.service.mapper.VenteMapper;
import bf.e_fixell_backoffice.web.rest.errors.CustomParameterizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Vente}.
 */
@Service
@Transactional
public class VenteService {

    private final Logger log = LoggerFactory.getLogger(VenteService.class);

    private final VenteRepository venteRepository;

    private final VenteMapper venteMapper;

    private final ClientRepository clientRepository;

    private final UserRepository userRepository;

    private final SessionCaisseRepository sessionCaisseRepository;

    private final CaisseRepository caisseRepository;

    private final ProduitRepository produitRepository;

    private final TransactionRepository transactionRepository;

    private final PaiementRepository paiementRepository;

    private final UserService userService;

    private final ApprovisionnementService approvisionnementService;

    private final SessionCaisseService sessionCaisseService;

    private final TransactionMapper transactionMapper;

    private final SessionCaisseMapper sessionCaisseMapper;

    public VenteService(VenteRepository venteRepository, VenteMapper venteMapper, ClientRepository clientRepository, UserRepository userRepository, SessionCaisseRepository sessionCaisseRepository, CaisseRepository caisseRepository, ProduitRepository produitRepository, TransactionRepository transactionRepository, PaiementRepository paiementRepository, UserService userService, ApprovisionnementService approvisionnementService, SessionCaisseService sessionCaisseService, TransactionMapper transactionMapper, SessionCaisseMapper sessionCaisseMapper) {
        this.venteRepository = venteRepository;
        this.venteMapper = venteMapper;
        this.clientRepository = clientRepository;
        this.userRepository = userRepository;
        this.sessionCaisseRepository = sessionCaisseRepository;
        this.caisseRepository = caisseRepository;
        this.produitRepository = produitRepository;
        this.transactionRepository = transactionRepository;
        this.paiementRepository = paiementRepository;
        this.userService = userService;
        this.approvisionnementService = approvisionnementService;
        this.sessionCaisseService = sessionCaisseService;
        this.transactionMapper = transactionMapper;
        this.sessionCaisseMapper = sessionCaisseMapper;
    }

    /**
     * Save a vente.
     *
     * @param venteDTO the entity to save.
     * @return the persisted entity.
     */
    @Transactional
    public VenteDTO save(VenteDTO venteDTO) {
        log.debug("Request to save Vente : {}", venteDTO);
        Vente vente = venteMapper.toEntity(venteDTO);
        String userLogin = SecurityUtils.getCurrentUserLogin().orElseThrow(() -> new CustomParameterizedException("Current user login not found"));
        Optional<User> user= Optional.empty();
        if(userLogin!=null){
            user =userRepository.findOneByLogin(userLogin);
            if(user.isPresent()){

               Optional<SessionCaisse> sessionCaisse=sessionCaisseRepository.findByUserAndStatut(user.get(), Statut.OUVERT);

                if(venteDTO.getClientId()!=null) {
                    Client client= clientRepository.getOne(venteDTO.getClientId());
                    vente.setClient(client);
                } else {
                    throw new CustomParameterizedException("Client non trouvé ");
                }

               if(sessionCaisse.isPresent()){
                   Caisse caisse=sessionCaisse.get().getCaisse();
                   // TODO: 31/10/2021 calcul du montant total de la vente
                   BigDecimal montantTotal= getMontantTotalVente(venteDTO.getTransactions());
                   venteDTO.setMontantSolder(new BigDecimal(150000));
                   // TODO: 31/10/2021 calcul du montant total si tva et application

                   if(venteDTO.getTvaEnPercent()!=null){
                       BigDecimal tva = montantTotal.multiply(venteDTO.getTva().multiply(new BigDecimal("0.01")));
                        montantTotal=montantTotal.add(tva);
                   }

                   if(montantTotal.compareTo(venteDTO.getMontantSolder())>0){
                       throw new CustomParameterizedException("");
                   }

                   // TODO: 31/10/2021 controle sur les paiements à moitier
                   if(vente.getAvance().compareTo(montantTotal)>=0) {
                       vente.setSolder(true);
                       vente.setReste(new BigDecimal(0));
                   }else {
                       vente.setReste(venteDTO.getMontant().subtract(venteDTO.getAvance()));
                       vente.setSolder(false);
                   }
                   vente.setDate(Instant.now());

                   vente = venteRepository.save(vente);
                    log.debug("transaction********"+venteDTO.getTransactions());
                   saveTransaction(vente,venteDTO.getTransactions());
                   doPaiementVente(vente,sessionCaisse.get(),montantTotal);
               } else {
                   throw new CustomParameterizedException("Veillez rendre actif votre session ");
               }
            } else {
                throw new CustomParameterizedException("Utilisateur not found!! ");
            }
        }

        return venteMapper.toDto(vente);
    }



    /**
     * Get all the ventes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<VenteDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Ventes");
        return venteRepository.findAll(pageable)
            .map(venteMapper::toDto);
    }


    /**
     * Get one vente by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<VenteDTO> findOne(Long id) {
        log.debug("Request to get Vente : {}", id);
        return venteRepository.findById(id)
            .map(venteMapper::toDto);
    }

    /**
     * Delete the vente by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Vente : {}", id);
        venteRepository.deleteById(id);
    }

    public void saveTransaction(Vente vente, List<TransactionDTO> transactionList){
        Transaction transaction=new Transaction();
        for(TransactionDTO transactionDTO: transactionList){
            if(transactionDTO.getProduitId()!=null){
                transaction=transactionMapper.toEntity(transactionDTO);
                Produit produit= produitRepository.getOne(transactionDTO.getProduitId());
                if(produit.getId()!=null){
                    if(produit.getQuantite()>=transactionDTO.getQuantite()){
                        transaction.setProduit(produit);
                        produit.setQuantite(produit.getQuantite()-transactionDTO.getQuantite());

                    } else {
                        throw new CustomParameterizedException(" Stock insuffisant pour ce produit "+produit.getLibelle());
                    }

                    updateProduit(produit,transactionDTO);

                } else {
                    throw new CustomParameterizedException("Echec lors de l\'opération produit introuvable");
                }
            } else {
                throw new CustomParameterizedException("Echec lors de l\'opération produit introuvable");
            }
            transaction.setQuantite(transactionDTO.getQuantite());
            transaction.setTypeTransaction(TypeTransaction.VENTE);
            transaction.setDate(Instant.now());
            transaction.setVente(vente);

            transactionRepository.save(transaction);
            transaction=new Transaction();
            // TODO: 06/11/2021 Mise à jour des quantités des produits
        }

    }

    public void doPaiementVente(Vente vente, SessionCaisse sessionCaisse,BigDecimal montantTotal) {
        Paiement paiement=new Paiement();
        paiement.setVente(vente);
        paiement.sessioncaisse(sessionCaisse);
        if(vente.getMontant().compareTo(montantTotal)==0){
            paiement.setMontant(vente.getMontant());
            sessionCaisse.setSommeFin(sessionCaisse.getSommeFin().add(montantTotal));

        } else {
            paiement.setMontant(vente.getAvance());
            sessionCaisse.setSommeFin(sessionCaisse.getSommeFin().add(vente.getAvance()));
        }
        paiement.setDate(Instant.now());
        paiementRepository.save(paiement);
        sessionCaisseRepository.save(sessionCaisse);
    }

    public BigDecimal getMontantTotalVente(List<TransactionDTO> listeTransaction){
        BigDecimal montantTotal= new BigDecimal(0);
        if(listeTransaction!=null){
            for(TransactionDTO transactionDTO: listeTransaction){
                BigDecimal prixProduit=transactionDTO.getPrixVenteReelUnitaire().multiply(new BigDecimal(transactionDTO.getQuantite()));
                montantTotal=montantTotal.add(prixProduit);
            }
        }
        return montantTotal;
    }

    public Long countVente(Instant startDate,Instant endDate){
        return venteRepository.countAll(startDate,endDate);
    }

    @Transactional
    public VenteDTO annulationVente(AnnulationDTO venteDTO) {
        log.debug("-----begin annulation----------------");
        Vente vente;
        List<Paiement> paiement=new ArrayList<>();
        List<Transaction> transactions=new ArrayList<>();
        Optional<SessionCaisseDTO> sessionCaisseDTO;
        Optional<User> user = userService.getUserWithAuthorities();
        sessionCaisseDTO = sessionCaisseService.findByUserAndStatut(user.get(), Statut.OUVERT);
        if(sessionCaisseDTO.isPresent()) {
            if (venteDTO.getId() != null) {
                vente = venteRepository.getOne(venteDTO.getId());
                if (vente.getId() != null) {
                    paiement = paiementRepository.findByVente(vente);
                    // TODO: 18/11/2021 Mise à jour des transactions de la vente
                    for(Transaction t:vente.getTransactions()){
                        t.setEtat(Etat.ANNULER);
                        transactionRepository.save(t);
                    }
                    // TODO: 18/11/2021 Mise à jour des paiements
                    resetAllPaiementOfVente(paiement,sessionCaisseDTO.get());
                   return doAnnulationVente(vente);
                } else
                {
                    throw new CustomParameterizedException("Vente introuvable");
                }
            }else{
                throw new CustomParameterizedException("Erreur!! Vente introuvable");
            }
        }else {
            throw new CustomParameterizedException("Session de caisse introuvable");
        }

    }

    private VenteDTO doAnnulationVente(Vente vente) {
        vente.setEtat(Etat.ANNULER);
        venteRepository.save(vente);
        return venteMapper.toDto(vente);
    }

    private void resetAllPaiementOfVente(List<Paiement> paiement,SessionCaisseDTO sessionCaisseDTO) {
        for(Paiement p: paiement){
            p.setEtat(Etat.ANNULER);
            paiementRepository.save(p);
            sessionCaisseDTO.setSommeFin(sessionCaisseDTO.getSommeFin().subtract(p.getMontant()));
            sessionCaisseRepository.save(sessionCaisseMapper.toEntity(sessionCaisseDTO));
        }
    }

    public void updateProduit(Produit produit,TransactionDTO transaction){
        if (produit.getPrixVenteCourant() != null) {
            if (produit.getPrixVenteCourant().compareTo(transaction.getPrixVenteReelUnitaire()) <= 0) {
                produit.setPrixVenteCourant(transaction.getPrixVenteReelUnitaire());
            }
        } else {
            produit.setPrixVenteCourant(transaction.getPrixVenteReelUnitaire());
        }
        produitRepository.save(produit);
    }
}
