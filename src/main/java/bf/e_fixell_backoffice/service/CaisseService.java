package bf.e_fixell_backoffice.service;

import bf.e_fixell_backoffice.domain.Caisse;
import bf.e_fixell_backoffice.domain.SessionCaisse;
import bf.e_fixell_backoffice.domain.enumeration.Statut;
import bf.e_fixell_backoffice.domain.enumeration.TypeCaisse;
import bf.e_fixell_backoffice.repository.CaisseRepository;
import bf.e_fixell_backoffice.repository.SessionCaisseRepository;
import bf.e_fixell_backoffice.service.dto.BrouillardCaisseDTO;
import bf.e_fixell_backoffice.service.dto.CaisseDTO;
import bf.e_fixell_backoffice.service.mapper.CaisseMapper;
import bf.e_fixell_backoffice.web.rest.errors.CustomParameterizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Caisse}.
 */
@Service
@Transactional
public class CaisseService {

    private final Logger log = LoggerFactory.getLogger(CaisseService.class);

    private final CaisseRepository caisseRepository;

    private final CaisseMapper caisseMapper;

    private final SessionCaisseRepository sessionCaisseRepository;

    public CaisseService(CaisseRepository caisseRepository, CaisseMapper caisseMapper, SessionCaisseRepository sessionCaisseRepository) {
        this.caisseRepository = caisseRepository;
        this.caisseMapper = caisseMapper;
        this.sessionCaisseRepository = sessionCaisseRepository;
    }

    /**
     * Save a caisse.
     *
     * @param caisseDTO the entity to save.
     * @return the persisted entity.
     */
    @Transactional
    public CaisseDTO save(CaisseDTO caisseDTO) {
        log.debug("Request to save Caisse : {}", caisseDTO);
        Caisse caisse = caisseMapper.toEntity(caisseDTO);

        if (caisseDTO.getId() != null) {
            if (caisseDTO.getLibelle() != null) {
                Caisse uniqueCaisse = caisseRepository.findByLibelleAndDeletedIsFalse(caisseDTO.getLibelle());
                if (uniqueCaisse != null && !caisseDTO.getId().equals(uniqueCaisse.getId())) {
                    if (!uniqueCaisse.getId().equals(caisseDTO.getId())) {
                        throw new CustomParameterizedException("Erreur Une caisse porte déja le meme libbelle");
                    }
                }
            }
            if (caisseDTO.getSommeMin().compareTo(caisseDTO.getSommeMax()) > 0) {
                throw new CustomParameterizedException("Solde minimum superieur au solde maximum");
            } else {
                caisse = caisseRepository.save(caisse);

            }

        } else {
            if (caisseDTO.getLibelle() != null) {
                Caisse uniqueCaisse = caisseRepository.findByLibelleAndDeletedIsFalse(caisseDTO.getLibelle());
                if (uniqueCaisse != null) {
                    throw new CustomParameterizedException("Erreur Une caisse porte déja le meme libbelle");
                }
            }
            caisse.setSomme(new BigDecimal(0));
            if (caisseDTO.getTypeCaisse().equals(TypeCaisse.CENTRALE)) {

                Caisse caisseExit = caisseRepository.findByTypeCaisseAndDeletedIsFalse(TypeCaisse.CENTRALE);
                if (caisseExit != null) {
                    throw new CustomParameterizedException("Une caisse centrale existe déja");
                } else {
                    caisse.setStatut(Statut.ACTIF);
                }
            }
            caisse = caisseRepository.save(caisse);
        }

        return caisseMapper.toDto(caisse);
    }

    /**
     * Get all the caisses.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<CaisseDTO> findAll(CaisseDTO caisseDTO, Pageable pageable) {
        log.debug("Request to get all Caisses--" + caisseDTO);
        return caisseRepository.findAllWithCriteria(caisseDTO.getCode(), caisseDTO.getLibelle(), caisseDTO.getTypeCaisse(), pageable)
            .map(caisseMapper::toDto);
    }

    public List<CaisseDTO> findListCaisse() {
        log.debug("Request to get all Caisses--");
        List<Caisse> list = new ArrayList<>();
        list = caisseRepository.findAll();

        return caisseMapper.toDto(list);
    }


    /**
     * Get one caisse by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public CaisseDTO findOne(Long id) {
        log.debug("Request to get Caisse : {}", id);
        Optional<Caisse> caisse = caisseRepository.findByIdAndDeletedIsFalse(id);
        CaisseDTO caisseDTO = caisseMapper.toDto(caisse.get());
        List<SessionCaisse> sessionCaisses = sessionCaisseRepository.findByCaisse(caisse.get());
        caisseDTO.setSessionCaisses(sessionCaisses);
        return caisseDTO;
    }

    /**
     * Delete the caisse by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Caisse : {}", id);
        Caisse caisse = caisseRepository.getOne(id);
        if (caisse.getId() != null) {
            caisse.setDeleted(true);
            caisseRepository.save(caisse);
        } else {
            throw new CustomParameterizedException("Caisse introuvable");
        }

    }

    public BrouillardCaisseDTO getBrouillardOfCaisse(BrouillardCaisseDTO brouillardCaisseDTO) {
        Caisse caisse = new Caisse();
        SessionCaisse sessionCaisse = new SessionCaisse();
        if (brouillardCaisseDTO.getCaisseId() != null) {

        }
        return null;
    }
}
