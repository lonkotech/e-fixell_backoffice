package bf.e_fixell_backoffice.service;

import bf.e_fixell_backoffice.domain.Paiement;
import bf.e_fixell_backoffice.domain.SessionCaisse;
import bf.e_fixell_backoffice.domain.User;
import bf.e_fixell_backoffice.domain.Vente;
import bf.e_fixell_backoffice.domain.enumeration.Statut;
import bf.e_fixell_backoffice.repository.PaiementRepository;
import bf.e_fixell_backoffice.repository.SessionCaisseRepository;
import bf.e_fixell_backoffice.repository.UserRepository;
import bf.e_fixell_backoffice.repository.VenteRepository;
import bf.e_fixell_backoffice.security.SecurityUtils;
import bf.e_fixell_backoffice.service.dto.PaiementDTO;
import bf.e_fixell_backoffice.service.dto.SessionCaisseDTO;
import bf.e_fixell_backoffice.service.dto.VenteDTO;
import bf.e_fixell_backoffice.service.mapper.PaiementMapper;
import bf.e_fixell_backoffice.web.rest.errors.CustomParameterizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Paiement}.
 */
@Service
@Transactional
public class PaiementService {

    private final Logger log = LoggerFactory.getLogger(PaiementService.class);

    private final PaiementRepository paiementRepository;

    private final VenteRepository venteRepository;

    private final UserRepository userRepository;

    private final SessionCaisseRepository sessionCaisseRepository;

    private final PaiementMapper paiementMapper;

    public PaiementService(PaiementRepository paiementRepository, VenteRepository venteRepository, UserRepository userRepository, SessionCaisseRepository sessionCaisseRepository, PaiementMapper paiementMapper) {
        this.paiementRepository = paiementRepository;
        this.venteRepository = venteRepository;
        this.userRepository = userRepository;
        this.sessionCaisseRepository = sessionCaisseRepository;
        this.paiementMapper = paiementMapper;
    }

    /**
     * Save a paiement.
     *
     * @param paiementDTO the entity to save.
     * @return the persisted entity.
     */
    public PaiementDTO save(PaiementDTO paiementDTO) {
        log.debug("Request to save Paiement : {}", paiementDTO);
        Paiement paiement = paiementMapper.toEntity(paiementDTO);
        paiement = paiementRepository.save(paiement);
        return paiementMapper.toDto(paiement);
    }

    /**
     * Get all the paiements.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<PaiementDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Paiements");
        return paiementRepository.findAll(pageable)
            .map(paiementMapper::toDto);
    }


    /**
     * Get one paiement by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<PaiementDTO> findOne(Long id) {
        log.debug("Request to get Paiement : {}", id);
        return paiementRepository.findById(id)
            .map(paiementMapper::toDto);
    }

    /**
     * Delete the paiement by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Paiement : {}", id);
        paiementRepository.deleteById(id);
    }

    @Transactional
    public PaiementDTO doPaiement(VenteDTO venteDTO) {
        String userLogin = SecurityUtils.getCurrentUserLogin().orElseThrow(() -> new CustomParameterizedException("Current user login not found"));
        Optional<User> user = Optional.empty();
        Paiement paiement = new Paiement();
        if (venteDTO.getId() != null) {
            Vente vente = venteRepository.getOne(venteDTO.getId());
            if (vente.getId() != null) {
                paiement.setDate(Instant.now());
                paiement.setVente(vente);
                paiement.setMontant(venteDTO.getMontant());
                if (userLogin != null) {
                    user = userRepository.findOneByLogin(userLogin);
                    if (user.isPresent()) {
                        Optional<SessionCaisse> sessionCaisse = sessionCaisseRepository.findByUserAndStatut(user.get(), Statut.ACTIF);
                        if (sessionCaisse.isPresent()) {
                            paiement.sessioncaisse(sessionCaisse.get());
                            paiementRepository.save(paiement);
                            BigDecimal montantTotal = paiementRepository.getMontantVente(vente.getId());
                            if (vente.getPrixReel().compareTo(montantTotal) == 0) {
                                vente.setSolder(true);
                                venteRepository.save(vente);
                            }
                        } else {
                            throw new CustomParameterizedException("Session innexistante");
                        }
                    } else {
                        throw new CustomParameterizedException(" Utilisateur introuvable");
                    }
                } else {
                    throw new CustomParameterizedException("Erreur lors de l\'opération ");

                }
            }
        }
        return paiementMapper.toDto(paiement);
    }

    public Page<PaiementDTO> findBySaissionCaisse(SessionCaisseDTO sessionCaisseDTO, Pageable pageable) {
        if (sessionCaisseDTO.getId() != null) {
            SessionCaisse sessionCaisse = sessionCaisseRepository.getOne(sessionCaisseDTO.getId());
            return paiementRepository.findBySessioncaisse(sessionCaisse, pageable).map(paiementMapper::toDto);
        } else {
            throw new CustomParameterizedException("Erreur interne session innexistante");
        }
    }

    @Transactional(readOnly = true)
    public Page<PaiementDTO> findAllForPersonnel(Pageable pageable) {
        log.debug("Request to get all Paiements");
        return paiementRepository.findByPersonnelIsNotNull(pageable)
            .map(paiementMapper::toDto);
    }

    @Transactional(readOnly = true)
    public List<PaiementDTO> findAllByPersonnelId(Long id) {
        log.debug("Request to get all Paiements");
        return paiementRepository.findByPersonnelId(id).stream().map(paiementMapper::toDto).collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public List<PaiementDTO> findByPersonnelIsNotNull() {

        return paiementRepository.findByPersonnelIsNotNull()
            .stream()
            .map(paiementMapper::toDto)
            .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public List<PaiementDTO> findAllByCommandeId(Long id) {
        log.debug("Request to get all Paiements");
        return paiementRepository.findByCommandeId(id).stream().map(paiementMapper::toDto).collect(Collectors.toList());
    }
}
