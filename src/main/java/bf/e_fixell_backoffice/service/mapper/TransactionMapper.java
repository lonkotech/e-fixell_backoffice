package bf.e_fixell_backoffice.service.mapper;


import bf.e_fixell_backoffice.domain.Transaction;
import bf.e_fixell_backoffice.service.dto.TransactionDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity {@link Transaction} and its DTO {@link TransactionDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProduitMapper.class, CommandeMapper.class,
    ApprovisionnementMapper.class,
    InventaireMapper.class,
    LivraisonMapper.class,
    LivraisonMapper.class,
    DeviseMapper.class,
    VenteMapper.class})
public interface TransactionMapper extends EntityMapper<TransactionDTO, Transaction> {

    @Mapping(source = "produit.id", target = "produitId")
    @Mapping(source = "commande.id", target = "commandeId")
    @Mapping(source = "livraison.id", target = "livraisonId")
    @Mapping(source = "approvisionnement.id", target = "approvisionnementId")
    @Mapping(source = "vente.id", target = "venteId")
    @Mapping(source = "inventaire.id", target = "inventaireId")
    @Mapping(source = "produit.libelle", target = "produitLibelle")
    TransactionDTO toDto(Transaction transaction);

    @Mapping(source = "produitId", target = "produit")
    @Mapping(source = "commandeId", target = "commande")
    @Mapping(source = "livraisonId", target = "livraison")
    @Mapping(source = "approvisionnementId", target = "approvisionnement")
    @Mapping(source = "venteId", target = "vente")
    @Mapping(source = "inventaireId", target = "inventaire")
    @Mapping(source = "deviseId", target = "devise")
    Transaction toEntity(TransactionDTO transactionDTO);

    default Transaction fromId(Long id) {
        if (id == null) {
            return null;
        }
        Transaction transaction = new Transaction();
        transaction.setId(id);
        return transaction;
    }
}
