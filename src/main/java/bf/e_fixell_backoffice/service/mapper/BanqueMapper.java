package bf.e_fixell_backoffice.service.mapper;


import bf.e_fixell_backoffice.domain.Banque;
import bf.e_fixell_backoffice.domain.Personnel;
import bf.e_fixell_backoffice.service.dto.BanqueDTO;
import bf.e_fixell_backoffice.service.dto.PersonnelDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity {@link Personnel} and its DTO {@link PersonnelDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface BanqueMapper extends EntityMapper<BanqueDTO, Banque> {

    @Mapping(target = "personnels", ignore = true)
    Banque toEntity(BanqueDTO banqueDTO);

    default Banque fromId(Long id) {
        if (id == null) {
            return null;
        }
        Banque banque = new Banque();
        banque.setId(id);
        return banque;
    }
}
