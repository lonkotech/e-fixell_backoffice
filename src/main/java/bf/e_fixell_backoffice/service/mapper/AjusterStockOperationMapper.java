package bf.e_fixell_backoffice.service.mapper;


import bf.e_fixell_backoffice.domain.AjusterStockOperation;
import bf.e_fixell_backoffice.service.dto.AjusterStockOperationDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity {@link AjusterStockOperation} and its DTO {@link AjusterStockOperationDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProduitMapper.class})
public interface AjusterStockOperationMapper extends EntityMapper<AjusterStockOperationDTO, AjusterStockOperation> {

    //    @Mapping(source = "ficheTechnique", target = "ficheTechnique")
    @Mapping(source = "produit.id", target = "produitId")
    @Mapping(source = "produit.libelle", target = "produitLibelle")
    AjusterStockOperationDTO toDto(AjusterStockOperation ajusterStockOperation);

    @Mapping(source = "produitId", target = "produit")
    AjusterStockOperation toEntity(AjusterStockOperationDTO ajusterStockOperationDTO);

    default AjusterStockOperation fromId(Long id) {
        if (id == null) {
            return null;
        }
        AjusterStockOperation ajusterStockOperation = new AjusterStockOperation();
        ajusterStockOperation.setId(id);
        return ajusterStockOperation;
    }
}
