package bf.e_fixell_backoffice.service.mapper;

import bf.e_fixell_backoffice.domain.EtapeTransport;
import bf.e_fixell_backoffice.service.dto.EtapeTransportDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity {@link EtapeTransport} and its DTO {@link EtapeTransportDTO}.
 */
@Mapper(componentModel = "spring", uses = {CommandeMapper.class, SocieteTransportMapper.class})
public interface EtapeTransportMapper extends EntityMapper<EtapeTransportDTO, EtapeTransport> {

    @Mapping(source = "commande.id", target = "commandeId")
    @Mapping(source = "societeTransport.id", target = "societeTransportId")
    EtapeTransportDTO toDto(EtapeTransport etapeTransport);

    @Mapping(source = "commandeId", target = "commande")
    @Mapping(source = "societeTransportId", target = "societeTransport")
    EtapeTransport toEntity(EtapeTransportDTO etapeTransportDTO);

    default EtapeTransport fromId(Long id) {
        if (id == null) {
            return null;
        }
        EtapeTransport etapeTransport = new EtapeTransport();
        etapeTransport.setId(id);
        return etapeTransport;
    }
}
