package bf.e_fixell_backoffice.service.mapper;


import bf.e_fixell_backoffice.domain.Approvisionnement;
import bf.e_fixell_backoffice.service.dto.ApprovisionnementDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity {@link Approvisionnement} and its DTO {@link ApprovisionnementDTO}.
 */
@Mapper(componentModel = "spring", uses = {FournisseurMapper.class, TransactionMapper.class, CommandeMapper.class})
public interface ApprovisionnementMapper extends EntityMapper<ApprovisionnementDTO, Approvisionnement> {

    @Mapping(source = "fournisseur.id", target = "fournisseurId")
    @Mapping(source = "commande.id", target = "commandeId")
    @Mapping(source = "fournisseur.nom", target = "fournisseurNom")
    @Mapping(source = "transactions", target = "transactions")
    ApprovisionnementDTO toDto(Approvisionnement approvisionnement);

    @Mapping(target = "transactions", ignore = true)
    @Mapping(target = "removeTransaction", ignore = true)
    @Mapping(source = "fournisseurId", target = "fournisseur")
    @Mapping(source = "commandeId", target = "commande")
    Approvisionnement toEntity(ApprovisionnementDTO approvisionnementDTO);

    default Approvisionnement fromId(Long id) {
        if (id == null) {
            return null;
        }
        Approvisionnement approvisionnement = new Approvisionnement();
        approvisionnement.setId(id);
        return approvisionnement;
    }
}
