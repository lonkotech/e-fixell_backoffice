package bf.e_fixell_backoffice.service.mapper;


import bf.e_fixell_backoffice.domain.Devise;
import bf.e_fixell_backoffice.domain.Profil;
import bf.e_fixell_backoffice.service.dto.DeviseDTO;
import bf.e_fixell_backoffice.service.dto.ProfilDTO;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity {@link Profil} and its DTO {@link ProfilDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface DeviseMapper extends EntityMapper<DeviseDTO, Devise> {


    default Devise fromId(Long id) {
        if (id == null) {
            return null;
        }
        Devise devise = new Devise();
        devise.setId(id);
        return devise;
    }
}
