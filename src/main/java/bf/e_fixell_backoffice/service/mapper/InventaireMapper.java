package bf.e_fixell_backoffice.service.mapper;


import bf.e_fixell_backoffice.domain.Inventaire;
import bf.e_fixell_backoffice.service.dto.InventaireDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity {@link Inventaire} and its DTO {@link InventaireDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProduitMapper.class})
public interface InventaireMapper extends EntityMapper<InventaireDTO, Inventaire> {

    @Mapping(source = "produit.id", target = "produitId")
    @Mapping(source = "produit.libelle", target = "produitLibelle")
    InventaireDTO toDto(Inventaire inventaire);

    @Mapping(source = "produitId", target = "produit")
    @Mapping(target = "pertes", ignore = true)
    @Mapping(target = "transactions", ignore = true)
    Inventaire toEntity(InventaireDTO inventaireDTO);

    default Inventaire fromId(Long id) {
        if (id == null) {
            return null;
        }
        Inventaire inventaire = new Inventaire();
        inventaire.setId(id);
        return inventaire;
    }
}
