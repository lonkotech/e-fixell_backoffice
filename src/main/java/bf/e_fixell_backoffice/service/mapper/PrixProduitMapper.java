package bf.e_fixell_backoffice.service.mapper;


import bf.e_fixell_backoffice.domain.PrixProduit;
import bf.e_fixell_backoffice.service.dto.PrixProduitDTO;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity {@link PrixProduit} and its DTO {@link PrixProduitDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProduitMapper.class})
public interface PrixProduitMapper extends EntityMapper<PrixProduitDTO, PrixProduit> {

    default PrixProduit fromId(Long id) {
        if (id == null) {
            return null;
        }
        PrixProduit prixProduit = new PrixProduit();
        prixProduit.setId(id);
        return prixProduit;
    }
}
