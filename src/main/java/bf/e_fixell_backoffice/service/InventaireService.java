package bf.e_fixell_backoffice.service;

import bf.e_fixell_backoffice.domain.Inventaire;
import bf.e_fixell_backoffice.repository.InventaireRepository;
import bf.e_fixell_backoffice.service.dto.InventaireDTO;
import bf.e_fixell_backoffice.service.mapper.InventaireMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Inventaire}.
 */
@Service
@Transactional
public class InventaireService {

    private final Logger log = LoggerFactory.getLogger(InventaireService.class);

    private final InventaireRepository inventaireRepository;

    private final InventaireMapper inventaireMapper;

    public InventaireService(InventaireRepository inventaireRepository, InventaireMapper inventaireMapper) {
        this.inventaireRepository = inventaireRepository;
        this.inventaireMapper = inventaireMapper;
    }

    /**
     * Save a inventaire.
     *
     * @param inventaireDTO the entity to save.
     * @return the persisted entity.
     */
    public InventaireDTO save(InventaireDTO inventaireDTO) {
        log.debug("Request to save Inventaire : {}", inventaireDTO);
        Inventaire inventaire = inventaireMapper.toEntity(inventaireDTO);
        inventaire = inventaireRepository.save(inventaire);
        return inventaireMapper.toDto(inventaire);
    }

    /**
     * Get all the inventaires.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<InventaireDTO> findAll() {
        log.debug("Request to get all Inventaires");
        return inventaireRepository.findAll().stream()
            .map(inventaireMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one inventaire by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<InventaireDTO> findOne(Long id) {
        log.debug("Request to get Inventaire : {}", id);
        return inventaireRepository.findById(id)
            .map(inventaireMapper::toDto);
    }

    /**
     * Delete the inventaire by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Inventaire : {}", id);
        inventaireRepository.deleteById(id);
    }

    public Page<InventaireDTO> findByCriteria(InventaireDTO inventaireDTO, Pageable pageable) {
        Page<InventaireDTO> page;
        List<InventaireDTO> list;
        Instant stard = null;
        Instant end = null;
        if (inventaireDTO.getStartDate() != null && inventaireDTO.getEndtDate() != null) {
            stard = inventaireDTO.getStartDate();
            end = inventaireDTO.getEndtDate();
        }
        if (inventaireDTO.getStartDate() != null) {
            stard = inventaireDTO.getStartDate();
            end = Instant.now();
        }
        if (inventaireDTO.getEndtDate() != null) {
            stard = inventaireDTO.getEndtDate();
            end = inventaireDTO.getEndtDate();
        }
        // list = inventaireRepository.findByCriteria(pageable,inventaireDTO.getProduitId()).getContent().stream().map(inventaireMapper::toDto).collect(Collectors.toList());
        list = inventaireRepository.findByCriteria(pageable, inventaireDTO.getProduitId(), stard, end).getContent().stream().map(inventaireMapper::toDto).collect(Collectors.toList());
        page = new PageImpl<>(list, pageable, list.size());
        return page;
    }
}
