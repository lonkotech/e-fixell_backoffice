package bf.e_fixell_backoffice.domain;

import bf.e_fixell_backoffice.domain.enumeration.Etat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;

/**
 * A Paiement.
 */
@Entity
@Table(name = "paiement")
public class Paiement implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "deleted", columnDefinition = "boolean default false")
    private boolean deleted;

    @Column(name = "code")
    private String code;

    @Column(name = "affecte_caissse", columnDefinition = "boolean default false")
    private boolean affecteCaisse = false;

    @Column(name = "date")
    private Instant date;

    @Column(name = "montant", precision = 21, scale = 2, columnDefinition = "numeric default 0")
    private BigDecimal montant;

    @Column(name = "motif")
    private String motif;

    @ManyToOne
    @JsonIgnoreProperties(value = "paiements", allowSetters = true)
    private Commande commande;

    @ManyToOne
    @JsonIgnoreProperties(value = "paiements", allowSetters = true)
    private Vente vente;

    @ManyToOne
    @JsonIgnoreProperties(value = "paiements", allowSetters = true)
    private SessionCaisse sessioncaisse;

    @Enumerated(EnumType.STRING)
    @Column(name = "etat")
    private Etat etat;

    @ManyToOne
    @JsonIgnoreProperties(value = "paiements", allowSetters = true)
    private Personnel personnel;

    @Column(name="numero_compte")
    private String numeroCompte;

    @Column(name="banque_nom")
    private String banque;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Paiement code(String code) {
        this.code = code;
        return this;
    }

    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public Paiement date(Instant date) {
        this.date = date;
        return this;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public Paiement montant(BigDecimal montant) {
        this.montant = montant;
        return this;
    }

    public Commande getCommande() {
        return commande;
    }

    public void setCommande(Commande commande) {
        this.commande = commande;
    }

    public Paiement commande(Commande commande) {
        this.commande = commande;
        return this;
    }

    public Vente getVente() {
        return vente;
    }

    public void setVente(Vente vente) {
        this.vente = vente;
    }

    public Paiement vente(Vente vente) {
        this.vente = vente;
        return this;
    }

    public SessionCaisse getSessioncaisse() {
        return sessioncaisse;
    }

    public void setSessioncaisse(SessionCaisse sessionCaisse) {
        this.sessioncaisse = sessionCaisse;
    }

    public Paiement sessioncaisse(SessionCaisse sessionCaisse) {
        this.sessioncaisse = sessionCaisse;
        return this;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    public boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Etat getEtat() {
        return etat;
    }

    public void setEtat(Etat etat) {
        this.etat = etat;
    }

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    public boolean isAffecteCaisse() {
        return affecteCaisse;
    }

    public void setAffecteCaisse(boolean affecteCaisse) {
        this.affecteCaisse = affecteCaisse;
    }

    public Personnel getPersonnel() {
        return personnel;
    }

    public void setPersonnel(Personnel personnel) {
        this.personnel = personnel;
    }

    public String getNumeroCompte() {
        return numeroCompte;
    }

    public void setNumeroCompte(String numeroCompte) {
        this.numeroCompte = numeroCompte;
    }

    public String getBanque() {
        return banque;
    }

    public void setBanque(String banque) {
        this.banque = banque;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Paiement)) {
            return false;
        }
        return id != null && id.equals(((Paiement) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Paiement{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", date='" + getDate() + "'" +
            ", montant=" + getMontant() +
            "}";
    }
}
