package bf.e_fixell_backoffice.domain.enumeration;

/**
 * The TypeCommande enumeration.
 */
public enum TypeCommande {
    COMMANDE_CLIENT, COMMANDE_FOURNISSEUR
}
