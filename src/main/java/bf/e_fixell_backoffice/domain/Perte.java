package bf.e_fixell_backoffice.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;

/**
 * A Perte.
 */
@Entity
@Table(name = "perte")
public class Perte implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "deleted", columnDefinition = "boolean default false")
    private boolean deleted;

    @Column(name = "libelle")
    private String libelle;

    @Column(name = "date")
    private Instant date;

    @Column(name = "quantite", columnDefinition = "Integer default 0")
    private Integer quantite;

    @Column(name = "montant", precision = 21, scale = 2, columnDefinition = "numeric default 0")
    private BigDecimal montant;

    @ManyToOne
    @JsonIgnoreProperties(value = "pertes", allowSetters = true)
    private Produit produit;

    @ManyToOne
    @JsonIgnoreProperties(value = "pertes", allowSetters = true)
    private Inventaire inventaire;

    // jhipster-needle-entity-add-field - JHipster will add fields here


    public Inventaire getInventaire() {
        return inventaire;
    }

    public void setInventaire(Inventaire inventaire) {
        this.inventaire = inventaire;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Perte libelle(String libelle) {
        this.libelle = libelle;
        return this;
    }

    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public Perte date(Instant date) {
        this.date = date;
        return this;
    }

    public Integer getQuantite() {
        return quantite;
    }

    public void setQuantite(Integer quantite) {
        this.quantite = quantite;
    }

    public Perte quantite(Integer quantite) {
        this.quantite = quantite;
        return this;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public Perte montant(BigDecimal montant) {
        this.montant = montant;
        return this;
    }

    public Produit getProduit() {
        return produit;
    }

    public void setProduit(Produit produit) {
        this.produit = produit;
    }

    public Perte produit(Produit produit) {
        this.produit = produit;
        return this;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    public boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Perte)) {
            return false;
        }
        return id != null && id.equals(((Perte) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Perte{" +
            "id=" + getId() +
            ", libelle='" + getLibelle() + "'" +
            ", date='" + getDate() + "'" +
            ", quantite=" + getQuantite() +
            ", montant=" + getMontant() +
            "}";
    }
}
