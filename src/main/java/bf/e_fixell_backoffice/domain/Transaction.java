//package bf.e_fixell_backoffice.domain;
//
//import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
//
//import javax.persistence.*;
//
//import java.io.Serializable;
//import java.math.BigDecimal;
//import java.time.Instant;
//
//import bf.e_fixell_backoffice.domain.enumeration.TypeTransaction;
//
//import bf.e_fixell_backoffice.domain.enumeration.Etat;
//
///**
// * A Transaction.
// */
//@Entity
//@Table(name = "transaction")
//public class Transaction implements Serializable {
//
//    private static final long serialVersionUID = 1L;
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
//    @SequenceGenerator(name = "sequenceGenerator")
//    private Long id;
//
//    @Column(name = "code")
//    private String code;
//
//    @Column(name = "date")
//    private Instant date;
//
//    @Column(name = "quantite")
//    private Integer quantite;
//
//    @Column(name = "prix_unitaire", precision = 21, scale = 2)
//    private BigDecimal prixUnitaire;
//
//    @Enumerated(EnumType.STRING)
//    @Column(name = "type_transaction")
//    private TypeTransaction typeTransaction;
//
//    @Enumerated(EnumType.STRING)
//    @Column(name = "etat")
//    private Etat etat;
//
//    @Column(name = "motif")
//    private String motif;
//
//    @Column(name = "valeur_devise")
//    private Double valeurDevise;
//
//    @OneToOne
//    @JoinColumn(unique = true)
//    private FicheTechnique ficheTechnique;
//
//    @ManyToOne
//    @JsonIgnoreProperties(value = "transactions", allowSetters = true)
//    private Produit produit;
//
//    @ManyToOne
//    @JsonIgnoreProperties(value = "transactions", allowSetters = true)
//    private Inventaire inventaire;
//
//    @ManyToOne
//    @JsonIgnoreProperties(value = "transactions", allowSetters = true)
//    private Commande commande;
//
//    @ManyToOne
//    @JsonIgnoreProperties(value = "transactions", allowSetters = true)
//    private Approvisionnement approvisionnement;
//
//    @ManyToOne
//    @JsonIgnoreProperties(value = "transactions", allowSetters = true)
//    private Livraison livraison;
//
//    @ManyToOne
//    @JsonIgnoreProperties(value = "transactions", allowSetters = true)
//    private Vente vente;
//
//    @ManyToOne
//    @JsonIgnoreProperties(value = "transactions", allowSetters = true)
//    private Devise devise;
//
//    // jhipster-needle-entity-add-field - JHipster will add fields here
//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }
//
//    public String getCode() {
//        return code;
//    }
//
//    public Transaction code(String code) {
//        this.code = code;
//        return this;
//    }
//
//    public void setCode(String code) {
//        this.code = code;
//    }
//
//    public Instant getDate() {
//        return date;
//    }
//
//    public Transaction date(Instant date) {
//        this.date = date;
//        return this;
//    }
//
//    public void setDate(Instant date) {
//        this.date = date;
//    }
//
//    public Integer getQuantite() {
//        return quantite;
//    }
//
//    public Transaction quantite(Integer quantite) {
//        this.quantite = quantite;
//        return this;
//    }
//
//    public void setQuantite(Integer quantite) {
//        this.quantite = quantite;
//    }
//
//    public BigDecimal getPrixUnitaire() {
//        return prixUnitaire;
//    }
//
//    public Transaction prixUnitaire(BigDecimal prixUnitaire) {
//        this.prixUnitaire = prixUnitaire;
//        return this;
//    }
//
//    public void setPrixUnitaire(BigDecimal prixUnitaire) {
//        this.prixUnitaire = prixUnitaire;
//    }
//
//    public TypeTransaction getTypeTransaction() {
//        return typeTransaction;
//    }
//
//    public Transaction typeTransaction(TypeTransaction typeTransaction) {
//        this.typeTransaction = typeTransaction;
//        return this;
//    }
//
//    public void setTypeTransaction(TypeTransaction typeTransaction) {
//        this.typeTransaction = typeTransaction;
//    }
//
//    public Etat getEtat() {
//        return etat;
//    }
//
//    public Transaction etat(Etat etat) {
//        this.etat = etat;
//        return this;
//    }
//
//    public void setEtat(Etat etat) {
//        this.etat = etat;
//    }
//
//    public String getMotif() {
//        return motif;
//    }
//
//    public Transaction motif(String motif) {
//        this.motif = motif;
//        return this;
//    }
//
//    public void setMotif(String motif) {
//        this.motif = motif;
//    }
//
//    public Double getValeurDevise() {
//        return valeurDevise;
//    }
//
//    public Transaction valeurDevise(Double valeurDevise) {
//        this.valeurDevise = valeurDevise;
//        return this;
//    }
//
//    public void setValeurDevise(Double valeurDevise) {
//        this.valeurDevise = valeurDevise;
//    }
//
//    public FicheTechnique getFicheTechnique() {
//        return ficheTechnique;
//    }
//
//    public Transaction ficheTechnique(FicheTechnique ficheTechnique) {
//        this.ficheTechnique = ficheTechnique;
//        return this;
//    }
//
//    public void setFicheTechnique(FicheTechnique ficheTechnique) {
//        this.ficheTechnique = ficheTechnique;
//    }
//
//    public Produit getProduit() {
//        return produit;
//    }
//
//    public Transaction produit(Produit produit) {
//        this.produit = produit;
//        return this;
//    }
//
//    public void setProduit(Produit produit) {
//        this.produit = produit;
//    }
//
//    public Commande getCommande() {
//        return commande;
//    }
//
//    public Transaction commande(Commande commande) {
//        this.commande = commande;
//        return this;
//    }
//
//    public void setCommande(Commande commande) {
//        this.commande = commande;
//    }
//
//    public Approvisionnement getApprovisionnement() {
//        return approvisionnement;
//    }
//
//    public Transaction approvisionnement(Approvisionnement approvisionnement) {
//        this.approvisionnement = approvisionnement;
//        return this;
//    }
//
//    public void setApprovisionnement(Approvisionnement approvisionnement) {
//        this.approvisionnement = approvisionnement;
//    }
//
//    public Livraison getLivraison() {
//        return livraison;
//    }
//
//    public Transaction livraison(Livraison livraison) {
//        this.livraison = livraison;
//        return this;
//    }
//
//    public void setLivraison(Livraison livraison) {
//        this.livraison = livraison;
//    }
//
//    public Vente getVente() {
//        return vente;
//    }
//
//    public Transaction vente(Vente vente) {
//        this.vente = vente;
//        return this;
//    }
//
//    public void setVente(Vente vente) {
//        this.vente = vente;
//    }
//
//    public Devise getDevise() {
//        return devise;
//    }
//
//    public Transaction devise(Devise devise) {
//        this.devise = devise;
//        return this;
//    }
//
//    public void setDevise(Devise devise) {
//        this.devise = devise;
//    }
//    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) {
//            return true;
//        }
//        if (!(o instanceof Transaction)) {
//            return false;
//        }
//        return id != null && id.equals(((Transaction) o).id);
//    }
//
//    @Override
//    public int hashCode() {
//        return 31;
//    }
//
//    // prettier-ignore
//    @Override
//    public String toString() {
//        return "Transaction{" +
//            "id=" + getId() +
//            ", code='" + getCode() + "'" +
//            ", date='" + getDate() + "'" +
//            ", quantite=" + getQuantite() +
//            ", prixUnitaire=" + getPrixUnitaire() +
//            ", typeTransaction='" + getTypeTransaction() + "'" +
//            ", etat='" + getEtat() + "'" +
//            ", motif='" + getMotif() + "'" +
//            ", valeurDevise=" + getValeurDevise() +
//            "}";
//    }
//}

package bf.e_fixell_backoffice.domain;

import bf.e_fixell_backoffice.domain.enumeration.Statut;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;

import bf.e_fixell_backoffice.domain.enumeration.TypeTransaction;

import bf.e_fixell_backoffice.domain.enumeration.Etat;
import io.swagger.annotations.ApiModelProperty;

/**
 * A Transaction.
 */
@Entity
@Table(name = "transaction")
public class Transaction implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "deleted", columnDefinition = "boolean default false")
    private boolean deleted;

    @Column(name = "code")
    private String code;

    @Column(name = "caracteristiques")
    private String caracteristiques;

    @Column(name = "date")
    private Instant date;

    @Column(name = "date_livre")
    private Instant dateLivre;

    @Column(name = "quantite",columnDefinition = "Integer default 0")
    private Integer quantite;

    @Column(name = "quantite_a_livre",columnDefinition = "Integer default 0")
    private Integer quantiteALivre;

    @Column(name = "quantite_totale_livre",columnDefinition = "Integer default 0")
    private Integer quantiteTotaleLivre;

    @Column(name = "decompte",columnDefinition = "Integer default 0")
    private Integer decompte;

    @Column(name = "prix_unitaire", precision = 21, scale = 2, columnDefinition = "numeric default 0")
    private BigDecimal prixUnitaire;

    @ApiModelProperty(name = "le prix d'achat unitaire")
    @Column(name = "prix_achat_unitaire", precision = 21, scale = 2, columnDefinition = "numeric default 0")
    private BigDecimal prixAchatUnitaire;

    @ApiModelProperty(name = "le prix de vente unitaire")
    @Column(name = "prix_vente_unitaire", precision = 21, scale = 2, columnDefinition = "numeric default 0")
    private BigDecimal prixVenteUnitaire;

    @ApiModelProperty(name = "le prix de vente reel unitaire")
    @Column(name = "prix_vente_reel_unitaire", precision = 21, scale = 2, columnDefinition = "numeric default 0")
    private BigDecimal prixVenteReelUnitaire;


    @Enumerated(EnumType.STRING)
    @Column(name = "type_transaction")
    private TypeTransaction typeTransaction;

    @Enumerated(EnumType.STRING)
    @Column(name = "etat")
    private Etat etat;

    @Column(name = "motif")
    private String motif;

    @Column(name = "valeur_devise")
    private Double valeurDevise;

    @Column(name = "transaction_parent_id")
    private Long transactionParentId;
//    @OneToOne
//    @JoinColumn(unique = true)
//    private FicheTechnique ficheTechnique;

//    @OneToOne
//    @JoinColumn(unique = true)
//    private PrixProduit prixProduit;

    @ManyToOne
    @JsonIgnoreProperties(value = "transactions", allowSetters = true)
    private Produit produit;

    @ManyToOne
    @JsonIgnoreProperties(value = "transactions", allowSetters = true)
    private Inventaire inventaire;

    @ManyToOne
    @JsonIgnoreProperties(value = "transactions", allowSetters = true)
    private Commande commande;

    @ManyToOne
    @JsonIgnoreProperties(value = "transactions", allowSetters = true)
    private Approvisionnement approvisionnement;

    @ManyToOne
    @JsonIgnoreProperties(value = "transactions", allowSetters = true)
    private Livraison livraison;

    @ManyToOne
    @JsonIgnoreProperties(value = "transactions", allowSetters = true)
    private Vente vente;

    @ManyToOne
    @JsonIgnoreProperties(value = "transactions", allowSetters = true)
    private Devise devise;

    // jhipster-needle-entity-add-field - JHipster will add fields here


    public Double getValeurDevise() {
        return valeurDevise;
    }

    public void setValeurDevise(Double valeurDevise) {
        this.valeurDevise = valeurDevise;
    }

    public Devise getDevise() {
        return devise;
    }

    public void setDevise(Devise devise) {
        this.devise = devise;
    }

    public Inventaire getInventaire() {
        return inventaire;
    }

    public void setInventaire(Inventaire inventaire) {
        this.inventaire = inventaire;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Transaction code(String code) {
        this.code = code;
        return this;
    }

    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public Transaction date(Instant date) {
        this.date = date;
        return this;
    }

    public Integer getQuantite() {
        return quantite;
    }

    public void setQuantite(Integer quantite) {
        this.quantite = quantite;
    }

    public Transaction quantite(Integer quantite) {
        this.quantite = quantite;
        return this;
    }

    public BigDecimal getPrixUnitaire() {
        return prixUnitaire;
    }

    public void setPrixUnitaire(BigDecimal prixUnitaire) {
        this.prixUnitaire = prixUnitaire;
    }

    public Transaction prixUnitaire(BigDecimal prixUnitaire) {
        this.prixUnitaire = prixUnitaire;
        return this;
    }

    public TypeTransaction getTypeTransaction() {
        return typeTransaction;
    }

    public void setTypeTransaction(TypeTransaction typeTransaction) {
        this.typeTransaction = typeTransaction;
    }

    public Transaction typeTransaction(TypeTransaction typeTransaction) {
        this.typeTransaction = typeTransaction;
        return this;
    }

    public String getCaracteristiques() {
        return caracteristiques;
    }

    public void setCaracteristiques(String caracteristiques) {
        this.caracteristiques = caracteristiques;
    }

    public Etat getEtat() {
        return etat;
    }

    public void setEtat(Etat etat) {
        this.etat = etat;
    }

    public Transaction etat(Etat etat) {
        this.etat = etat;
        return this;
    }

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    public Transaction motif(String motif) {
        this.motif = motif;
        return this;
    }

//    public FicheTechnique getFicheTechnique() {
//        return ficheTechnique;
//    }
//
//    public void setFicheTechnique(FicheTechnique ficheTechnique) {
//        this.ficheTechnique = ficheTechnique;
//    }
//
//    public Transaction ficheTechnique(FicheTechnique ficheTechnique) {
//        this.ficheTechnique = ficheTechnique;
//        return this;
//    }

    public Produit getProduit() {
        return produit;
    }

    public void setProduit(Produit produit) {
        this.produit = produit;
    }

    public Transaction produit(Produit produit) {
        this.produit = produit;
        return this;
    }

    public Commande getCommande() {
        return commande;
    }

    public void setCommande(Commande commande) {
        this.commande = commande;
    }

    public Transaction commande(Commande commande) {
        this.commande = commande;
        return this;
    }

    public Integer getQuantiteALivre() {
        return quantiteALivre;
    }

    public void setQuantiteALivre(Integer quantiteALivre) {
        this.quantiteALivre = quantiteALivre;
    }

    public Integer getQuantiteTotaleLivre() {
        return quantiteTotaleLivre;
    }

    public void setQuantiteTotaleLivre(Integer quantiteTotaleLivre) {
        this.quantiteTotaleLivre = quantiteTotaleLivre;
    }

    public Approvisionnement getApprovisionnement() {
        return approvisionnement;
    }

    public void setApprovisionnement(Approvisionnement approvisionnement) {
        this.approvisionnement = approvisionnement;
    }

    public Transaction approvisionnement(Approvisionnement approvisionnement) {
        this.approvisionnement = approvisionnement;
        return this;
    }

    public Livraison getLivraison() {
        return livraison;
    }

    public void setLivraison(Livraison livraison) {
        this.livraison = livraison;
    }

    public Transaction livraison(Livraison livraison) {
        this.livraison = livraison;
        return this;
    }

    public Vente getVente() {
        return vente;
    }

    public void setVente(Vente vente) {
        this.vente = vente;
    }

    public Transaction vente(Vente vente) {
        this.vente = vente;
        return this;
    }

//    public PrixProduit getPrixProduit() {
//        return prixProduit;
//    }
//
//    public void setPrixProduit(PrixProduit prixProduit) {
//        this.prixProduit = prixProduit;
//    }

    public Integer getDecompte() {
        return decompte;
    }

    public void setDecompte(Integer decompte) {
        this.decompte = decompte;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    public boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Instant getDateLivre() {
        return dateLivre;
    }

    public void setDateLivre(Instant dateLivre) {
        this.dateLivre = dateLivre;
    }

    public BigDecimal getPrixAchatUnitaire() {
        return prixAchatUnitaire;
    }

    public void setPrixAchatUnitaire(BigDecimal prixAchatUnitaire) {
        this.prixAchatUnitaire = prixAchatUnitaire;
    }

    public BigDecimal getPrixVenteUnitaire() {
        return prixVenteUnitaire;
    }

    public void setPrixVenteUnitaire(BigDecimal prixVenteUnitaire) {
        this.prixVenteUnitaire = prixVenteUnitaire;
    }

    public BigDecimal getPrixVenteReelUnitaire() {
        return prixVenteReelUnitaire;
    }

    public void setPrixVenteReelUnitaire(BigDecimal prixVenteReelUnitaire) {
        this.prixVenteReelUnitaire = prixVenteReelUnitaire;
    }

    public Long getTransactionParentId() {
        return transactionParentId;
    }

    public void setTransactionParentId(Long transactionParentId) {
        this.transactionParentId = transactionParentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Transaction)) {
            return false;
        }
        return id != null && id.equals(((Transaction) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore

    /*@Override
    public String toString() {
        return "Transaction{" +
            "id=" + id +
            ", code='" + code + '\'' +
            ", date=" + date +
            ", quantite=" + quantite +
            ", quantiteLivre=" + quantiteLivre +
            ", prixUnitaire=" + prixUnitaire +
            ", typeTransaction=" + typeTransaction +
            ", caracteristiques=" + caracteristiques +
            ", etat=" + etat +
            ", motif='" + motif + '\'' +
//            ", ficheTechnique=" + ficheTechnique +
//            ", prixProduit=" + prixProduit +
            ", produit=" + produit +
            ", commande=" + commande +
            ", approvisionnement=" + approvisionnement +
            ", livraison=" + livraison +
            ", vente=" + vente +
            '}';
    }*/
}

