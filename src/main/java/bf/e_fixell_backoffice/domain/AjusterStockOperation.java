package bf.e_fixell_backoffice.domain;

import javax.persistence.*;
import java.io.Serializable;

public class AjusterStockOperation extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "stock_theorique", columnDefinition = "Integer default 0")
    private Integer stockTheorique;

    @Column(name = "stock_physique", columnDefinition = "Integer default 0")
    private Integer stockPhysique;

    @OneToOne
    @JoinColumn(unique = true)
    private Produit produit;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getStockTheorique() {
        return stockTheorique;
    }

    public void setStockTheorique(Integer stockTheorique) {
        this.stockTheorique = stockTheorique;
    }

    public Integer getStockPhysique() {
        return stockPhysique;
    }

    public void setStockPhysique(Integer stockPhysique) {
        this.stockPhysique = stockPhysique;
    }

    public Produit getProduit() {
        return produit;
    }

    public void setProduit(Produit produit) {
        this.produit = produit;
    }

    @Override
    public String toString() {
        return "AjusterStockOperation{" +
            "id=" + id +
            ", stockTheorique=" + stockTheorique +
            ", stockPhysique=" + stockPhysique +
            ", produit=" + produit +
            '}';
    }
}
