package bf.e_fixell_backoffice.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.math.BigDecimal;

/**
 * A Devise.
 */
@Entity
@Table(name = "devise")
public class Devise implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "libelle")
    private String libelle;

    @Column(name = "description")
    private String description;

    @Column(name = "devise_base")
    private String deviseBase;

    @Column(name = "valeur", precision = 21, scale = 2)
    private BigDecimal valeur;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public Devise libelle(String libelle) {
        this.libelle = libelle;
        return this;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getDescription() {
        return description;
    }

    public Devise description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getValeur() {
        return valeur;
    }

    public Devise valeur(BigDecimal valeur) {
        this.valeur = valeur;
        return this;
    }

    public void setValeur(BigDecimal valeur) {
        this.valeur = valeur;
    }

    public String getDeviseBase() {
        return deviseBase;
    }

    public void setDeviseBase(String deviseBase) {
        this.deviseBase = deviseBase;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Devise)) {
            return false;
        }
        return id != null && id.equals(((Devise) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Devise{" +
            "id=" + id +
            ", libelle='" + libelle + '\'' +
            ", description='" + description + '\'' +
            ", deviseBase='" + deviseBase + '\'' +
            ", valeur=" + valeur +
            '}';
    }
}
