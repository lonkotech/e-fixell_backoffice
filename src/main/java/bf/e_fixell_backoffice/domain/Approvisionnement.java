package bf.e_fixell_backoffice.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * A Approvisionnement.
 */
@Entity
@Table(name = "approvisionnement")
public class Approvisionnement extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "code")
    private String code;

    @Column(name = "libelle")
    private String libelle;

    @Column(name = "date")
    private Instant date;

    @Column(name = "montant", precision = 21, scale = 2, columnDefinition = "numeric default 0")
    private BigDecimal montant;

    @Column(name = "deleted", columnDefinition = "boolean default false")
    private boolean deleted;

    @OneToMany(mappedBy = "approvisionnement")
    private Set<Transaction> transactions = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = "approvisionnements", allowSetters = true)
    private Fournisseur fournisseur;

    @OneToOne
    @JoinColumn(unique = true)
    private Commande commande;


    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Approvisionnement code(String code) {
        this.code = code;
        return this;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Approvisionnement libelle(String libelle) {
        this.libelle = libelle;
        return this;
    }

    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public Approvisionnement date(Instant date) {
        this.date = date;
        return this;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public Approvisionnement montant(BigDecimal montant) {
        this.montant = montant;
        return this;
    }

    public Set<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(Set<Transaction> transactions) {
        this.transactions = transactions;
    }

    public Approvisionnement transactions(Set<Transaction> transactions) {
        this.transactions = transactions;
        return this;
    }

    public Approvisionnement addTransaction(Transaction transaction) {
        this.transactions.add(transaction);
        transaction.setApprovisionnement(this);
        return this;
    }

    public Approvisionnement removeTransaction(Transaction transaction) {
        this.transactions.remove(transaction);
        transaction.setApprovisionnement(null);
        return this;
    }

    public Fournisseur getFournisseur() {
        return fournisseur;
    }

    public void setFournisseur(Fournisseur fournisseur) {
        this.fournisseur = fournisseur;
    }

    public Approvisionnement fournisseur(Fournisseur fournisseur) {
        this.fournisseur = fournisseur;
        return this;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    public boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Commande getCommande() {
        return commande;
    }

    public void setCommande(Commande commande) {
        this.commande = commande;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Approvisionnement)) {
            return false;
        }
        return id != null && id.equals(((Approvisionnement) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Approvisionnement{" +
            "id=" + id +
            ", code='" + code + '\'' +
            ", libelle='" + libelle + '\'' +
            ", date=" + date +
            ", montant=" + montant +
            ", commande =" + commande +
            ", deleted=" + deleted +
            ", transactions=" + transactions +
            ", fournisseur=" + fournisseur +
            '}';
    }
}
