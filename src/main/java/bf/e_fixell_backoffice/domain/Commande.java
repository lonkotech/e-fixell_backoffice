package bf.e_fixell_backoffice.domain;

import bf.e_fixell_backoffice.domain.enumeration.Statut;
import bf.e_fixell_backoffice.domain.enumeration.TypeCommande;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

import bf.e_fixell_backoffice.domain.enumeration.Etat;

/**
 * A Commande.
 */
@Entity
@Table(name = "commande")
public class Commande implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "deleted", columnDefinition = "boolean default false")
    private boolean deleted;

    @Column(name = "code")
    private String code;

    @Column(name = "libelle")
    private String libelle;

    @Column(name = "date")
    private Instant date;

    @Column(name = "somme", precision = 21, scale = 2, columnDefinition = "numeric default 0")
    private BigDecimal somme;

    @Column(name = "date_livraison_prevu")
    private Instant dateLivraisonPrevu;

    @Column(name = "date_livraison")
    private Instant dateLivraison;

    @Enumerated(EnumType.STRING)
    @Column(name = "etat")
    private Etat etat;

    @Enumerated(EnumType.STRING)
    @Column(name = "type_commande")
    private TypeCommande typeCommande;

    @Enumerated(EnumType.STRING)
    @Column(name = "statut")
    private Statut statut;

    @Column(name = "motif")
    private String motif;

    @Column(name = "avance", precision = 21, scale = 2, columnDefinition = "numeric default 0")
    private BigDecimal avance;

    @Column(name = "tva", precision = 21, scale = 2, columnDefinition = "numeric default 0")
    private BigDecimal tva;

    @Column(name = "tva_en_percent")
    private Boolean tvaEnPercent;

    @Column(name = "avance_en_percent")
    private Boolean avanceEnPercent;


    @Column(name = "est_solde", columnDefinition = "boolean default false")
    private Boolean estSolde;


    @Column(name = "montant_solde",columnDefinition = "numeric default 0")
    private BigDecimal montantSolde;

    @OneToMany(mappedBy = "commande")
    private Set<Transaction> transactions = new HashSet<>();

//    @OneToMany(mappedBy = "commande")
//    private Set<Livraison> livraisons = new HashSet<>();

    @OneToMany(mappedBy = "commande")
    private Set<Paiement> paiements = new HashSet<>();

    @OneToMany(mappedBy = "commande")
    private Set<Frais> frais = new HashSet<>();

    @OneToMany(mappedBy = "commande")
    private Set<EtapeTransport> etapeTransports = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = "commandes", allowSetters = true)
    private Fournisseur fournisseur;

    @ManyToOne
    @JsonIgnoreProperties(value = "commandes", allowSetters = true)
    private Client client;

    @Column(name = "commande_fournisseur_id")
    private Long commandeFournisseurId;

    // jhipster-needle-entity-add-field - JHipster will add fields here


    public BigDecimal getMontantSolde() {
        return montantSolde;
    }

    public void setMontantSolde(BigDecimal montantSolde) {
        this.montantSolde = montantSolde;
    }

    public Boolean getEstSolde() {
        return estSolde;
    }

    public void setEstSolde(Boolean estSolde) {
        this.estSolde = estSolde;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Commande code(String code) {
        this.code = code;
        return this;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Commande libelle(String libelle) {
        this.libelle = libelle;
        return this;
    }

    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public Commande date(Instant date) {
        this.date = date;
        return this;
    }

    public BigDecimal getSomme() {
        return somme;
    }

    public void setSomme(BigDecimal somme) {
        this.somme = somme;
    }

    public Commande somme(BigDecimal somme) {
        this.somme = somme;
        return this;
    }

    public Instant getDateLivraisonPrevu() {
        return dateLivraisonPrevu;
    }

    public void setDateLivraisonPrevu(Instant dateLivraisonPrevu) {
        this.dateLivraisonPrevu = dateLivraisonPrevu;
    }

    public Commande dateLivraisonPrevu(Instant dateLivraisonPrevu) {
        this.dateLivraisonPrevu = dateLivraisonPrevu;
        return this;
    }

    public Etat getEtat() {
        return etat;
    }

    public void setEtat(Etat etat) {
        this.etat = etat;
    }

    public Commande etat(Etat etat) {
        this.etat = etat;
        return this;
    }

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    public Commande motif(String motif) {
        this.motif = motif;
        return this;
    }

    public BigDecimal getAvance() {
        return avance;
    }

    public void setAvance(BigDecimal avance) {
        this.avance = avance;
    }

    public Commande avance(BigDecimal avance) {
        this.avance = avance;
        return this;
    }

    public Boolean isAvanceEnPercent() {
        return avanceEnPercent;
    }

    public Commande avanceEnPercent(Boolean avanceEnPercent) {
        this.avanceEnPercent = avanceEnPercent;
        return this;
    }

    public Set<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(Set<Transaction> transactions) {
        this.transactions = transactions;
    }

    public Commande transactions(Set<Transaction> transactions) {
        this.transactions = transactions;
        return this;
    }

    public Commande addTransaction(Transaction transaction) {
        this.transactions.add(transaction);
        transaction.setCommande(this);
        return this;
    }

    public Commande removeTransaction(Transaction transaction) {
        this.transactions.remove(transaction);
        transaction.setCommande(null);
        return this;
    }

    public Set<Paiement> getPaiements() {
        return paiements;
    }

//    public Set<Livraison> getLivraisons() {
//        return livraisons;
//    }
//
//    public void setLivraisons(Set<Livraison> livraisons) {
//        this.livraisons = livraisons;
//    }
//
//    public Commande livraisons(Set<Livraison> livraisons) {
//        this.livraisons = livraisons;
//        return this;
//    }
//
//    public Commande addLivraison(Livraison livraison) {
//        this.livraisons.add(livraison);
//        livraison.setCommande(this);
//        return this;
//    }
//
//    public Commande removeLivraison(Livraison livraison) {
//        this.livraisons.remove(livraison);
//        livraison.setCommande(null);
//        return this;
//    }

    public void setPaiements(Set<Paiement> paiements) {
        this.paiements = paiements;
    }

    public Commande paiements(Set<Paiement> paiements) {
        this.paiements = paiements;
        return this;
    }

    public Commande addPaiement(Paiement paiement) {
        this.paiements.add(paiement);
        paiement.setCommande(this);
        return this;
    }

    public Commande removePaiement(Paiement paiement) {
        this.paiements.remove(paiement);
        paiement.setCommande(null);
        return this;
    }

    public Set<Frais> getFrais() {
        return frais;
    }

    public void setFrais(Set<Frais> frais) {
        this.frais = frais;
    }

    public Commande frais(Set<Frais> frais) {
        this.frais = frais;
        return this;
    }

    public Commande addFrais(Frais frais) {
        this.frais.add(frais);
        frais.setCommande(this);
        return this;
    }

    public Commande removeFrais(Frais frais) {
        this.frais.remove(frais);
        frais.setCommande(null);
        return this;
    }

    public Fournisseur getFournisseur() {
        return fournisseur;
    }

    public void setFournisseur(Fournisseur fournisseur) {
        this.fournisseur = fournisseur;
    }

    public Commande fournisseur(Fournisseur fournisseur) {
        this.fournisseur = fournisseur;
        return this;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Commande client(Client client) {
        this.client = client;
        return this;
    }

    public boolean getDeleted() {
        return deleted;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Instant getDateLivraison() {
        return dateLivraison;
    }

    public void setDateLivraison(Instant dateLivraison) {
        this.dateLivraison = dateLivraison;
    }

    public Boolean getAvanceEnPercent() {
        return avanceEnPercent;
    }

    public void setAvanceEnPercent(Boolean avanceEnPercent) {
        this.avanceEnPercent = avanceEnPercent;
    }

    public Set<EtapeTransport> getEtapeTransports() {
        return etapeTransports;
    }

    public void setEtapeTransports(Set<EtapeTransport> etapeTransports) {
        this.etapeTransports = etapeTransports;
    }

    public Statut getStatut() {
        return statut;
    }

    public void setStatut(Statut statut) {
        this.statut = statut;
    }

    public BigDecimal getTva() {
        return tva;
    }

    public void setTva(BigDecimal tva) {
        this.tva = tva;
    }

    public Boolean getTvaEnPercent() {
        return tvaEnPercent;
    }

    public void setTvaEnPercent(Boolean tvaEnPercent) {
        this.tvaEnPercent = tvaEnPercent;
    }

    public Long getCommandeFournisseurId() {
        return commandeFournisseurId;
    }

    public void setCommandeFournisseurId(Long commandeFournisseurId) {
        this.commandeFournisseurId = commandeFournisseurId;
    }

    public TypeCommande getTypeCommande() {
        return typeCommande;
    }

    public void setTypeCommande(TypeCommande typeCommande) {
        this.typeCommande = typeCommande;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Commande)) {
            return false;
        }
        return id != null && id.equals(((Commande) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Commande{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", libelle='" + getLibelle() + "'" +
            ", date='" + getDate() + "'" +
            ", somme=" + getSomme() +
            ", dateLivraisonPrevu='" + getDateLivraisonPrevu() + "'" +
            ", dateLivraison='" + getDateLivraison() + "'" +
            ", etat='" + getEtat() + "'" +
            ", statut='" + getStatut() + "'" +
            ", motif='" + getMotif() + "'" +
            ", avance=" + getAvance() +
            ", tva=" + getTva() +
            ", tvaEnPercent='" + getTvaEnPercent() + "'" +
            ", avanceEnPercent='" + isAvanceEnPercent() + "'" +
            "}";
    }
}
