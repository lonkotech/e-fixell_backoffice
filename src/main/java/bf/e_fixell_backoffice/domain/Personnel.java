package bf.e_fixell_backoffice.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

/**
 * A Personnel.
 */
@Entity
@Table(name = "personnel")
public class Personnel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "deleted", columnDefinition = "boolean default false")
    private boolean deleted;

    @Column(name = "nom")
    private String nom;

    @Column(name = "activated", columnDefinition = "boolean default false")
    private boolean activated;

    @Column(name = "prenom")
    private String prenom;

    @Column(name = "telephone")
    private String telephone;

    @Column(name = "matricule")
    private String matricule;

    @Column(name = "bic")
    private String bic;

    @Column(name = "numero_compte")
    private String numeroCompte;

    @Column(name = "salaire", precision = 21, scale = 2, columnDefinition = "numeric default 0")
    private BigDecimal salaire;

    @Column(name = "isUser", columnDefinition = "boolean default false")
    private boolean isUser;


    @OneToMany(mappedBy = "personnel")
    private Set<HistoriqueAffectation> historiqueAffectations = new HashSet<>();

    @ManyToOne
    private Banque banque;

    @OneToMany(mappedBy = "personnel")
    private Set<Paiement> paiements = new HashSet<>();

    @Column(name = "iban")
    private String iban;

    public String getNumeroCompte() {
        return numeroCompte;
    }

    public void setNumeroCompte(String numeroCompte) {
        this.numeroCompte = numeroCompte;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Personnel nom(String nom) {
        this.nom = nom;
        return this;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Personnel prenom(String prenom) {
        this.prenom = prenom;
        return this;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Personnel telephone(String telephone) {
        this.telephone = telephone;
        return this;
    }

    public String getMatricule() {
        return matricule;
    }

    public void setMatricule(String matricule) {
        this.matricule = matricule;
    }

    public Personnel matricule(String matricule) {
        this.matricule = matricule;
        return this;
    }

    public BigDecimal getSalaire() {
        return salaire;
    }

    public void setSalaire(BigDecimal salaire) {
        this.salaire = salaire;
    }

    public Personnel salaire(BigDecimal salaire) {
        this.salaire = salaire;
        return this;
    }

    public Set<HistoriqueAffectation> getHistoriqueAffectations() {
        return historiqueAffectations;
    }

    public void setHistoriqueAffectations(Set<HistoriqueAffectation> historiqueAffectations) {
        this.historiqueAffectations = historiqueAffectations;
    }

    public Personnel historiqueAffectations(Set<HistoriqueAffectation> historiqueAffectations) {
        this.historiqueAffectations = historiqueAffectations;
        return this;
    }

    public Personnel addHistoriqueAffectation(HistoriqueAffectation historiqueAffectation) {
        this.historiqueAffectations.add(historiqueAffectation);
        historiqueAffectation.setPersonnel(this);
        return this;
    }

    public Personnel removeHistoriqueAffectation(HistoriqueAffectation historiqueAffectation) {
        this.historiqueAffectations.remove(historiqueAffectation);
        historiqueAffectation.setPersonnel(null);
        return this;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    public boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public String getBic() {
        return bic;
    }

    public void setBic(String bic) {
        this.bic = bic;
    }

    public Banque getBanque() {
        return banque;
    }

    public void setBanque(Banque banque) {
        this.banque = banque;
    }

    public boolean isUser() {
        return isUser;
    }

    public void setUser(boolean user) {
        isUser = user;
    }

    public Set<Paiement> getPaiements() {
        return paiements;
    }

    public void setPaiements(Set<Paiement> paiements) {
        this.paiements = paiements;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Personnel)) {
            return false;
        }
        return id != null && id.equals(((Personnel) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore

    @Override
    public String toString() {
        return "Personnel{" +
            "id=" + id +
            ", deleted=" + deleted +
            ", nom='" + nom + '\'' +
            ", activated=" + activated +
            ", prenom='" + prenom + '\'' +
            ", telephone='" + telephone + '\'' +
            ", matricule='" + matricule + '\'' +
            ", bic='" + bic + '\'' +
            ", numeroCompte='" + numeroCompte + '\'' +
            ", salaire=" + salaire +
            ", banque=" + banque +
            '}';
    }
}
