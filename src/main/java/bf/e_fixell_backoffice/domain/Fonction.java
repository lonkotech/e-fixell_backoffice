package bf.e_fixell_backoffice.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

/**
 * A Fonction.
 */
@Entity
@Table(name = "fonction")
public class Fonction implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "deleted", columnDefinition = "boolean default false")
    private boolean deleted;

    @Column(name = "libelle")
    private String libelle;

    @Column(name = "description")
    private String description;

    @Column(name = "salaire_min", columnDefinition = "numeric default 0")
    private BigDecimal salaireMin;

    @Column(name = "salaire_max", columnDefinition = "numeric default 0")
    private BigDecimal salaireMax;

    @OneToMany(mappedBy = "fonction")
    private Set<HistoriqueAffectation> historiqueAffectations = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Fonction libelle(String libelle) {
        this.libelle = libelle;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Fonction description(String description) {
        this.description = description;
        return this;
    }

    public BigDecimal getSalaireMin() {
        return salaireMin;
    }

    public void setSalaireMin(BigDecimal salaireMin) {
        this.salaireMin = salaireMin;
    }

    public Fonction salaireMin(BigDecimal salaireMin) {
        this.salaireMin = salaireMin;
        return this;
    }

    public BigDecimal getSalaireMax() {
        return salaireMax;
    }

    public void setSalaireMax(BigDecimal salaireMax) {
        this.salaireMax = salaireMax;
    }

    public Fonction salaireMax(BigDecimal salaireMax) {
        this.salaireMax = salaireMax;
        return this;
    }

    public Set<HistoriqueAffectation> getHistoriqueAffectations() {
        return historiqueAffectations;
    }

    public void setHistoriqueAffectations(Set<HistoriqueAffectation> historiqueAffectations) {
        this.historiqueAffectations = historiqueAffectations;
    }

    public Fonction historiqueAffectations(Set<HistoriqueAffectation> historiqueAffectations) {
        this.historiqueAffectations = historiqueAffectations;
        return this;
    }

    public Fonction addHistoriqueAffectation(HistoriqueAffectation historiqueAffectation) {
        this.historiqueAffectations.add(historiqueAffectation);
        historiqueAffectation.setFonction(this);
        return this;
    }

    public Fonction removeHistoriqueAffectation(HistoriqueAffectation historiqueAffectation) {
        this.historiqueAffectations.remove(historiqueAffectation);
        historiqueAffectation.setFonction(null);
        return this;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    public boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Fonction)) {
            return false;
        }
        return id != null && id.equals(((Fonction) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Fonction{" +
            "id=" + getId() +
            ", libelle='" + getLibelle() + "'" +
            ", description='" + getDescription() + "'" +
            ", salaireMin=" + getSalaireMin() +
            ", salaireMax=" + getSalaireMax() +
            "}";
    }
}
