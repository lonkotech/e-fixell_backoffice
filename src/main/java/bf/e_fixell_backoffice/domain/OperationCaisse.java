package bf.e_fixell_backoffice.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.math.BigDecimal;

import bf.e_fixell_backoffice.domain.enumeration.TypeOperationCaisse;

/**
 * A OperationCaisse.
 */
@Entity
@Table(name = "operation_caisse")
public class OperationCaisse extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "deleted", columnDefinition = "boolean default false")
    private boolean deleted;

    @Enumerated(EnumType.STRING)
    @Column(name = "type_operation_caisse")
    private TypeOperationCaisse typeOperationCaisse;

    @Column(name = "montant", precision = 21, scale = 2, columnDefinition = "numeric default 0")
    private BigDecimal montant;

    @ManyToOne
    @JsonIgnoreProperties(value = "operationCaisses", allowSetters = true)
    private Caisse caisseSrc;

    @ManyToOne
    @JsonIgnoreProperties(value = "operationCaisses", allowSetters = true)
    private Caisse caisseDst;

    @Column(name = "solde_avant", columnDefinition = "numeric default 0")
    private BigDecimal soldeAvant;

    @Column(name = "solde_apres", columnDefinition = "numeric default 0")
    private BigDecimal soldeApres;

    @Column(name = "description")
    private String description;

    @ManyToOne
    private SessionCaisse sessionCaisse;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TypeOperationCaisse getTypeOperationCaisse() {
        return typeOperationCaisse;
    }

    public void setTypeOperationCaisse(TypeOperationCaisse typeOperationCaisse) {
        this.typeOperationCaisse = typeOperationCaisse;
    }

    public OperationCaisse typeOperationCaisse(TypeOperationCaisse typeOperationCaisse) {
        this.typeOperationCaisse = typeOperationCaisse;
        return this;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public OperationCaisse montant(BigDecimal montant) {
        this.montant = montant;
        return this;
    }

    public Caisse getCaisseSrc() {
        return caisseSrc;
    }

    public void setCaisseSrc(Caisse caisse) {
        this.caisseSrc = caisse;
    }

    public OperationCaisse caisseSrc(Caisse caisse) {
        this.caisseSrc = caisse;
        return this;
    }

    public Caisse getCaisseDst() {
        return caisseDst;
    }

    public void setCaisseDst(Caisse caisse) {
        this.caisseDst = caisse;
    }

    public OperationCaisse caisseDst(Caisse caisse) {
        this.caisseDst = caisse;
        return this;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    public boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public SessionCaisse getSessionCaisse() {
        return sessionCaisse;
    }

    public void setSessionCaisse(SessionCaisse sessionCaisse) {
        this.sessionCaisse = sessionCaisse;
    }

    public BigDecimal getSoldeAvant() {
        return soldeAvant;
    }

    public void setSoldeAvant(BigDecimal soldeAvant) {
        this.soldeAvant = soldeAvant;
    }

    public BigDecimal getSoldeApres() {
        return soldeApres;
    }

    public void setSoldeApres(BigDecimal soldeApres) {
        this.soldeApres = soldeApres;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OperationCaisse)) {
            return false;
        }
        return id != null && id.equals(((OperationCaisse) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OperationCaisse{" +
            "id=" + getId() +
            ", typeOperationCaisse='" + getTypeOperationCaisse() + "'" +
            ", montant=" + getMontant() +
            "}";
    }
}
