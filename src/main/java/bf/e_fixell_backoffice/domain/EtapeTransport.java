package bf.e_fixell_backoffice.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;

/**
 * A Etape Transport
 */
@Entity
@Table(name = "etape_transport")
public class EtapeTransport extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "libelle")
    private String libelle;

    @Column(name = "niveau")
    private Long niveau;

    @Column(name = "date_depart")
    private Instant dateDepart;

    @Column(name = "date_arrive")
    private Instant dateArrive;

    @Column(name = "deleted", columnDefinition = "boolean default false")
    private boolean deleted;

    @ManyToOne
    @JsonIgnoreProperties(value = "etapeTransport", allowSetters = true)
    private Commande commande;

    @ManyToOne
    @JsonIgnoreProperties(value = "etapeTransport", allowSetters = true)
    private SocieteTransport societeTransport;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    public boolean getDeleted() {
        return deleted;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Long getNiveau() {
        return niveau;
    }

    public void setNiveau(Long niveau) {
        this.niveau = niveau;
    }

    public Instant getDateDepart() {
        return dateDepart;
    }

    public void setDateDepart(Instant dateDepart) {
        this.dateDepart = dateDepart;
    }

    public Instant getDateArrive() {
        return dateArrive;
    }

    public void setDateArrive(Instant dateArrive) {
        this.dateArrive = dateArrive;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Commande getCommande() {
        return commande;
    }

    public void setCommande(Commande commande) {
        this.commande = commande;
    }

    public SocieteTransport getSocieteTransport() {
        return societeTransport;
    }

    public void setSocieteTransport(SocieteTransport societeTransport) {
        this.societeTransport = societeTransport;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EtapeTransport)) {
            return false;
        }
        return id != null && id.equals(((EtapeTransport) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore

    @Override
    public String toString() {
        return "EtapeTransport{" +
            "id=" + id +
            ", libelle='" + libelle + '\'' +
            ", niveau=" + niveau +
            ", dateDepart=" + dateDepart +
            ", dateArrive=" + dateArrive +
            ", deleted=" + deleted +
            ", commande=" + commande +
            ", societeTransport=" + societeTransport +
            '}';
    }
}
