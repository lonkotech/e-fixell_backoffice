package bf.e_fixell_backoffice.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Inventaire.
 */
@Entity
@Table(name = "inventaire")
public class Inventaire extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "quantite_theorique")
    private Integer quantiteTheorique;

    @Column(name = "quantite_physique")
    private Integer quantitePhysique;

    @Column(name = "ecart")
    private Integer ecart;

    @Column(name = "commentaire")
    private String commentaire;

    @ManyToOne
    @JsonIgnoreProperties(value = "inventaires", allowSetters = true)
    private Produit produit;

    @OneToMany(mappedBy = "inventaire")
    private Set<Perte> pertes = new HashSet<>();

    @OneToMany(mappedBy = "inventaire")
    private Set<Transaction> transactions = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getQuantiteTheorique() {
        return quantiteTheorique;
    }

    public void setQuantiteTheorique(Integer quantiteTheorique) {
        this.quantiteTheorique = quantiteTheorique;
    }

    public Inventaire quantiteTheorique(Integer quantiteTheorique) {
        this.quantiteTheorique = quantiteTheorique;
        return this;
    }

    public Integer getQuantitePhysique() {
        return quantitePhysique;
    }

    public void setQuantitePhysique(Integer quantitePhysique) {
        this.quantitePhysique = quantitePhysique;
    }

    public Inventaire quantitePhysique(Integer quantitePhysique) {
        this.quantitePhysique = quantitePhysique;
        return this;
    }

    public Integer getEcart() {
        return ecart;
    }

    public void setEcart(Integer ecart) {
        this.ecart = ecart;
    }

    public Inventaire ecart(Integer ecart) {
        this.ecart = ecart;
        return this;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public Inventaire commentaire(String commentaire) {
        this.commentaire = commentaire;
        return this;
    }

    public Produit getProduit() {
        return produit;
    }

    public void setProduit(Produit produit) {
        this.produit = produit;
    }

    public Inventaire produit(Produit produit) {
        this.produit = produit;
        return this;
    }

    public Set<Perte> getPertes() {
        return pertes;
    }

    public void setPertes(Set<Perte> pertes) {
        this.pertes = pertes;
    }

    public Inventaire pertes(Set<Perte> pertes) {
        this.pertes = pertes;
        return this;
    }

    public Set<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(Set<Transaction> transactions) {
        this.transactions = transactions;
    }

    public Inventaire transactions(Set<Transaction> transactions) {
        this.transactions = transactions;
        return this;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Inventaire)) {
            return false;
        }
        return id != null && id.equals(((Inventaire) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Inventaire{" +
            "id=" + getId() +
            ", quantiteTheorique=" + getQuantiteTheorique() +
            ", quantitePhysique=" + getQuantitePhysique() +
            ", ecart=" + getEcart() +
            ", commentaire='" + getCommentaire() + "'" +
            "}";
    }
}
