package bf.e_fixell_backoffice.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

/**
 * A Produit.
 */
@Entity
@Table(name = "produit")
public class Produit extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "code")
    private String code;

    @Column(name = "libelle")
    private String libelle;

    @Column(name = "quantite", columnDefinition = "Integer default 0")
    private Integer quantite;

    @Column(name = "stock_client", columnDefinition = "Integer default 0")
    private Integer stockClient;

    @Column(name = "prix_actuel", columnDefinition = "numeric default 0")
    private BigDecimal prixActuel;

    @ApiModelProperty(name = "le prix de vente courant")
    @Column(name = "prix_vente", columnDefinition = "numeric default 0")
    private BigDecimal prixVenteCourant;

    @ApiModelProperty(name = "le prix d'achat courant")
    @Column(name = "prix_achat", columnDefinition = "numeric default 0")
    private BigDecimal prixAchatCourant;

    @Column(name = "hs_code")
    private String hsCode;

    @Column(name = "deleted", columnDefinition = "boolean default false")
    private boolean deleted;

    @OneToMany(mappedBy = "produit")
    private Set<Perte> pertes = new HashSet<>();

    @OneToMany(mappedBy = "produit")
    private Set<Transaction> transactions = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = "produits", allowSetters = true)
    private Categorie categorie;

    @ManyToOne
    @JsonIgnoreProperties(value = "produits", allowSetters = true)
    private Classification classification;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Produit code(String code) {
        this.code = code;
        return this;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Produit libelle(String libelle) {
        this.libelle = libelle;
        return this;
    }

    public Integer getQuantite() {
        return quantite;
    }

    public void setQuantite(Integer quantite) {
        this.quantite = quantite;
    }

    public Produit quantite(Integer quantite) {
        this.quantite = quantite;
        return this;
    }

    public String getHsCode() {
        return hsCode;
    }

    public void setHsCode(String hsCode) {
        this.hsCode = hsCode;
    }

    public Produit hsCode(String hsCode) {
        this.hsCode = hsCode;
        return this;
    }

    public Set<Perte> getPertes() {
        return pertes;
    }

    public void setPertes(Set<Perte> pertes) {
        this.pertes = pertes;
    }

    public Produit pertes(Set<Perte> pertes) {
        this.pertes = pertes;
        return this;
    }

    public Produit addPerte(Perte perte) {
        this.pertes.add(perte);
        perte.setProduit(this);
        return this;
    }

    public Produit removePerte(Perte perte) {
        this.pertes.remove(perte);
        perte.setProduit(null);
        return this;
    }

    public Set<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(Set<Transaction> transactions) {
        this.transactions = transactions;
    }

    public Produit transactions(Set<Transaction> transactions) {
        this.transactions = transactions;
        return this;
    }

    public Produit addTransaction(Transaction transaction) {
        this.transactions.add(transaction);
        transaction.setProduit(this);
        return this;
    }

    public Produit removeTransaction(Transaction transaction) {
        this.transactions.remove(transaction);
        transaction.setProduit(null);
        return this;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    public Produit categorie(Categorie categorie) {
        this.categorie = categorie;
        return this;
    }

    public Classification getClassification() {
        return classification;
    }

    public void setClassification(Classification classification) {
        this.classification = classification;
    }

    public Produit classification(Classification classification) {
        this.classification = classification;
        return this;
    }

    public BigDecimal getPrixActuel() {
        return prixActuel;
    }

    public void setPrixActuel(BigDecimal prixActuel) {
        this.prixActuel = prixActuel;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    public boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Integer getStockClient() {
        return stockClient;
    }

    public void setStockClient(Integer stockClient) {
        this.stockClient = stockClient;
    }

    public BigDecimal getPrixVenteCourant() {
        return prixVenteCourant;
    }

    public void setPrixVenteCourant(BigDecimal prixVenteCourant) {
        this.prixVenteCourant = prixVenteCourant;
    }

    public BigDecimal getPrixAchatCourant() {
        return prixAchatCourant;
    }

    public void setPrixAchatCourant(BigDecimal prixAchatCourant) {
        this.prixAchatCourant = prixAchatCourant;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Produit)) {
            return false;
        }
        return id != null && id.equals(((Produit) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Produit{" +
            "id=" + id +
            ", code='" + code + '\'' +
            ", libelle='" + libelle + '\'' +
            ", quantite=" + quantite +
            ", prixActuel=" + prixActuel +
            ", hsCode='" + hsCode + '\'' +
            ", deleted=" + deleted +
            ", pertes=" + pertes +
            ", transactions=" + transactions +
            ", stockClient=" + stockClient +
            ", categorie=" + categorie +
            ", classification=" + classification +
            '}';
    }
}
