package bf.e_fixell_backoffice.web.rest.reporting;

public enum ReportFormat {
    PDF, XLSX, CSV
}
