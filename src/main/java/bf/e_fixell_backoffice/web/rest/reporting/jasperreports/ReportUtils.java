package bf.e_fixell_backoffice.web.rest.reporting.jasperreports;

import bf.e_fixell_backoffice.web.rest.reporting.ReportFormat;

public class ReportUtils {

    public static AbstractJasperReportsView getJasperView(ReportFormat format) {
        AbstractJasperReportsView view;
        switch (format) {
            case XLSX:
                view = new JasperReportsXlsxView();
                break;
            case CSV:
                view = new JasperReportsCsvView();
                break;
            case PDF:
                view = new JasperReportsPdfView();
                break;
            default:
                view = new JasperReportsPdfView();
                break;
        }
        return view;
    }
}
