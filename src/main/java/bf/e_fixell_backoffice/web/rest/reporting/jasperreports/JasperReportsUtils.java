//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package bf.e_fixell_backoffice.web.rest.reporting.jasperreports;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanArrayDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;

import java.io.OutputStream;
import java.io.Writer;
import java.util.Collection;
import java.util.Map;

public abstract class JasperReportsUtils {
    public JasperReportsUtils() {
    }

    public static JRDataSource convertReportData(Object value) throws IllegalArgumentException {
        if (value instanceof JRDataSource) {
            return (JRDataSource) value;
        } else if (value instanceof Collection) {
            return new JRBeanCollectionDataSource((Collection) value);
        } else if (value instanceof Object[]) {
            return new JRBeanArrayDataSource((Object[]) ((Object[]) value));
        } else {
            throw new IllegalArgumentException("Value [" + value + "] cannot be converted to a JRDataSource");
        }
    }

    public static void render(JRExporter exporter, JasperPrint print, Writer writer) throws JRException {
        exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
        exporter.setParameter(JRExporterParameter.OUTPUT_WRITER, writer);
        exporter.exportReport();
    }

    public static void render(JRExporter exporter, JasperPrint print, OutputStream outputStream) throws JRException {
        exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outputStream);
        exporter.exportReport();
    }

    public static void renderAsCsv(JasperReport report, Map<String, Object> parameters, Object reportData, Writer writer) throws JRException {
        JasperPrint print = JasperFillManager.fillReport(report, parameters, convertReportData(reportData));
        render(new JRCsvExporter(), print, (Writer) writer);
    }

    public static void renderAsCsv(JasperReport report, Map<String, Object> parameters, Object reportData, Writer writer, Map<JRExporterParameter, Object> exporterParameters) throws JRException {
        JasperPrint print = JasperFillManager.fillReport(report, parameters, convertReportData(reportData));
        JRCsvExporter exporter = new JRCsvExporter();
        exporter.setParameters(exporterParameters);
        render(exporter, print, (Writer) writer);
    }

    public static void renderAsPdf(JasperReport report, Map<String, Object> parameters, Object reportData, OutputStream stream) throws JRException {
        JasperPrint print = JasperFillManager.fillReport(report, parameters, convertReportData(reportData));
        render(new JRPdfExporter(), print, (OutputStream) stream);
    }

    public static void renderAsPdf(JasperReport report, Map<String, Object> parameters, Object reportData, OutputStream stream, Map<JRExporterParameter, Object> exporterParameters) throws JRException {
        JasperPrint print = JasperFillManager.fillReport(report, parameters, convertReportData(reportData));
        JRPdfExporter exporter = new JRPdfExporter();
        exporter.setParameters(exporterParameters);
        render(exporter, print, (OutputStream) stream);
    }

    public static void renderAsXls(JasperReport report, Map<String, Object> parameters, Object reportData, OutputStream stream) throws JRException {
        JasperPrint print = JasperFillManager.fillReport(report, parameters, convertReportData(reportData));
        render(new JRXlsExporter(), print, (OutputStream) stream);
    }

    public static void renderAsXls(JasperReport report, Map<String, Object> parameters, Object reportData, OutputStream stream, Map<JRExporterParameter, Object> exporterParameters) throws JRException {
        JasperPrint print = JasperFillManager.fillReport(report, parameters, convertReportData(reportData));
        JRXlsExporter exporter = new JRXlsExporter();
        exporter.setParameters(exporterParameters);
        render(exporter, print, (OutputStream) stream);
    }
}
