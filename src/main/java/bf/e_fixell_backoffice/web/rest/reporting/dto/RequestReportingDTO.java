package bf.e_fixell_backoffice.web.rest.reporting.dto;

import bf.e_fixell_backoffice.web.rest.reporting.ReportFormat;

public class RequestReportingDTO {
    private Long id;
    private ReportFormat format;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ReportFormat getFormat() {
        return format;
    }

    public void setFormat(ReportFormat format) {
        this.format = format;
    }
}
