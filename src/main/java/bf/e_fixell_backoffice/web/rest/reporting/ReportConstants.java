package bf.e_fixell_backoffice.web.rest.reporting;

public class ReportConstants {
    public final static String PARAM_DATASOURCE = "datasource";
    public final static String URL_IMAGE = "src/main/resources/report/image/";
    public final static String ORDRE_VIREMENT_PDF = "classpath:report/ordre_virement_pdf.jasper";
    public final static String ORDRE_VIREMENT_XLSX = "classpath:report/ordre_virement_csv.jasper";
    public final static String BORDEREAU_COMMANDE_PDF = "classpath:report/bordereau_commande_pdf.jasper";
    public final static String BORDEREAU_COMMANDE_XLSX = "classpath:report/bordereau_commande_csv.jasper";
    public final static String BORDEREAU_LIVRAISON_PDF = "classpath:report/bordereau_livraison_pdf.jasper";
    public final static String BORDEREAU_LIVRAISON_XLSX = "classpath:report/bordereau_livraison_csv.jasper";
}
