package bf.e_fixell_backoffice.web.rest.reporting.dto;

import java.math.BigDecimal;
import java.util.List;

public class BordereauCommandeDTO extends Common{
    private String client;
    private String adresse;
    private String tel;
    private String date;
    private String numeroClient;
    private String libelle;
    private String numeroBordereau;
    private String gerant;
    private String tva;
    private BigDecimal totalTTC;
    private BigDecimal totalHT;
    private BigDecimal montantTva;
    private BigDecimal avance;
    private BigDecimal restant;
    private String delai;
    private String nb;
    private String cc;
    private String regimeImposition;
    private String centreImpot;
    private String pourcentageCommande;
    private String pourcentageLivraison;
    private String prixTotalEnLettre;
    private String remarque;
    private List<LigneDTO> lignes;

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getNumeroClient() {
        return numeroClient;
    }

    public void setNumeroClient(String numeroClient) {
        this.numeroClient = numeroClient;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getNumeroBordereau() {
        return numeroBordereau;
    }

    public void setNumeroBordereau(String numeroBordereau) {
        this.numeroBordereau = numeroBordereau;
    }

    public String getGerant() {
        return gerant;
    }

    public void setGerant(String gerant) {
        this.gerant = gerant;
    }

    public String getTva() {
        return tva;
    }

    public void setTva(String tva) {
        this.tva = tva;
    }

    public BigDecimal getTotalTTC() {
        return totalTTC;
    }

    public void setTotalTTC(BigDecimal totalTTC) {
        this.totalTTC = totalTTC;
    }

    public BigDecimal getAvance() {
        return avance;
    }

    public void setAvance(BigDecimal avance) {
        this.avance = avance;
    }

    public BigDecimal getRestant() {
        return restant;
    }

    public void setRestant(BigDecimal restant) {
        this.restant = restant;
    }

    public String getDelai() {
        return delai;
    }

    public void setDelai(String delai) {
        this.delai = delai;
    }

    public String getNb() {
        return nb;
    }

    public void setNb(String nb) {
        this.nb = nb;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getRegimeImposition() {
        return regimeImposition;
    }

    public void setRegimeImposition(String regimeImposition) {
        this.regimeImposition = regimeImposition;
    }

    public String getCentreImpot() {
        return centreImpot;
    }

    public void setCentreImpot(String centreImpot) {
        this.centreImpot = centreImpot;
    }

    public String getPourcentageCommande() {
        return pourcentageCommande;
    }

    public void setPourcentageCommande(String pourcentageCommande) {
        this.pourcentageCommande = pourcentageCommande;
    }

    public String getPourcentageLivraison() {
        return pourcentageLivraison;
    }

    public void setPourcentageLivraison(String pourcentageLivraison) {
        this.pourcentageLivraison = pourcentageLivraison;
    }

    public String getPrixTotalEnLettre() {
        return prixTotalEnLettre;
    }

    public void setPrixTotalEnLettre(String prixTotalEnLettre) {
        this.prixTotalEnLettre = prixTotalEnLettre;
    }

    public String getRemarque() {
        return remarque;
    }

    public void setRemarque(String remarque) {
        this.remarque = remarque;
    }

    public List<LigneDTO> getLignes() {
        return lignes;
    }

    public void setLignes(List<LigneDTO> lignes) {
        this.lignes = lignes;
    }

    public BigDecimal getTotalHT() {
        return totalHT;
    }

    public void setTotalHT(BigDecimal totalHT) {
        this.totalHT = totalHT;
    }

    public BigDecimal getMontantTva() {
        return montantTva;
    }

    public void setMontantTva(BigDecimal montantTva) {
        this.montantTva = montantTva;
    }

}
