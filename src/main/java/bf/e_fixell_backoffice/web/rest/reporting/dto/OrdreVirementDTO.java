package bf.e_fixell_backoffice.web.rest.reporting.dto;


public class OrdreVirementDTO extends Common{
    private String banque;
    private String iban;
    private String montant;
    private String libelle;
    private String titulaire;
    private String numeroCompte;
    private String bic;
    private String lieuDate;

    public String getBanque() {
        return banque;
    }

    public void setBanque(String banque) {
        this.banque = banque;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getMontant() {
        return montant;
    }

    public void setMontant(String montant) {
        this.montant = montant;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getTitulaire() {
        return titulaire;
    }

    public void setTitulaire(String titulaire) {
        this.titulaire = titulaire;
    }

    public String getNumeroCompte() {
        return numeroCompte;
    }

    public void setNumeroCompte(String numeroCompte) {
        this.numeroCompte = numeroCompte;
    }

    public String getBic() {
        return bic;
    }

    public void setBic(String bic) {
        this.bic = bic;
    }

    public String getLieuDate() {
        return lieuDate;
    }

    public void setLieuDate(String lieuDate) {
        this.lieuDate = lieuDate;
    }
}
