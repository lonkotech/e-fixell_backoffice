package bf.e_fixell_backoffice.web.rest.reporting.rest;

import bf.e_fixell_backoffice.web.rest.reporting.ReportFormat;
import bf.e_fixell_backoffice.web.rest.reporting.dto.RequestReportingDTO;
import bf.e_fixell_backoffice.web.rest.reporting.service.ReportingService;
import io.swagger.annotations.ApiParam;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping("api/reporting/")
public class ReportingResource {

    private final ReportingService reportingService;
    private final ApplicationContext context;

    public ReportingResource(ReportingService reportingService, ApplicationContext context) {
        this.reportingService = reportingService;
        this.context = context;
    }

    @GetMapping("bordereau-commande/{id}")
    public ModelAndView viewCommande(@PathVariable Long id) {
        RequestReportingDTO r = new RequestReportingDTO();
        r.setId(id);
        r.setFormat(ReportFormat.PDF);
        return this.reportingService.viewBordereauCommande(context, r);
    }

    @GetMapping("bordereau-livraison/{id}")
    public ModelAndView viewLivraison(@PathVariable Long id) {
        RequestReportingDTO r = new RequestReportingDTO();
        r.setId(id);
        r.setFormat(ReportFormat.PDF);
        return this.reportingService.viewBordereauLivraison(context, r);

    }

    @GetMapping("ordre-virement/{id}")
    public ModelAndView viewOrdreVirement(@ApiParam(defaultValue = "id correspond au id du paiement en question") @PathVariable Long id){
        RequestReportingDTO r = new RequestReportingDTO();
        r.setId(id);
        r.setFormat(ReportFormat.PDF);
        return this.reportingService.viewOrdeVirement(context,r);

    }
}
