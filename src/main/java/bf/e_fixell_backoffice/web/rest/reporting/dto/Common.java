package bf.e_fixell_backoffice.web.rest.reporting.dto;

public class Common {
    String entetePage;
    String piedPage;

    public String getEntetePage() {
        return entetePage;
    }

    public void setEntetePage(String entetePage) {
        this.entetePage = entetePage;
    }

    public String getPiedPage() {
        return piedPage;
    }

    public void setPiedPage(String piedPage) {
        this.piedPage = piedPage;
    }
}
