package bf.e_fixell_backoffice.web.rest.reporting.dto;

import java.math.BigDecimal;

public class LigneDTO {
    private Integer quantite;
    private BigDecimal prixUnitaireHT;
    private BigDecimal prixTotalHT;
    private String designation;
    private String statut;
    private String observation;

    public Integer getQuantite() {
        return quantite;
    }

    public void setQuantite(Integer quantite) {
        this.quantite = quantite;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public BigDecimal getPrixUnitaireHT() {
        return prixUnitaireHT;
    }

    public void setPrixUnitaireHT(BigDecimal prixUnitaireHT) {
        this.prixUnitaireHT = prixUnitaireHT;
    }

    public BigDecimal getPrixTotalHT() {
        return prixTotalHT;
    }

    public void setPrixTotalHT(BigDecimal prixTotalHT) {
        this.prixTotalHT = prixTotalHT;
    }
}
