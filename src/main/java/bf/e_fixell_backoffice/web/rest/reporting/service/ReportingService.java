package bf.e_fixell_backoffice.web.rest.reporting.service;

import bf.e_fixell_backoffice.domain.*;
import bf.e_fixell_backoffice.domain.enumeration.Etat;
import bf.e_fixell_backoffice.repository.*;
import bf.e_fixell_backoffice.service.TransactionService;
import bf.e_fixell_backoffice.service.dto.TransactionDTO;
import bf.e_fixell_backoffice.service.mapper.LivraisonMapper;
import bf.e_fixell_backoffice.web.rest.CommandeResource;
import bf.e_fixell_backoffice.web.rest.reporting.ReportConstants;
import bf.e_fixell_backoffice.web.rest.reporting.ReportFormat;
import bf.e_fixell_backoffice.web.rest.reporting.dto.*;
import bf.e_fixell_backoffice.web.rest.reporting.jasperreports.AbstractJasperReportsView;
import bf.e_fixell_backoffice.web.rest.reporting.jasperreports.ReportUtils;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.ModelAndView;
import pl.allegro.finance.tradukisto.MoneyConverters;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;

@Service
@Transactional
public class ReportingService {

    DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    private final CommandeRepository commandeRepository;
    private final LivraisonRepository livraisonRepository;
    private final PersonnelRepository personnelRepository;
    private final TransactionRepository transactionRepository;
    private final TransactionService transactionService;
    private final PaiementRepository paiementRepository;
    private final Logger log = LoggerFactory.getLogger(CommandeResource.class);

    public ReportingService(CommandeRepository commandeRepository, LivraisonRepository livraisonRepository, PersonnelRepository personnelRepository, TransactionRepository transactionRepository, TransactionService transactionService, PaiementRepository paiementRepository) {
        this.commandeRepository = commandeRepository;
        this.livraisonRepository = livraisonRepository;
        this.personnelRepository = personnelRepository;
        this.transactionRepository = transactionRepository;
        this.transactionService = transactionService;
        this.paiementRepository = paiementRepository;
    }

    private BordereauCommandeDTO createBordereauCommande(Long idCommande){
        Commande c = commandeRepository.findById(idCommande).get();
        BordereauCommandeDTO bCommande = new BordereauCommandeDTO();
        Boolean hasTva = c.getClient().getPayerTva();
        bCommande.setTotalHT(BigDecimal.ZERO);
        bCommande.setMontantTva(BigDecimal.ZERO);
        List<LigneDTO> lignes = new ArrayList<>();
        bCommande.setTotalTTC(c.getSomme());
        bCommande.setNumeroBordereau(c.getCode());
        bCommande.setTel(c.getClient().getTelephone());
        bCommande.setClient(c.getClient() != null ?
                 (c.getClient().getPrenom() != null ? c.getClient().getPrenom() : c.getClient().getRaisonSocial()) : null );
        String nom  =
            c.getClient() != null ?
                (c.getClient().getNom() != null ? c.getClient().getNom() : null) : null;
        if (nom != null)
            bCommande.setClient(bCommande.getClient()+" "+nom);

        bCommande.setAdresse(c.getClient() != null ? c.getClient().getAdresse() : null);
        bCommande.setCc(c.getClient() != null ? c.getClient().getIdentifiant() : null);
        bCommande.setDate(c.getDate().atZone(ZoneId.systemDefault()).format(dateFormatter));
        bCommande.setLibelle(c.getLibelle());
        bCommande.setNb("Proformat valable 10 jours compte tenu d'une fluctuation importante du court du dollar américain");
        bCommande.setDelai(ChronoUnit.DAYS.between(c.getDate(),c.getDateLivraisonPrevu()) + " jour(s)");
        for (Transaction tt: c.getTransactions()) {
            Transaction t = transactionRepository.findById(tt.getId()).get();
            LigneDTO l = new LigneDTO();
            l.setDesignation(t.getProduit().getLibelle());
            String caracteristque = returnCaracteristique(tt.getCaracteristiques());
/*            if (tt.getCaracteristiques() != null){
                String tab[] = tt.getCaracteristiques().split(";");
                for (int i = 0; i < tab.length; i++) {
                    String tabb[] = tab[i].split(":");
                    caracteristque = caracteristque+" "+tabb[1];
                }
            }*/
            l.setDesignation(l.getDesignation()+" "+caracteristque);
            l.setQuantite(t.getQuantite());
            l.setPrixUnitaireHT(t.getPrixVenteUnitaire());
            l.setPrixTotalHT(l.getPrixUnitaireHT().multiply(new BigDecimal(l.getQuantite().toString())));
            lignes.add(l);
            bCommande.setTotalHT(bCommande.getTotalHT().add(l.getPrixTotalHT()));
        }
        if (hasTva){
            if (c.getTvaEnPercent())
                bCommande.setMontantTva(c.getTva().multiply(bCommande.getTotalHT().multiply(new BigDecimal("0.01"))));
            else
                bCommande.setMontantTva(c.getTva());
        }

        bCommande.setTotalTTC((bCommande.getTotalHT().add(bCommande.getMontantTva())).setScale(0, RoundingMode.HALF_DOWN));

        bCommande.setPrixTotalEnLettre("Arrêter la présente facture proforma à la somme de : "+chifferToLetters(bCommande.getTotalTTC().toString()));
        if (!c.getAvanceEnPercent()) {
            bCommande.setAvance(c.getAvance());
            System.out.println("\n\nc.getAvance()"+c.getAvance());
            System.out.println("bCommande.getTotalTTC()"+bCommande.getTotalTTC()+"\n\n");
            bCommande.setPourcentageCommande((c.getAvance().multiply(new BigDecimal("100"))).divide(bCommande.getTotalTTC(),2, RoundingMode.HALF_DOWN).toString());
            bCommande.setPourcentageLivraison(new BigDecimal("100").subtract(new BigDecimal(bCommande.getPourcentageCommande())).toString());
            bCommande.setPourcentageCommande(bCommande.getPourcentageCommande()+"% à la commande");
            bCommande.setPourcentageLivraison(bCommande.getPourcentageLivraison()+"% à la livraison");
        }
        else {
            bCommande.setAvance(bCommande.getTotalTTC().multiply(c.getAvance().multiply(new BigDecimal("0.01"))).setScale(0, RoundingMode.HALF_DOWN));
            bCommande.setPourcentageCommande(c.getAvance().setScale(0).toString());
            bCommande.setPourcentageLivraison((new BigDecimal("100").subtract(new BigDecimal(bCommande.getPourcentageCommande())).setScale(0).toString()));
            bCommande.setPourcentageCommande(bCommande.getPourcentageCommande()+"% à la commande");
            bCommande.setPourcentageLivraison(bCommande.getPourcentageLivraison()+"% à la livraison");
        }

        bCommande.setRestant(bCommande.getTotalTTC().subtract(bCommande.getAvance()));
        bCommande.setLignes(lignes);
        bCommande.setGerant("cooool");
        bCommande.setPiedPage(new File(ReportConstants.URL_IMAGE+"piedpage.jpg").getAbsolutePath());
        bCommande.setEntetePage(new File(ReportConstants.URL_IMAGE+"entetepage.jpg").getAbsolutePath());

        System.out.println("--------------------------------------");
        System.out.println(c.getClient());
        System.out.println("--------------------------------------");
        return bCommande;
    }

    private BordereauLivraisonDTO createBordereauLivraison(Long idCommande){
        BordereauLivraisonDTO bL = new BordereauLivraisonDTO();
        List<Transaction> tList = transactionRepository.findByCommandeIdAndEtat(idCommande, Etat.VALIDER);
        List<LigneDTO> lignes = new ArrayList<>();

        Commande c = commandeRepository.findById(idCommande).get();
        if (c != null && c.getDateLivraison() != null){
            bL.setClient(c.getClient() != null ?
                (c.getClient().getPrenom() != null ? c.getClient().getPrenom() : c.getClient().getRaisonSocial()) : null );
            String nom  =
                c.getClient() != null ?
                (c.getClient().getNom() != null ? c.getClient().getNom() : null) : null;
            if (nom != null)
                bL.setClient(bL.getClient()+" "+nom);
            bL.setAdresse(c.getClient() != null ? c.getClient().getAdresse() : null);
            bL.setNumeroClient(c.getClient() != null ? c.getClient().getIdentifiant() : null);
            // la date de la dernière livraison
            bL.setDate(c.getDateLivraison().atZone(ZoneId.systemDefault()).format(dateFormatter));
            bL.setLibelle(c.getLibelle());

            for (Transaction t: tList) {
                LigneDTO op = new LigneDTO();
                op.setDesignation(t.getProduit().getLibelle());
                op.setQuantite(t.getQuantiteALivre());
                op.setObservation("");
                op.setStatut("Livré");

                String caracteristque = "";
                if (t.getCaracteristiques() != null){
                    String tab[] = t.getCaracteristiques().split(";");
                    for (int i = 0; i < tab.length; i++) {
                        String tabb[] = tab[i].split(":");
                        caracteristque = caracteristque+" "+tabb[1];
                    }
                }
                op.setDesignation(op.getDesignation()+" "+caracteristque);
                lignes.add(op);
            }
            bL.setLignes(lignes);
            bL.setGerant("cooool");
            bL.setLieuLivraison("EEAD YOPOUGON PORT-BOUET 2");
            bL.setPiedPage(new File(ReportConstants.URL_IMAGE+"piedpage.jpg").getAbsolutePath());
            bL.setEntetePage(new File(ReportConstants.URL_IMAGE+"entetepage.jpg").getAbsolutePath());

        } else {
            throw new RuntimeException("commande non livré ou inexistant");
        }
        return bL;
    }


    public BordereauLivraisonDTO createBordereauLivraisonNew(Long idLivraison){
        BordereauLivraisonDTO bL = new BordereauLivraisonDTO();
        Livraison l = livraisonRepository.findById(idLivraison).get();

        List<LigneDTO> lignes = new ArrayList<>();

        bL.setClient(l.getCommande().getClient() != null ?
            (l.getCommande().getClient().getPrenom() != null ? l.getCommande().getClient().getPrenom() : l.getCommande().getClient().getRaisonSocial()) : null );
        String nom  =
            l.getCommande().getClient() != null ?
                (l.getCommande().getClient().getNom() != null ? l.getCommande().getClient().getNom() : null) : null;
        if (nom != null)
            bL.setClient(bL.getClient()+" "+nom);

        bL.setAdresse(l.getCommande().getClient() != null ? l.getCommande().getClient().getAdresse() : null);
        bL.setNumeroClient(l.getCommande().getClient() != null ? l.getCommande().getClient().getIdentifiant() : null);

        // la date de la dernière livraison
        bL.setDate(l.getDate().atZone(ZoneId.systemDefault()).format(dateFormatter));
        bL.setLibelle(l.getLibelle());
        bL.setTel(l.getCommande().getClient().getTelephone());
        bL.setCc(l.getCommande().getClient().getIdentifiant());

        bL.setGerant("cooool");
        bL.setNumeroBordereau(l.getCode());
        bL.setLieuLivraison("EEAD YOPOUGON PORT-BOUET 2");
        bL.setPiedPage(new File(ReportConstants.URL_IMAGE+"piedpage.jpg").getAbsolutePath());
        bL.setEntetePage(new File(ReportConstants.URL_IMAGE+"entetepage.jpg").getAbsolutePath());

        List<TransactionDTO> transactionList = transactionService.findByLivraisonId(idLivraison);

        for (Transaction t: l.getTransactions()) {
            LigneDTO op = new LigneDTO();
            String caracteristque = returnCaracteristique(t.getCaracteristiques());
            op.setDesignation(t.getProduit().getLibelle()+" "+caracteristque);
            op.setStatut("Livré");
            op.setQuantite(t.getQuantiteALivre());
            op.setObservation("");
            lignes.add(op);
        }
        bL.setLignes(lignes);
        return bL;
    }

    private OrdreVirementDTO createOrdreVirement(Long idPaiement){
        OrdreVirementDTO ordreV = new OrdreVirementDTO();
        Paiement p = paiementRepository.findById(idPaiement).get();
        if (p != null){
            Personnel empl = personnelRepository.findById(p.getPersonnel().getId()).get();

            ordreV.setTitulaire(empl.getNom()+" "+empl.getPrenom());
            ordreV.setLibelle(p.getMotif());
            ordreV.setNumeroCompte("1002947626630");
            ordreV.setMontant(p.getMontant().toString());
            ordreV.setBanque(p.getBanque());
            ordreV.setBic(empl.getBic());
            ordreV.setIban(empl.getNumeroCompte());
            ordreV.setLieuDate("Abidjan, le "+ p.getDate().atZone(ZoneId.systemDefault()).format(dateFormatter));
            ordreV.setEntetePage(new File(ReportConstants.URL_IMAGE+"entetepage.jpg").getAbsolutePath());

        }

        return ordreV;
    }

    public ModelAndView viewOrdeVirement(ApplicationContext applicationContext, RequestReportingDTO r) {
        AbstractJasperReportsView view = ReportUtils.getJasperView(r.getFormat());
        Map<String, Object> params = new HashMap<>();
        OrdreVirementDTO ordreVirementDTO = createOrdreVirement(r.getId());
        JRBeanCollectionDataSource dataSource;
        if (ReportFormat.XLSX.equals(r.getFormat()) || ReportFormat.CSV.equals(r.getFormat())) {
            view.setUrl(ReportConstants.ORDRE_VIREMENT_XLSX);
        } else {
            view.setUrl(ReportConstants.ORDRE_VIREMENT_PDF);
        }
        dataSource = new JRBeanCollectionDataSource(Collections.singletonList(ordreVirementDTO));
        view.setApplicationContext(applicationContext);
        params.put(ReportConstants.PARAM_DATASOURCE, dataSource);
        return new ModelAndView(view, params);
    }

    public ModelAndView viewBordereauLivraison(ApplicationContext applicationContext, RequestReportingDTO r) {
        AbstractJasperReportsView view = ReportUtils.getJasperView(r.getFormat());
        Map<String, Object> params = new HashMap<>();
        BordereauLivraisonDTO b = createBordereauLivraisonNew(r.getId());
        JRBeanCollectionDataSource dataSource;
        if (ReportFormat.XLSX.equals(r.getFormat()) || ReportFormat.CSV.equals(r.getFormat())) {
            view.setUrl(ReportConstants.BORDEREAU_LIVRAISON_XLSX);
        } else {
            view.setUrl(ReportConstants.BORDEREAU_LIVRAISON_PDF);
        }
        dataSource = new JRBeanCollectionDataSource(Collections.singletonList(b));
        view.setApplicationContext(applicationContext);
        params.put(ReportConstants.PARAM_DATASOURCE, dataSource);
        return new ModelAndView(view, params);
    }

    public ModelAndView viewBordereauCommande(ApplicationContext applicationContext, RequestReportingDTO r) {

        log.debug("\n\n\n\nreport format: {}",r.getId());
        log.debug("report format: {}\n\n\n\n",r.getFormat());
        AbstractJasperReportsView view = ReportUtils.getJasperView(r.getFormat());
        Map<String, Object> params = new HashMap<>();
        BordereauCommandeDTO bC = createBordereauCommande(r.getId());
      //  JRDataSource dataSource;
        JRBeanCollectionDataSource dataSource;
        if (ReportFormat.XLSX.equals(r.getFormat()) || ReportFormat.CSV.equals(r.getFormat())) {
            view.setUrl(ReportConstants.BORDEREAU_COMMANDE_XLSX);
        } else {
            log.debug("---------------ELSE---------: {}\n\n\n\n",view.getContentType());
            view.setUrl(ReportConstants.BORDEREAU_COMMANDE_PDF);
        }
        dataSource = new JRBeanCollectionDataSource(Collections.singletonList(bC));
        view.setApplicationContext(applicationContext);
        params.put(ReportConstants.PARAM_DATASOURCE, dataSource);
        return new ModelAndView(view, params);
    }

    public String chifferToLetters (String input) {
        MoneyConverters converter = MoneyConverters.FRENCH_BANKING_MONEY_VALUE;
        String s = converter.asWords(new BigDecimal(input));
        String[] tab =  s.split("€");
        return tab[0];
    }

    public String returnCaracteristique(String chaine){
        if (chaine != null){
            String tab[] = chaine.split(";");
            for (int i = 0; i < tab.length; i++) {
                String tabb[] = tab[i].split(":");
                chaine = chaine+" "+tabb[1];
            }

            return chaine;
        }

        return " ";
    }
}
