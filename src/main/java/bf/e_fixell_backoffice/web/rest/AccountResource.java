package bf.e_fixell_backoffice.web.rest;

import bf.e_fixell_backoffice.domain.User;
import bf.e_fixell_backoffice.domain.enumeration.Statut;
import bf.e_fixell_backoffice.domain.enumeration.TypeCommande;
import bf.e_fixell_backoffice.repository.UserRepository;
import bf.e_fixell_backoffice.security.SecurityUtils;
import bf.e_fixell_backoffice.service.*;
import bf.e_fixell_backoffice.service.dto.DataParameter;
import bf.e_fixell_backoffice.service.dto.PasswordChangeDTO;
import bf.e_fixell_backoffice.service.dto.SessionCaisseDTO;
import bf.e_fixell_backoffice.service.dto.UserDTO;
import bf.e_fixell_backoffice.service.mapper.UserMapper;
import bf.e_fixell_backoffice.web.rest.errors.*;
import bf.e_fixell_backoffice.web.rest.errors.EmailAlreadyUsedException;
import bf.e_fixell_backoffice.web.rest.errors.InvalidPasswordException;
import bf.e_fixell_backoffice.web.rest.vm.KeyAndPasswordVM;
import bf.e_fixell_backoffice.web.rest.vm.ManagedUserVM;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.*;

/**
 * REST controller for managing the current user's account.
 */
@RestController
@RequestMapping("/api")
public class AccountResource {

    private final Logger log = LoggerFactory.getLogger(AccountResource.class);
    private final UserRepository userRepository;
    private final UserService userService;
    private final SessionCaisseService sessionCaisseService;
    private final MailService mailService;
    private final UserMapper userMapper;
    private final CommandeService commandeService;
    private final ClientService clientService;
    private final FournisseurService fournisseurService;
    private final VenteService venteService;

    public AccountResource(UserRepository userRepository, UserService userService, SessionCaisseService sessionCaisseService, MailService mailService, UserMapper userMapper, CommandeService commandeService, ClientService clientService, FournisseurService fournisseurService, VenteService venteService) {

        this.userRepository = userRepository;
        this.userService = userService;
        this.sessionCaisseService = sessionCaisseService;
        this.mailService = mailService;
        this.userMapper = userMapper;
        this.commandeService = commandeService;
        this.clientService = clientService;
        this.fournisseurService = fournisseurService;
        this.venteService = venteService;
    }

    private static boolean checkPasswordLength(String password) {
        return !StringUtils.isEmpty(password) &&
            password.length() >= ManagedUserVM.PASSWORD_MIN_LENGTH &&
            password.length() <= ManagedUserVM.PASSWORD_MAX_LENGTH;
    }

    /**
     * {@code POST  /register} : register the user.
     *
     * @param managedUserVM the managed user View Model.
     * @throws InvalidPasswordException  {@code 400 (Bad Request)} if the password is incorrect.
     * @throws EmailAlreadyUsedException {@code 400 (Bad Request)} if the email is already used.
     * @throws LoginAlreadyUsedException {@code 400 (Bad Request)} if the login is already used.
     */
    @PostMapping("/register")
    @ResponseStatus(HttpStatus.CREATED)
    public void registerAccount(@Valid @RequestBody ManagedUserVM managedUserVM) {
        if (!checkPasswordLength(managedUserVM.getPassword())) {
            throw new InvalidPasswordException();
        }
        User user = userService.registerUser(managedUserVM, managedUserVM.getPassword());
        mailService.sendActivationEmail(user);
    }

    /**
     * {@code GET  /activate} : activate the registered user.
     *
     * @param key the activation key.
     * @throws RuntimeException {@code 500 (Internal Server Error)} if the user couldn't be activated.
     */
    @GetMapping("/activate")
    public void activateAccount(@RequestParam(value = "key") String key) {
        Optional<User> user = userService.activateRegistration(key);
        if (!user.isPresent()) {
            throw new AccountResourceException("No user was found for this activation key");
        }
    }

    /**
     * {@code GET  /authenticate} : check if the user is authenticated, and return its login.
     *
     * @param request the HTTP request.
     * @return the login if the user is authenticated.
     */
    @GetMapping("/authenticate")
    public String isAuthenticated(HttpServletRequest request) {
        log.debug("REST request to check if the current user is authenticated");
        return request.getRemoteUser();
    }

    /**
     * {@code GET  /account} : get the current user.
     *
     * @return the current user.
     * @throws RuntimeException {@code 500 (Internal Server Error)} if the user couldn't be returned.
     */
    @GetMapping("/account")
    public DataParameter getAccount() {
        UserDTO user;
        int cmdeClient=0;
        int cmdeFournisseur=0;
        long hours = 23;
        long minutes = 59;
        long seconde = 59;
        List<String> typeCommant;
        LocalDate start = LocalDate.now().minusDays(LocalDate.now().getDayOfMonth()-1);
        LocalDate end = LocalDate.now().minusDays(LocalDate.now().getDayOfMonth()).plusMonths(1);
        SessionCaisseDTO sessionCaisseDTO =null;
        DataParameter dataParameter=new DataParameter();
        user= userService.getUserWithAuthorities()
            .map(UserDTO::new)
            .orElseThrow(() -> new AccountResourceException("User could not be found"));
        Optional<SessionCaisseDTO>sess = sessionCaisseService.findByUserAndStatut(userMapper.userDTOToUser(user) , Statut.OUVERT);
        if (sess.isPresent())
            sessionCaisseDTO = sess.get();
        dataParameter.setSessionCaisseDTO(sessionCaisseDTO);
        dataParameter.setUserDTO(user);
        typeCommant = commandeService.findTypeCommande(start.atStartOfDay(ZoneId.systemDefault()).toInstant(),end.atStartOfDay(ZoneId.systemDefault()).toInstant().plus(hours, ChronoUnit.HOURS).plus(minutes,ChronoUnit.MINUTES).plus(seconde,ChronoUnit.SECONDS));
        for (String typeC: typeCommant){
            if (typeC.equals(TypeCommande.COMMANDE_CLIENT.name()))
                cmdeClient +=1;
            else
                cmdeFournisseur+=1;
        }
        dataParameter.setTotalCommandeClient(cmdeClient);
        dataParameter.setTotalCommandeFournisseur(cmdeFournisseur);
        dataParameter.setNbClient(clientService.countClient());
        dataParameter.setNbFournisseur(fournisseurService.countFournisseur());
        dataParameter.setTotalVente(venteService.countVente(start.atStartOfDay(ZoneId.systemDefault()).toInstant(),end.atStartOfDay(ZoneId.systemDefault()).toInstant().plus(hours, ChronoUnit.HOURS).plus(minutes,ChronoUnit.MINUTES).plus(seconde,ChronoUnit.SECONDS)));
        return dataParameter;
        /*return userService.getUserWithAuthorities()
            .map(UserDTO::new)
            .orElseThrow(() -> new AccountResourceException("User could not be found"));*/
    }

    /**
     * {@code POST  /account} : update the current user information.
     *
     * @param userDTO the current user information.
     * @throws EmailAlreadyUsedException {@code 400 (Bad Request)} if the email is already used.
     * @throws RuntimeException          {@code 500 (Internal Server Error)} if the user login wasn't found.
     */
    @PostMapping("/account")
    public void saveAccount(@Valid @RequestBody UserDTO userDTO) {
        String userLogin = SecurityUtils.getCurrentUserLogin().orElseThrow(() -> new AccountResourceException("Current user login not found"));
        Optional<User> existingUser = userRepository.findOneByEmailIgnoreCase(userDTO.getEmail());
        if (existingUser.isPresent() && (!existingUser.get().getLogin().equalsIgnoreCase(userLogin))) {
            throw new EmailAlreadyUsedException();
        }
        Optional<User> user = userRepository.findOneByLogin(userLogin);
        if (!user.isPresent()) {
            throw new AccountResourceException("User could not be found");
        }
        userService.updateUser(userDTO.getFirstName(), userDTO.getLastName(), userDTO.getEmail(),
            userDTO.getLangKey(), userDTO.getImageUrl());
    }

    /**
     * {@code POST  /account/change-password} : changes the current user's password.
     *
     * @param passwordChangeDto current and new password.
     * @throws InvalidPasswordException {@code 400 (Bad Request)} if the new password is incorrect.
     */
    @PostMapping(path = "/account/change-password")
    public void changePassword(@RequestBody PasswordChangeDTO passwordChangeDto) {
        if (!checkPasswordLength(passwordChangeDto.getNewPassword())) {
            throw new InvalidPasswordException();
        }
        userService.changePassword(passwordChangeDto.getCurrentPassword(), passwordChangeDto.getNewPassword());
    }


    @PostMapping(path = "/account/modified-profil")
    public UserDTO modifiedProfil(@RequestBody UserDTO userDTO) {
       return userService.modifiedProfil(userDTO);
    }

    // ACTIVE OU DESACTIVE UN COMPTE UTILISATEUR
    @PutMapping(path = "/account/enable-desable")
    public Optional<UserDTO> enableOrDesableAccount(@RequestBody UserDTO userDTO) {
        log.info("----------- ENABLE OR DESABLE ACCOUNT----------- " + userDTO.toString());
        return userService.enableOrDesableAccount(userDTO);
    }

    /**
     * {@code POST   /account/reset-password/init} : Send an email to reset the password of the user.
     *
     * @param mail the mail of the user.
     */
    @PostMapping(path = "/account/reset-password/init")
    public void requestPasswordReset(@RequestBody String mail) {
        Optional<User> user = userService.requestPasswordReset(mail);
        if (user.isPresent()) {
            mailService.sendPasswordResetMail(user.get());
        } else {
            // Pretend the request has been successful to prevent checking which emails really exist
            // but log that an invalid attempt has been made
            log.warn("Password reset requested for non existing mail '{}'", mail);
        }
    }

    /**
     * {@code POST   /account/reset-password/finish} : Finish to reset the password of the user.
     *
     * @param keyAndPassword the generated key and the new password.
     * @throws InvalidPasswordException {@code 400 (Bad Request)} if the password is incorrect.
     * @throws RuntimeException         {@code 500 (Internal Server Error)} if the password could not be reset.
     */
    @PostMapping(path = "/account/reset-password/finish")
    public void finishPasswordReset(@RequestBody KeyAndPasswordVM keyAndPassword) {
        if (!checkPasswordLength(keyAndPassword.getNewPassword())) {
            throw new InvalidPasswordException();
        }
        Optional<User> user =
            userService.completePasswordReset(keyAndPassword.getNewPassword(), keyAndPassword.getKey());

        if (!user.isPresent()) {
            throw new AccountResourceException("No user was found for this reset key");
        }
    }

    private static class AccountResourceException extends RuntimeException {
        private AccountResourceException(String message) {
            super(message);
        }
    }
}
