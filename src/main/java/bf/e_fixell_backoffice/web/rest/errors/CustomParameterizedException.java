package bf.e_fixell_backoffice.web.rest.errors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class CustomParameterizedException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    private static final String PARAM = "param";

    private final String message;
    private final Logger log = LoggerFactory.getLogger(CustomParameterizedException.class);
    private final Map<String, String> paramMap = new HashMap<>();

    public CustomParameterizedException(String message, String... params) {
        super(message);
        this.message = message;
        if (params != null && params.length > 0) {
            for (int i = 0; i < params.length; i++) {
                paramMap.put(PARAM + i, params[i]);
            }
        }
        //  this.messageProducer.sendMessage(message, "error");
        log.error(message);
    }

    public CustomParameterizedException(String message, Map<String, String> paramMap) {
        super(message);
        this.message = message;
        this.paramMap.putAll(paramMap);
        log.error(message);
        //  this.messageProducer.sendMessage(message, "error");
    }

    public ParameterizedErrorVM getErrorVM() {
        return new ParameterizedErrorVM(message, paramMap);
    }

}
