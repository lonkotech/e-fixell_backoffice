package bf.e_fixell_backoffice.repository;

import bf.e_fixell_backoffice.domain.EtapeTransport;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EtapeTransportRepository extends JpaRepository<EtapeTransport, Long> {
}
