package bf.e_fixell_backoffice.repository;

import bf.e_fixell_backoffice.domain.Commande;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Commande entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CommandeRepository extends JpaRepository<Commande, Long>, JpaSpecificationExecutor<Commande> {

    @Modifying
    @Query("UPDATE Commande c SET  c.deleted= true where c.id=:id")
    void deleteById(@Param("id") Long id);


    @Query("select c from Commande c  where(" +
        "(:libelle is null or :libelle='' or UPPER(c.libelle) like upper('%'||:libelle||'%'))" +
        "and (:code is null or :code='' or UPPER(c.code) like upper('%'||:code||'%') )" +
        "and (:typeCommande is null or :typeCommande='' or UPPER(c.typeCommande) like upper('%'||:typeCommande||'%') )" +
        "and (:etat is null or :etat=''  or LOWER(c.etat) like LOWER('%'||:etat||'%'))" +
        "and (c.deleted=false)" +
        ")")
    Page<Commande> findWithCriteria(
        Pageable pageable,
        @Param(value = "libelle") String libelle,
        @Param(value = "code") String code,
        @Param(value = "typeCommande") String typeCommande,
        @Param(value = "etat") String etat
    );

    List<Commande> findAllByCommandeFournisseurId(Long id);

    List<Commande> findAllByFournisseurIsNullAndCommandeFournisseurIdIsNullAndDateLivraisonIsNull();

    List<Commande> findAllByClientIsNotNullAndFournisseurIsNullAndDateLivraisonIsNullAndDeletedIsFalse();

    List<Commande> findCommandesByClientIsNotNullAndFournisseurIsNullAndDateLivraisonIsNullAndDeletedIsFalse();

    Optional<Commande> findByCode(String code);

    @Query("select c.typeCommande from Commande c where c.deleted=false and c.date between :dateStart and :dateEnd")
    List<String> findTypeCommande(@Param("dateStart") Instant dateStart, @Param("dateEnd") Instant dateEnd);
}
