package bf.e_fixell_backoffice.repository;

import bf.e_fixell_backoffice.domain.SocieteTransport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the SocieteTransport entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SocieteTransportRepository extends JpaRepository<SocieteTransport, Long>, JpaSpecificationExecutor<SocieteTransport> {
}
