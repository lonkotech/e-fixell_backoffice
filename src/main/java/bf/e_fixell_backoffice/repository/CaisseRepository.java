package bf.e_fixell_backoffice.repository;

import bf.e_fixell_backoffice.domain.Caisse;
import bf.e_fixell_backoffice.domain.enumeration.TypeCaisse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Caisse entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CaisseRepository extends JpaRepository<Caisse, Long>, JpaSpecificationExecutor<Caisse> {

    Caisse findByLibelleAndDeletedIsFalse(String libelle);

    Caisse findByTypeCaisseAndDeletedIsFalse(TypeCaisse typeCaisse);

    Optional<Caisse> findByIdAndDeletedIsFalse(Long id);

    @Query("select c from Caisse c where ( " +
        "(:code is null or :code='' or c.code=:code ) and" +
        "(:libelle is null or :libelle='' or c.libelle=:libelle ) and " +
        "(:typeCaisse is null or c.typeCaisse=:typeCaisse) and c.deleted=false ) ")
    Page<Caisse> findAllWithCriteria(@Param("code") String code, @Param("libelle") String libelle, @Param("typeCaisse") TypeCaisse typeCaisse, Pageable pageable);


    @Query("select c from Caisse c where ( " +
        "(:code is null or :code='' or c.code=:code ) and" +
        "(:libelle is null or :libelle='' or c.libelle=:libelle ) and " +
        "(:typeCaisse is null or c.typeCaisse=:typeCaisse) and c.deleted=false ) ")
    List<Caisse> findAllWithCriteriaList(@Param("code") String code, @Param("libelle") String libelle, @Param("typeCaisse") TypeCaisse typeCaisse);

}
