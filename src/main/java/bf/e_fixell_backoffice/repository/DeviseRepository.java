package bf.e_fixell_backoffice.repository;

import bf.e_fixell_backoffice.domain.Devise;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Devise entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DeviseRepository extends JpaRepository<Devise, Long> {
    @Query("select d from Devise d where d.deviseBase is not null ")
    Devise findByDeviseBase();
}
