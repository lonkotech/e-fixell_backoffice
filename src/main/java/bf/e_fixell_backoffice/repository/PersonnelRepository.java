package bf.e_fixell_backoffice.repository;

import bf.e_fixell_backoffice.domain.Personnel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Spring Data  repository for the Personnel entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PersonnelRepository extends JpaRepository<Personnel, Long>, JpaSpecificationExecutor<Personnel> {

    @Query("select p from Personnel p inner join HistoriqueAffectation h on h.personnel.id=p.id where ( " +
        "(:nom is null or :nom='' or p.nom like ('%'||:nom||'%') ) and (:prenom is null or:prenom='' or p.prenom like ('%'||:prenom||'%') ) and " +
        "(:telephone is null or :telephone='' or p.telephone like('%'||:telephone||'%') ) and (:matricule is null or :matricule='' or p.matricule like('%'||:matricule||'%') ) and(:fonctionId is null or (h.fonction.id=:fonctionId) ) and p.deleted=false and h.dateFin is null )")
    Page<Personnel> findAllWithCriteria(@Param("nom") String nom, @Param("prenom") String prenom, @Param("telephone") String telephone, @Param("matricule") String matricule, @Param("fonctionId") Long fonctionId, Pageable pageable);

    Optional<Personnel> findByIdAndDeletedIsFalse(Long personnelId);


}

