package bf.e_fixell_backoffice.repository;

import bf.e_fixell_backoffice.domain.OperationCaisse;
import bf.e_fixell_backoffice.domain.SessionCaisse;
import bf.e_fixell_backoffice.domain.enumeration.TypeOperationCaisse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

/**
 * Spring Data  repository for the OperationCaisse entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OperationCaisseRepository extends JpaRepository<OperationCaisse, Long>, JpaSpecificationExecutor<OperationCaisse> {


    Page<OperationCaisse> findBySessionCaisse(SessionCaisse sessionCaisse, Pageable pageable);

    @Query("select o from OperationCaisse  o where (" +
        "(:caisseSrcId is null or o.caisseSrc.id=:caisseSrcId) or (" +
        ":caisseDestId is null or o.caisseDst.id=:caisseDestId) and(" +
        " :typeOperation is null or o.typeOperationCaisse=:typeOperation) and(" +
        ":montant is null or o.montant=:montant) and (" +
        "o.createdDate between :dateDebut and :dateFin or cast(:dateDebut as timestamp) is null ) )")
    Page<OperationCaisse> findByCriteria(@Param("caisseSrcId") Long caisseSrcId, @Param("caisseDestId") Long caisseDestId
        , @Param("typeOperation") TypeOperationCaisse typeOperationCaisse, @Param("montant") BigDecimal montant, @Param("dateDebut") ZonedDateTime dateDebut,
                                         @Param("dateFin") ZonedDateTime dateFin, Pageable pageable);
}
