package bf.e_fixell_backoffice.repository;

import bf.e_fixell_backoffice.domain.Fonction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Spring Data  repository for the Fonction entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FonctionRepository extends JpaRepository<Fonction, Long>, JpaSpecificationExecutor<Fonction> {

    Optional<Fonction> findByIdAndDeletedIsFalse(Long id);


    @Query("SELECT f from Fonction f where f.deleted=false ")
    Page<Fonction> findAllWithCriteria(Pageable pageable);

}
