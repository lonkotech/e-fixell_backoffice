package bf.e_fixell_backoffice.repository;

import bf.e_fixell_backoffice.domain.Paiement;

import bf.e_fixell_backoffice.domain.SessionCaisse;
import bf.e_fixell_backoffice.domain.Vente;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

/**
 * Spring Data  repository for the Paiement entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PaiementRepository extends JpaRepository<Paiement, Long>, JpaSpecificationExecutor<Paiement> {

    List<Paiement> findByVente(Vente vente);

    @Query("select coalesce(sum(p.montant),0) from Paiement p where p.id=?1 ")
    BigDecimal getMontantVente(Long venteId);


    Page<Paiement> findBySessioncaisse(SessionCaisse sessionCaisse, Pageable pageable);

    List<Paiement> findByCommandeId(Long id);

    List<Paiement> findByPersonnelId(Long id);

    List<Paiement> findByPersonnelIsNotNull();
    Page<Paiement> findByPersonnelIsNotNull(Pageable pageable);
    Page<Paiement> findByPersonnelIsNotNull(@Nullable Specification<Paiement> var1, Pageable pageable);

    @Query("select coalesce(sum(p.montant),0) from Paiement p where p.commande.id=?1")
    BigDecimal getMontantPaimentCommande(Long commandeId);

}
