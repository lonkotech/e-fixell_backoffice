package bf.e_fixell_backoffice.repository;

import bf.e_fixell_backoffice.domain.Vente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.Instant;

/**
 * Spring Data  repository for the Vente entity.
 */
@SuppressWarnings("unused")
@Repository
public interface VenteRepository extends JpaRepository<Vente, Long>, JpaSpecificationExecutor<Vente> {
    @Query("select count(v.id)from Vente v where v.deleted=false and v.date between :dateStart and :dateEnd")
    Long countAll(@Param("dateStart") Instant dateStart, @Param("dateEnd") Instant dateEnd);
}
