package bf.e_fixell_backoffice.repository;

import bf.e_fixell_backoffice.domain.Caisse;
import bf.e_fixell_backoffice.domain.SessionCaisse;
import bf.e_fixell_backoffice.domain.User;
import bf.e_fixell_backoffice.domain.enumeration.Statut;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the SessionCaisse entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SessionCaisseRepository extends JpaRepository<SessionCaisse, Long>, JpaSpecificationExecutor<SessionCaisse> {

    SessionCaisse findByIdAndDeletedIsFalse(Long id);

    @Query("select s from SessionCaisse s where (:caisseId is null or s.caisse.id=:caisseId ) and " +
        "(s.user.id=:userId) and s.deleted=false ")
    Page<SessionCaisse> findAllByCriteria(@Param("caisseId") Long caisseId, @Param("userId") Long userId, Pageable pageable);

    List<SessionCaisse> findAll();

    SessionCaisse findByCaisseAndStatut(Caisse caisse, Statut statut);

    List<SessionCaisse> findByCaisse(Caisse caisse);

    Optional<SessionCaisse> findByUserAndStatut(User user, Statut statut);

    SessionCaisse findByIdAndUser(Long id, User user);
}
