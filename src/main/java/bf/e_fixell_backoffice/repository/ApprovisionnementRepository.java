package bf.e_fixell_backoffice.repository;

import bf.e_fixell_backoffice.domain.Approvisionnement;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Spring Data  repository for the Approvisionnement entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ApprovisionnementRepository extends JpaRepository<Approvisionnement, Long>, JpaSpecificationExecutor<Approvisionnement> {
    @Query("select app from Approvisionnement app where app.deleted =false ")
    Page<Approvisionnement> findAll(Pageable pageable);

    Optional<Approvisionnement> findByCode(String code);
}
