package bf.e_fixell_backoffice.repository;

import bf.e_fixell_backoffice.domain.Inventaire;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.Instant;

/**
 * Spring Data  repository for the Inventaire entity.
 */
@SuppressWarnings("unused")
@Repository
public interface InventaireRepository extends JpaRepository<Inventaire, Long> {
    @Query("select i from Inventaire i where (i.produit.id=:produitId or :produitId  is null) and (i.createdDate between :startDate and :endDate or cast(:startDate as timestamp) is null) ")
    Page<Inventaire> findByCriteria(Pageable pageable, @Param("produitId") Long produitId, @Param("startDate") Instant startDate, @Param("endDate") Instant endDate);
}
