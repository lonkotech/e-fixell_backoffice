package bf.e_fixell_backoffice.repository;

import bf.e_fixell_backoffice.domain.Fournisseur;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Fournisseur entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FournisseurRepository extends JpaRepository<Fournisseur, Long>, JpaSpecificationExecutor<Fournisseur> {

    @Query("select f from Fournisseur f  where(" +
        "(:nom is null or :nom='' or UPPER(f.nom) like upper('%'||:nom||'%'))" +
        "and (:prenom is null or :prenom='' or UPPER(f.prenom) like upper('%'||:prenom||'%') )" +
        "and (:raisonSocial is null or :raisonSocial='' or UPPER(f.raisonSocial) like upper('%'||:raisonSocial||'%') )" +
        "and (:telephone is null or :telephone='' or f.telephone=:telephone)" +
        "and (:typePersonne is null or :typePersonne=''  or f.typePersonne=:typePersonne)" +
        "and (f.deleted=false)" +
        ")")
    Page<Fournisseur> findWithCriteria(
        Pageable pageable,
        @Param(value = "nom") String nom,
        @Param(value = "prenom") String prenom,
        @Param(value = "raisonSocial") String raisonSocial,
        @Param(value = "telephone") String telephone,
        @Param(value = "typePersonne") String typePersonne
    );

    @Query("select f from Fournisseur f where f.deleted=false ")
    List<Fournisseur> findAll();

    @Modifying
    @Query("UPDATE Fournisseur f SET  f.deleted= true where f.id=:id")
    void deleteById(@Param("id") Long id);

    @Query("select count(f.id) from Fournisseur f where f.deleted=false ")
    Long countAllAndDeletedIsFalse();

}
