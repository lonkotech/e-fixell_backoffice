package bf.e_fixell_backoffice.repository;

import bf.e_fixell_backoffice.domain.HistoriqueAffectation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the HistoriqueAffectation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HistoriqueAffectationRepository extends JpaRepository<HistoriqueAffectation, Long>, JpaSpecificationExecutor<HistoriqueAffectation> {

    List<HistoriqueAffectation> findByFonctionId(Long fonctionId);

    @Query("select h from HistoriqueAffectation h where h.personnel.id=:personnelId and h.dateFin is null ")
    HistoriqueAffectation findByPersonnelIdAndDateFinIsNull(@Param("personnelId") Long personnelId);

}
