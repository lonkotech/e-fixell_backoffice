package bf.e_fixell_backoffice.repository;

import bf.e_fixell_backoffice.domain.Transaction;
import bf.e_fixell_backoffice.domain.enumeration.Etat;
import bf.e_fixell_backoffice.domain.enumeration.TypeTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;
import java.util.Set;

/**
 * Spring Data  repository for the Transaction entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long>, JpaSpecificationExecutor<Transaction> {
    Set<Transaction> findByApprovisionnement_Id(Long id);

    @Query("select trans from Transaction trans where trans.produit.id =:id and trans.typeTransaction=:typeTransaction and trans.decompte>0")
    List<Transaction> findByProduitIdAndTyAppro(@Param("id") Long id, @Param("typeTransaction") TypeTransaction typeTransaction);

    @Modifying
    @Query("UPDATE Transaction t SET  t.etat=:etat,t.motif=:motif where t.commande.id=:id")
    int resetTransaction(@Param("etat") Etat etat, @Param("id") Long id, @Param("motif") String motif);

    List<Transaction> findByCommandeIdAndEtat(Long commandeId, Etat etat);

    List<Transaction> findByCommandeIdAndEtatAndDateLivreIsNotNull(Long commandeId, Etat etat);

    List<Transaction> findByLivraisonId(Long livraisonId);

    @Query("select t from Transaction t where t.produit.id=:produitId and (t.date between :dateDebut and :dateFin or cast(:dateDebut as timestamp) is null)")
    List<Transaction> findByProduitIdAndDateBetween(@Param("produitId") Long produitId, @Param("dateDebut") Instant dateDebut,@Param("dateFin")Instant dateFin);
}
