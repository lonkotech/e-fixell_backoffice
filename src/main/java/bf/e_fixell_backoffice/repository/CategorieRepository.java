package bf.e_fixell_backoffice.repository;

import bf.e_fixell_backoffice.domain.Categorie;
import bf.e_fixell_backoffice.service.dto.CategorieDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Categorie entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CategorieRepository extends JpaRepository<Categorie, Long>, JpaSpecificationExecutor<Categorie> {

    @Query("select c from Categorie c where c.deleted=false and (" +
        "(:libelle is null or :libelle='' or c.libelle like ('%'||:libelle||'%') )) ")
    Page<Categorie> findByCriteria(Pageable pageable,@Param("libelle") String libelle);
}
