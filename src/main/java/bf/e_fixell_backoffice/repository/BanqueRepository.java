package bf.e_fixell_backoffice.repository;

import bf.e_fixell_backoffice.domain.Banque;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Banque entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BanqueRepository extends JpaRepository<Banque, Long> {

    Page<Banque> findByDeletedIsFalse(Pageable pageable);

    Banque findByIdAndDeletedIsFalse(Long id);
}
